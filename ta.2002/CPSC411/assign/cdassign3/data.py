#CPSC411 -- Datatypes for M+ in python


#data M_prog = M_prog ([M_decl],[M_stmt])
from irclass import *
from mpexcep import semanticError

class prog:
      def __init__(self,decs,stmts):
            self.decs = decs  # a list
            self.stmts = stmts # a list

      def __repr__(self):
            "Possible representation(prettyprint) included in this one only."
            rep="["
            for dec in self.decs: rep = rep + dec.__repr__()+","
            rep = rep[:-1] +"],["
            for stmt in self.stmts: rep = rep + stmt.__repr__()+","
            rep = rep[:-1] +"]"
            return "prog("+rep + ")"

      def check(self,st):
            for d in self.decs: st.insert(d)

            irds=[]
            for d in self.decs:
                  ird=d.check(st)
                  if ird != None : irds.append(d.check(st))
            irss=[]
            for s in self.stmts:
                  irss.append(s.check(st))
            return iprog(irds, st.storage(),irss)

      def setfuncrettype(self,rettype):
   
      
#data M_decl = M_var (string,M_type)
#            | M_fun (string ,[(string,M_type)],M_type,M_prog)

class decl:
      def __init__(self,type,varid=None,vartype=None,
                   funid=None,funargdecs=None,funtype=None,funprog=None):
            self.type=type   # VAR or FUN
            self.varid=varid  # the id
            self.vartype=vartype # Bool or Int

            self.funid=funid #The id in case it's a function
            self.funargdecs=funargdecs # a list of argument declarations
            self.funtype=funtype
            self.funprog=funprog  # an instance of prog

      def check(self,st):
            if self.type == 'var' : return None
            #otherwise, functionwith new scope etc...
 

      def id(self):
            if self.type=='var': return self.varid
            if self.type=='fun': return self.funid

      def isvar(self):
            return self.type == 'var'
      def isfun(self):
            return self.type == 'fun'
            

      def funargtypes(self):
            ret=[]
            for  x in self.funargdecs:
                  ret.append(x[1])
            return ret


      def __repr__(self):
            ret=""
            if self.type =="var":
                  return "(VAR:"+self.varid+","+self.vartype+")"
            else:
                  ret = ret+ "(Fun: "+self.funid +",["
                  for d in self.funargdecs:
                        ret = ret+ d[0]+':'+d[1] +","
                  ret = ret[:-1] + "],"+self.funtype +"," + self.funprog.__repr__()+")"
                  return ret
            
#data M_stmt = M_ass (string,M_expr)
#            | M_while (M_expr,M_stmt)
#            | M_cond (M_expr,M_stmt,M_stmt) 
#            | M_read string
#            | M_print M_expr
#            | M_return M_expr
#            | M_block ([M_decl],[M_stmt])

class stmt:
      def __init__(self):
            "A base class to make life easier - use it or do it like decl."
            pass

      def settype(self,t):
            pass

class ass_stmt(stmt):
      def __init__(self,id,exp):
            self.id=id
            self.exp = exp

       def __repr__(self):
            return "(Assign:,"+self.id+","+self.exp.__repr__()+")"

      def check(self,st):
            stid=st.lookup(self.id)
            (ire,etype)=self.exp.check(st)
            if etype != stid[2] :raise semanticError("Mixed types in assign.")
            return istmt("iassign",offset=stid[1],level=stid[0],expr=ire)

class while_stmt(stmt):
      def __init__(self,exp,stmt):
            self.exp = exp
            self.stmt = stmt
            
      def __repr__(self):
            return "(While:,"+self.exp.__repr__()+","+self.stmt.__repr__()+")"

  
      def check(self,st):
 
            return istmt("iwhile",...


class cond_stmt(stmt):
      def __init__(self,exp,tstmt,fstmt):
            self.exp = exp
            self.tstmt=tstmt
            self.fstmt=fstmt

      def __repr__(self):
            return "(If:,"+self.exp.__repr__()+","+self.tstmt.__repr__()+","+self.fstmt.__repr__()+")"


      def check(self,st):
 


class read_stmt(stmt):
      def __init__(self,id):
            self.id=id

      def __repr__(self):
            return "(Read:,"+self.id+")"


      def check(self,st):
     
                              
class print_stmt(stmt):
      def __init__(self,exp):
            self.exp=exp
            
      def __repr__(self):
            return "(Print,"+self.exp.__repr__()+")"
 
      
      def check(self,st):


class ret_stmt(stmt):
      def __init__(self,exp):
            self.exp=exp

      def __repr__(self):
            return "(Return,"+self.exp.__repr__()+")"

      def settype(self,type):
            self.type=type

      def check(self,st):
  
            
class block_stmt(stmt):
      def __init__(self,decs,stmts):
            self.decs = decs
            self.stmts = stmts

      def __repr__(self):
            rep="["
            for dec in self.decs: rep = rep + dec.__repr__()+","
            rep = rep[:-1] +"],["
            for stmt in self.stmts: rep = rep + stmt.__repr__()+","
            rep = rep[:-1] +"]"
            return "block("+rep + ")"

 
      def check(self,st):
 
      

#data M_type = M_int | M_bool
# Not needed in python - use a string or enumerated value  if you want.

#data M_expr = M_num Int
#            | M_bl Bool
#            | M_id String
#            | M_app (M_operation,[M_expr])

class expr:
      def __init__(self, numv=None, bval=None, id=None, apply=None):
            self.numv=numv
            self.bval=bval
            self.id=id
            self.apply=apply

      def __repr__(self):
            if self.numv <>None :
                  return "Num=%d"%self.numv
            if self.bval <>None : return "Bool=%d"%self.bval
            if self.id <>None : return "ID="+self.id
            #else, apply
            
            appfn=self.apply[0]
            appfnparms=self.apply[1]
            ret = "(Apply %s to [%s])"%(appfn,appfnparms.__repr__())
            return ret

      def check(self,st):
            if self.numv <> None : return (iexpr('iint',ival=self.numv),'int')
            # handle bool and id

            #else we are in an apply operation
 

class args:
      def __init__(self,arglist,exist="yes"):
            self.arglist=arglist
            self.exist=exist
            

      def notthere(self):
            return self.exist=="no"

      def __repr__(self):
            rep = ""
            for a in self.arglist:
                  rep = rep + a.__repr__()+","

            return rep[:-1]
      def __len__(self):
            return len(self.arglist)

      def __getitem__(self,key):
            return self.arglist[key]
      def __setitem__(self,key,val):
            self.arglist[key]=val
      def __delitem__(self,key):
            del self.arglist[key]

      def check(self,st):
            ret=[]
            for e in self.arglist:
                  ret.append(e.check(st))
            return ret
                  
          
