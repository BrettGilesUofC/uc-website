


from mpexcep import semanticError

class stentry:
      def __init__(self):
            self.args=0
            self.vars=0
            self.dict={} 

      def __repr__(self):
            return "args:%d, vars:%d, dict:%s"%(self.args,self.vars, self.dict)


      def keys(self):

      def values(self):

      def items(self):

      def has_key(self,lookup):
            return self.dict.has_key(lookup)

      def __len__(self):
            return self.dict.len()

      def __getitem__(self,key):
            return self.dict[key]
      def __setitem__(self,key,val):
            self.dict[key]=val
      def __delitem__(self,key):
            del self.dict[key]

      def incargs(self):


      def incvars(self):


      def argoffset(self):


      def varoffset(self):


class vardata:
      def __init__(self,offset,mptype):
            self.mptype=mptype
            self.offset=offset

      def retsdata(self,level):
            return (level,self.offset,self.mptype)

class fundata:
      def __init__(self,codelabel,mptype,mpargtypes):
            self.mpargtypes=mpargtypes
            self.mptype=mptype
            self.cdlbl=codelabel

      def retsdata(self,level):
            return (level,self.cdlbl,self.mpargtypes,self.mptype)


class symboltable:
      def __init__(self):
            self.st=[]

      def __repr__(self):
      
      def newscope(self):
            self.st.append(stentry())
      def removescope(self):


      def storage(self):


      def lookup(self, lk):
            symt=self.st
            for level in range(len(symt)-1,-1,-1):
                  sentry = symt[level]
                  if sentry.has_key(lk):
                        return sentry[lk].retsdata(level)
                   
            raise semanticError("Identifier not found:"+lk)
                  
      def insert(self,dec):
            '''Does not handle overloading!, dec is an instance of the class
            decl and therefore is a var or fun '''
            symentry=self.st[len(self.st)-1] # ease of use
            if symentry.has_key(dec.id()):
                  raise semanticError("Duplicate id." + dec.id())
            if dec.isvar():
                  symentry.incvars()
                  symentry[dec.id()]=vardata(symentry.varoffset(),dec.vartype)
            if dec.isfun():
                  symentry[dec.id()]=fundata(getlabel( dec.id()),dec.funtype,dec.funargtypes())

      def insertbuiltin(self,fname,type,argtypes):
            '''handle in a similar way to insert above, flaggin the codelabel some way'''
            symentry=self.st[len(self.st)-1] # ease of use

                              
      def insertarg(self,argid,argtype):
            '''handle like insert'''



def getlabel(s):

