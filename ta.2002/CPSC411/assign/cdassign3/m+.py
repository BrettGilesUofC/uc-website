#!/usr/bin/python
import lex
from cmntlex import remove_comments
from mpparse import mpparse
from st import *
import sys

from mpexcep import semanticError

inp=remove_comments(sys.stdin.read())

newprog=mpparse(inp)


stab = symboltable()

stab.newscope()
stab.insertbuiltin('+','int',['int','int'])
# and all the rest

try:
      irpr = newprog.check(stab)
      print irpr
except semanticError, se:
      print se


