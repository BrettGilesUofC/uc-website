
import yacc
from mplex import tokens
from data import *
# Get the token map
#tokens = mplex.tokens

def p_prog(t):
    'prog : block'
    t[0]=t[1]


def p_block(t):
    'block : declarations programbody'
    t[0]=prog(t[1],t[2])


def p_programbody(t):
    'programbody : BEGIN progstmts END'
    t[0]=t[2]


def p_progstmts(t):
    'progstmts : progstmt SEMICOLON progstmts'
    t[0]=[t[1]]+t[3] # catenate the first element onto the front of the list


def p_progstmts_empty(t):
    'progstmts : empty'
    t[0]=[]


def p_progstmt(t):
    '''progstmt : IF expr THEN progstmt ELSE progstmt
                 | READ ID
                 | WHILE expr DO progstmt
                 | ID ASSIGN expr
                 | PRINT expr
                 | CLPAR block CRPAR'''
    if t[1]=='if': 
        t[0]=cond_stmt(t[2],t[4],t[6])
    elif t[1] == 'read':
        t[0]=read_stmt(t[2])
    elif t[1] == 'while':
        t[0]=while_stmt(t[2],t[4])
    elif t[1] == 'print':
        t[0]=print_stmt(t[2])
    elif t[1] == '{':
        t[0]=block_stmt(t[2].decs, t[2].stmts) #t2 is a block which returns a prog...
    else:  # 'ID':
        t[0]=ass_stmt(t[1],t[3])

def p_declarations_empty(t):
    'declarations : empty'
    t[0]=[]

def p_declarataions_list(t):
    'declarations : declaration SEMICOLON declarations'
    t[0]=[t[1]]+t[3]

def p_declaration(t):
    '''declaration : var_declaration
                    | fundec'''
    t[0]=t[1]

def p_var_declaration(t):
    'var_declaration :  VAR basicvardeclaration'
    t[0]=decl('var',varid=(t[2])[0], vartype=(t[2])[1])


def p_basicvardeclaration(t):
    'basicvardeclaration : ID COLON type'
    t[0]=(t[1],t[3])

def p_type(t):
    '''type : INT
            | BOOL'''
    t[0]=t[1]

def p_fundec(t):
    'fundec : FUN ID paramlist COLON type CLPAR funblock CRPAR'
    t[0]=decl('fun', funid=t[2],funargdecs=t[3],funtype=t[5],funprog=t[7])

def p_paramlist(t):
    'paramlist : LPAR parameters RPAR'
    t[0]=t[2]

def p_parameters(t):
    'parameters : parameters1'
    t[0]=t[1]

def p_parameters_empty(t):
    'parameters : empty'

    t[0]=[]

def p_parameters1_1(t):
    'parameters1 : parameters1 COMMA basicvardeclaration'

    t[0]=t[1]+[t[3]]

    
def p_parameters1_2(t):
    'parameters1 : basicvardeclaration'

    t[0]=[t[1]]




def p_funblock(t):
    'funblock : declarations fun_body'
    t[0]=prog(t[1],t[2])

def p_fun_body(t):
    'fun_body :  BEGIN progstmts RETURN expr SEMICOLON END'
    rstmt=ret_stmt(t[4])
    t[0]=t[2] + [rstmt]

def p_expr_1(t):
    'expr  :   expr OR bint_term'
    t[0]=expr(apply=(t[2],args([t[1],t[3]])))

def p_expr_2(t):
    'expr :  bint_term'
    t[0]=t[1]

def p_bint_term_1(t):
    'bint_term : bint_term AND bint_factor'
    t[0]=expr(apply=(t[2],args([t[1],t[3]])))

def p_bint_term_2(t):
    'bint_term :  bint_factor'
    t[0]=t[1]

def p_bint_factor_1(t):
    'bint_factor : NOT bint_factor'
    t[0]=expr(apply=(t[1],args([t[2]])))
              
def p_bint_factor_2(t):
    'bint_factor : int_expr compare_op int_expr'
    t[0]=expr(apply=(t[2],args([t[1],t[3]])))
    
def p_bint_factor_3(t):
    'bint_factor :  int_expr'
    t[0]=t[1]

def p_compare_op(t):
    '''compare_op : EQUAL
                  | LT
                  | GT
                  | LE
                  | GE'''
    t[0]=t[1]

def p_int_expr_1(t):
    'int_expr : int_expr addop int_term'
    t[0]=expr(apply=(t[2],args([t[1],t[3]])))

    
def p_int_expr_2(t):
    'int_expr : int_term'
    t[0]=t[1]
    
def p_addop(t):
    '''addop : ADD
             | SUB'''
    t[0]=t[1]

def p_int_term_1(t):
    'int_term : int_term mulop int_factor'
    t[0]=expr(apply=(t[2],args([t[1],t[3]])))

def p_int_term_2(t):
    'int_term  : int_factor'
    t[0]=t[1]

def p_mulop(t):
    '''mulop : MULT 
           | DIV'''
    t[0]=t[1]

def p_int_factor_1(t):
    'int_factor : LPAR expr RPAR'
    t[0]=t[2]
    
def p_int_factor_2(t):
    'int_factor : ID argument_list'
    if t[2].notthere() :
        t[0]=expr(id=t[1])
    else:
        t[0]=expr(apply=(t[1],t[2]))
        
def p_int_factor_3(t):
    'int_factor : NUM'
    t[0]=expr(numv=t[1])

def p_int_factor_4(t):
    'int_factor : BVAL'
    t[0]=expr(bval=t[1])

def p_int_factor_5(t):
    'int_factor : SUB int_factor'
    t[0]=expr(apply=("NEG",args([t[2]])))

def p_argument_list(t):
    'argument_list : LPAR arguments RPAR'
    t[0]=args(t[2])

def p_argument_list_empty(t):
    'argument_list : empty'
    t[0]=args([],exist="no")

def p_arguments(t):
    'arguments : arguments1'
    t[0]=t[1]
    
def p_arguments_empty(t):
    'arguments :  empty'
    t[0]=[]

def p_arguments1_1(t):
    'arguments1 : arguments1 COMMA expr'
    t[0]=t[1] + [t[3]]
def p_arguments1_2(t):
    'arguments1 :  expr'
    t[0]=[t[1]]



def p_empty(t):
    'empty : '
    pass

def p_error(t):
    print "Whoa. We're hosed"

yacc.yacc()

def mpparse(str):
    return yacc.parse(str)



