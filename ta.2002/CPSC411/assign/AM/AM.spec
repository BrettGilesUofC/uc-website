Minisculus+ stack machine description
-------------------------------------

Author: Robin Cockett
Date: January 1st, 2000

=============================================================================


Reading: 
Louden: ch7
Dragon: ch7

=============================================================================

INTRODUCTION:
============

This machine is not designed for efficiency: its aim is to illustrate 
the details of how stack-based runtime storage management works.  In 
particular we are interested in how activation records for functions 
and blocks actually work.

MACHINE LAYOUT
==============

The machine has the following registers (which can be manipulated):

%sp   --- stack pointer
%fp   --- frame pointer
%cp   --- code pointer

It also has a "virtual register":

%hp  --- the heap pointer

which cannot be manipulated.

On the stack are stored integer values, VALUE_I, boolean values, VALUE_B, 
character values, VALUE_C, void cells, VOID, stack pointers, STACK_PTR, 
heap pointers, HEAP_PTR, or code pointers, CODE_PTR.

All computations proceed by LOADing data onto the stack, APPlying operations
on the top two elements, or STOREing the top (or penultimate) stack element 
either elsewhere onto the stack or onto the heap. 

There are no register to register addressing modes.

Conceptually the heap is storage which grows opposing the stack.  However, 
the only access into the heap is through heap pointers and the only way to 
create a heap pointer is to allocate heap storage (or to move/copy it from 
one place to another on the stack or heap).  This machine has automatic 
garbage collection so there is never any need to release storage.

Here are the instructions:

(1)  Frame pointer instructions

     LOAD_R %fp
               Pushes %fp onto the top of the stack
               (literally load onto the stack register %fp) 

     LOAD_R %sp
               Pushes %sp onto the top of the stack
               (literally load onto the stack register %sp) 

     STORE_R %fp 
               Removes the stack pointer on the top of the stack and 
               sets register %fp to this value.
               (literally store to register %fp top of stack)

(2) Code pointer instructions

     JUMP   <code label>
               Sets the %cp to the value of the code label

     JUMP_S
               Removes the code pointer on top of the stack and sets 
               %cp to this value advanced by one step.

     JUMP_C <code label>
               Removes the boolean value on top of the stack and 
               if it is FALSE sets the CP to the value of the code label

     JUMP_O    Removes the (positive) integer on top of the stack 
               and moves the code pointer that number of steps 
               ahead (negative or zero values cause errors):
                   1 = next instruction
                   2 = skip next instruction
                   3 = skip 2 instructions
                      ....

     LOAD_R $cp
               Saves the program pointer advanced by one step on the 
               stack.
               (literally load onto the stack register %cp) 


     HALT
               End of execution

(3) Data manipulation on stack

     LOAD_I k
              Load a constant integer onto the stack.

     LOAD_B k
              Load a constant boolean onto the stack.

     LOAD_C c
              Load a constant character onto the stack.

     LOAD_O  m
              Removes the stack pointer on top of the stack and replaces 
              it by the value at offset m from the removed pointer. Note 
              m can be negative in which case the offset is deeper into 
              the stack.

     STORE_O m
              Removes the stack pointer on top of the stack and the 
              cell below and replaces the cell at offset m (can be 
              positive or negative) from the removed
              stack pointer by the removed cell.

     ALLOC   n
              Creates n void cells on top of the stack.  To deallocate 
              n cells from the top of the stack use a negative number.
              Removes the top n cells from the stack.

(4) Data manipulation on heap

     LOAD_H
              Removes the heap pointer on top of the stack and replaces 
              it by the heap record (which may be multiple cells).

     LOAD_HO  m
              Removes the heap pointer on top of the stack and replaces 
              it by the value at offset m from the removed pointer in 
              the heap. Note m cannot be negative nor can it be outside 
              the heap block which is being accessed.

     STORE_H  n
              Creates a heap record of the top n cells of the stack 
              and leaves a heap pointer to those cells on top of the 
              stack.

     STORE_HO m
              Removes the heap pointer on top of the stack and the 
              cell below and replaces the cell at offset m from the removed
              heap pointer by the removed cell.

     ALLOC_H  n
              Creates a block of n void cells on top of the heap and places 
              the heap pointer to them on the top of the stack.    


(4) Arithmetic, boolean, and character operations

     APP <operation>
              Removes the top one/two integer/boolean values and replaces them 
              with the resulting of applying the operation.

              Values of <operation> are:
              (a) Integer operations:
                         ADD SUB DIV MUL   arity 2
                         NEG               arity 1
              (b) comparison operations on integers
                         LT,LE,GT,GE,EQ    arity 2
              (c) logical operatios
                         AND,OR            arity 2
                         NOT               arity 1
              (d) comparison operators for characters
                         LT_C,LE_C,GT_C,
                         GE_C,EQ_C         arity 2

     READ_I   Read integer value onto top of stack.
     READ_B   Read boolean value onto top of stack.
     READ_C   Reads a character from the input buffer onto the top 
              of the stack.
     PRINT_I  Removes top integer value of stack and prints it.
     PRINT_B  Removes top boolean value of stack and prints it.
     PRINT_C  Removes the top character value of the stack and prints it.

(6) Labels 

    Any instruction can be labelled. Labels must start with a lower case 
    letter and can be followed by any sequence of lowercase letters, digits, 
    or underscore.  A labelled instruction is an instruction so a given 
    basic instruction (i.e. one described above) can have multiple labels.
     
    <label> : <instruction>
         ( {label} = [a-z][_a-z0-9]* )

    A given line may contain many instructions: a label refers to the 
    first basic instruction it preceeds. 

(7) Constants

    The machine recognizes three sorts of constant: integer, boolean, 
    and character.
                                                  examples:
    Integer:  -?[0-9]+                            12, -47, 102
    Boolean: "true"|"false"                       true, false
    Character: ["]([a-zA-Z0-9\ ]|"\n")["]         "a","X"," ","\n"




Synopsis of instructions
------------------------

   Notice that there are the following types of instruction
       LOAD  --- this is loading something onto the stack/heap
       STORE --- this is storing something on (or near) the top of the 
                 stack/heap somewhere.
          _R --- means registers (needs register argument)
          _O --- means stack offset (needs integer argument 
                 and stack pointer on top of the stack)
          _H  -- means heap (for storing needs a positive integer)
                 and the exchange of the heap record for a heap pointer 
                 on top of the stack.
          _HO -- means heap offset (needs a positive integer argument 
                 and heap pointer on top of the stack)
          _I --- means an integer constant (needs integer argument)
          _B --- means a boolean constant (needs boolean argument)
          _C --- means a character constant

       JUMP - this alters the program counter (possibly)
          _C --- conditional on the top of stack
             --- absolute jump
          _S --- to code pointer on top of stack
          _O --- advances the code pointer.

       APP  - this applies an operation to the top few elements of the stack
              (needs operation argument).

       ALLOC       - allocating and deallocating space on the stack
                     (needs integer)
       ALLOC_H     - allocates space on the heap
                     (needs positive integer)
       PRINT_I/B/C, READ_I/B/C - ineract with outside world!
 


Activation record layout for m+ function
----------------------------------------

When a function is called in m+ here is the arrangement of the 
activation record:


                BEFORE                             AFTER

         +----------------------+             +----------------------+
         |   argument 1         |             |   return value       |
         +----------------------+             +----------------------+
         .                      .             |   return pointer     |
         .                      .             +----------------------+
         +----------------------+
         |   argument n         | 
         +----------------------+
         |      (void)          |
         +----------------------+
         |   static link        |
         +----------------------+
         |   dynamic link       |
   FP    +----------------------+
  -->    |   return pointer     |
         +----------------------+
         |   local var 1        |
         +----------------------+
         .                      .
         .                      .
         +----------------------+
         |   local var m        |
         +----------------------+

Caller/callee responsibilities
------------------------------

In writing the code for creating an activation record the one tricky thing is 
to assign responsibilities correctly.  The caller MUST BE responsible for 
     (i)   setting the "return" code pointer
     (ii)  setting the access link for the called routine 
as (i) only the caller knows where the code should be continued (ii) 
only the caller knows how many (static) levels the place of the call 
to the function is from the point of definition of the function.

In contrast either the caller or the callee can save the current frame 
pointer and reset it to the top of stack.  Here we make the former the 
responsibility of the caller and the latter the responsibility of the 
callee.

Thus, entry into a m+ function requires: 

(1) load the arguments on the stack                 
    (caller code: usual postfix evaluation)
(2) caller starts the function call sequence:
  (a) creates space for a return value (this is vital if the function 
      has no arguments: the caller is conservative and allocates the 
      space anyway!).
      (caller code: ALLOC 1
      this creates a "void" stack element)
  (b) calculates the access pointer
      (caller code LOAD_R %fp LOAD_O -2 LOAD_O -2
  (c) save the current frame  pointer - dynamic link
      (caller code: LOAD_R %fp)
  (d) loads the (return) program counter on the stack                            
      (caller code: STORE_R %cp
       this advances the program counter one step and saves it on the stack
       recall that loading a code pointer will also advances it ...)
  (e) call the function
      (caller code: JUMP function_label)
(3) function finishes the call sequence:
  (a) Sets the frame pointer to top of stack
      (LOAD_R %sp STORE_R %fp)
  (b) Allocate the local storage 
    (function code: ALLOC m)
(6) continue with the function body.


Return from a function involves:
(7) Write the return value to the first argument slot. We assume 
the return value is at the top stack value:
   (function code: LOAD_R %fp 
                   STORE_O -(n+3) )
(8) Write the return pointer to the second argument slot
   (function code: LOAD_R %fp 
                   LOAD_O 0
                   LOAD_R %fp
                   STORE_O -(n+2) )
(9) Deallocate the local storage including the return pointer
    (function code: DEALLOC m+1) 
(10) Restore the old frame pointers
    (function code: STORE_R %fp)
(11) Clean up the argument storage leaving the return pointer and return 
     value on the stack
    (function code: ALLOC -n)
(12) Do a jump based on the code address on the stack ...
    (function code: JUMP_S )  


Collecting this code we have:

(a) caller code:
                   .....
                   ALLOC 1                   % void on stack
                   LOAD_R %fp                % set the dynamic link
                   LOAD_R %fp                % 
                   LOAD_O -1                 % calculate static link
                   LOAD_O -1                 %
                   STORE_R %cp               % save program counter
                   JUMP <function label>     % jump to function code
                   ......

(b) function code:

<function label> : LOAD_R  %sp
                   STORE_R %fp               % set new FP to top stack element
                   ALLOC m                   % allocate m void cells
                   ......


                   ......                    % value of result loaded
                   LOAD_R %fp                % load frame pointer
                   STORE -(n+3)              % store <result> at begining
                   LOAD_R %fp                % load frame pointer
                   LOAD_O 0                  % load <return>  
                   LOAD_R %fp              
                   STORE_O -(n+2)            % place <return> below <result>
                   ALLOC -(m+2)              % remove top m+2 stack elements
                   STORE_R %fp               % restore old frame pointer
                   ALLOC -n                  % remove top n stack elements
                   JUMP_S                    & jump to top of stack code address 



Example of a recursive program call ...
---------------------------------------

Here is a complete example involving non-local references and a recursive 
function call.  The m+ program is (roughly!):

          var x:int;
          var y:int;
          fun exp(b:int):int
          { var z:int;
            begin if b=0 then z:= 1
                  else z:= x * exp(b-1);
            return z;
            end
          }
          begin
            read x; 
            read y;
            print exp(y);
          end

This should translate into the following m+ assembler (which you should be 
able to run on the am+ machine simulator:

            LOAD_R %sp                % access pointer for program
            LOAD_R %sp                % set the frame pointer for prog
            STORE_R %fp
            ALLOC 2                   % allocate space for variables
            READ                      % read and store x
            LOAD_R %fp
            STORE_O 1
            READ                      % read and store y
            LOAD_R %fp
            STORE_O 2
            LOAD_R %fp                %  load y
            LOAD_O 2
            ALLOC 1                   % void on stack for return value
            LOAD_R %fp                % static link
            LOAD_R %fp                % dynamic link
            LOAD_R %cp                % save program counter
            JUMP fun_exp              % call function
            PRINT                     % print result
            ALLOC -3                  % deallocate stack
            HALT                      %  HALT!
% code for function exp ...
fun_exp :   LOAD_R  %sp
            STORE_R %fp               % set new %fp to top stack element
            ALLOC 1                   % allocate 1 void cell for local var
            LOAD_R %fp
            LOAD_O -4                 % load argument of fuction
            LOAD_I 0           
            APP EQ                    % test whether zero
            JUMP_C  label1            %  conditional 
            LOAD_I 1
            LOAD_R %fp                % set z to 1
            STORE_O 1
            JUMP label2
label1 :    LOAD_R %fp
            LOAD_O -2                 % access x by chasing static link once
            LOAD_O 1
            LOAD_R %fp
            LOAD_O -4                 % load argument b
            LOAD_I 1
            APP SUB                   % obtain b-1
            ALLOC 1
            LOAD_R %fp                % access link
            LOAD_O -2
            LOAD_R %fp                % dynamic link
            LOAD_R %cp                % save program counter
            JUMP fun_exp              % call function recursively
            APP MUL
            LOAD_R %fp
            STORE_O 1                 % store result in z
label2:     LOAD_R %fp
            LOAD_O 1                  % load z again!
            LOAD_R %fp                % load frame pointers
            STORE_O -4                % store <result> at begining
            LOAD_R %fp                % load frame pointer
            LOAD_O 0                  % load <return>  
            LOAD_R %fp               
            STORE_O -3                % place <return> below <result>
            ALLOC -2                  % remove top m+1 stack elements
            STORE_R %fp               % restore old frame pointer
            ALLOC -1                  % remove top n stack elements
            JUMP_S                    % jump to top of stack code address 



Activation record layout for minisculus+ block
----------------------------------------------

Before entering a block the frame pointer is sitting below the local 
variables allocated.  Entry into a block simply consists of advancing the 
stack pointer enough to allocate the required space for the local variables.
Leaving a block will return a stack of the same size (hopefully with some 
entries modified)

             BEFORE and AFTER !                 In the block

  FP     +----------------------+   FP    +----------------------+
  -->    |   returm pointer     |  -->    |   return pointer     |
         +----------------------+         +----------------------+
         |   argument 1         |         |   argument 1         |
         +----------------------+         +----------------------+
         .                      .         .                      .
         .                      .         .                      .
         +----------------------+         +----------------------+
         |   argument n         |         |   argument n         |
         +----------------------+         +----------------------+
         |   local var 1        |         |   local var 1        |
         +----------------------+         +----------------------+
         .                      .         .                      .
         .                      .         .                      .
         +----------------------+         +----------------------+
         |   local var m        |         |   local var m        |
         +----------------------+         +----------------------+
                                          |   block var m+1      |
                                          +----------------------+
                                          .                      .
                                          .                      .
                                          +----------------------+ 
                                          |   block var m + p    |
                                          +----------------------+

Notice the frame pointer is not altered so that the compiler has to 
calculate variable addresses in the block relative to that frame pointer.

On exit from the block the only action is to decrement the stack pointer 
by p the number of cells the block allocated.  No jump is required.

Entry into block:
(a) allocate the required local storage  (block)
           ALLOC p
(b) continue execution (block)

Exit from block:
(a) deallocate the local storage by decrementing the stack pointer  (block)
           ALLOC -p
(b) continue execution (caller)

Notice the fact that functions are declared does not affect the storage 
allocation: they are compiled separately ...

An alternative implementation is to actually build an activation record.
No return pointer is required (so it is set to void) but the return 
code restoring the frame pointer and removing storage is required. 
This implementation does allow a more uniform manner of accessing 
variables but is less efficient.

