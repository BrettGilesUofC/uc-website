#CPSC411 -- Datatypes for M+ in python



#Python classes corresponding to 
#Haskell datatype for the syntax tree:


#data M_prog = M_prog ([M_decl],[M_stmt])

class prog:
      def __init__(self,decs,stmts):
            self.decs = decs  # a list
            self.stmts = stmts # a list

      def __repr__(self):
            "Possible representation(prettyprint) included in this one only."
            rep="["
            for dec in self.decs: rep = rep + dec.__repr__()+","
            rep = rep[:-2] +"],["
            for stmt in self.stmts: rep = rep + stmt.__repr__()+","
            rep = rep[:-2] +"]"
            return "prog("+rep + ")"

      
#data M_decl = M_var (string,M_type)
#            | M_fun (string ,[(string,M_type)],M_type,M_prog)

class decl:
      def __init__(self,type,varid=none,vartype=none,
                   funid=none,funargdecs=none,funtype=none,funprog=none):
            self.type=type   # VAR or FUN
            self.varid=varid  # the id
            self.vartype=vartype # Bool or Int

            self.funid=funid #The id in case it's a function
            self.funargdecs=funargdecs # a list of argument declarations
            self.funtype=funtype
            self.funprog=funprog  # an instance of prog


            
#data M_stmt = M_ass (string,M_expr)
#            | M_while (M_expr,M_stmt)
#            | M_cond (M_expr,M_stmt,M_stmt) 
#            | M_read string
#            | M_print M_expr
#            | M_return M_expr
#            | M_block ([M_decl],[M_stmt])

class stmt:
      def __init__(self):
            "A base class to make life easier - use it or do it like decl."
            pass

class ass_stmt(stmt):
      def __init__(self,id,exp):
            self.id=id
            self.exp = exp

class while_stmt(stmt):
      def __init__(self,exp,stmt):
            self.exp = exp
            self.stmt = stmt

class cond_stmt(stmt):
      def __init__(self,exp,tstmt,fstmt):
            self.exp = exp
            self.tstmt=tstmt
            self.fstmt=fstmt

class read_stmt(stmt):
      def __init__(self,id):
            self.id=id

class print_stmt(stmt):
      def __init__(self,exp):
            self.exp=exp

class ret_stmt(stmt):
      def __init__(self,exp):
            self.exp=exp


class block_stmt(stmt):
      def __init__(self,decs,stmts):
            self.decs = decs
            self.stmts = stmts

      
#data M_type = M_int | M_bool
# Not needed in python - use a string or enumerated value  if you want.

#data M_expr = M_num Int
#            | M_bl Bool
#            | M_id String
#            | M_app (M_operation,[M_expr])

class expr:
      def __init__(self, numv=none, bval=none, id=none, apply=none):
            self.numv=numv
            self.bval=bval
            self.id=id
            self.apply=apply

#data M_operation =  M_fn String | M_add | M_mul | M_sub | M_div | M_neg
#                | M_lt | M_le | M_gt | M_ge | M_eq | M_not | M_and | M_or;

class operation:
      def __init__(self,func=none,arithop=none,compop=none,boolop=none):
            self.func=func
            self.arithop=arithop
            self.compop=compop
            self.boolop=boolop
#
#
#Here is an M+ program:
#
#============================================================
#          var x:int;
#          var y:int;
#          fun exponent(b:int):int
#          { var z:int;
#            begin if b=0 then z:= 1
#                  else z:= x * exponent(b-1);
#           return z;
#           end};
#          begin
#            read x; 
#            read y;
#            print exponent(y);
#          end
#============================================================
#
#Here is its syntax tree: 
#
# 
# 
#  mprog==(decs,stmts)
#  mprog.decs==[("VAR","x","INT"),("VAR","y","INT"),
#                   ("FUN","exponent",[("b","INT")],"INT", funprog)]

#  mprog.decs[2].funprog==(decs,stmts)
#  mprog.decs[2].funprog.decs==[("VAR""z","INT")]

#  mprog.decs[2].funprog.stmts[0] is cond_stmt
#  mprog.decs[2].funprog.stmts[0].expr==('=',["b",0])  ### expr.apply
#  mprog.decs[2].funprog.stmts[0].tstmt==("z",1)  ### assign statement
#  mprog.decs[2].funprog.stmts[0].fstmt==("z",('*',["x",("exponent",("-",["b",1]))]))  ### expr.apply
#
#  mprog.decs[2].funprog.stmts[0] is ret_stmt
#  mprog.decs[2].funprog.stmts[0] =="z"  

#  mprog.stmts[0] is read_stmt
#  mprog.stmts[0].id == "x"

#  mprog.stmts[1] is read_stmt
#  mprog.stmts[1].id == "y"


#  mprog.stmts[2] is print_stmt
#  mprog.stmts[2].exp == ("exponent",["y"])
#
#



# Following not yet translated to PYTHON as it is for 3rd and 4th assignments.


#Finally let us briefly consider what the intermediate representation will be 
#for the stack machine.  As mentioned before it will be essentially the 
#same except each variable name will be replaced by a pair of integers 
#(the levels from static declaration and the offset in the activation record) 
#and each function call by the level from its static declaration and the 
#label used for the call.
#
#The new Haskell datatype may look like:
#
#data I_prog  = IPROG    ([I_fbody],Int,[I_stmt])
#data I_fbody = IFUN     (string,[I_fbody],Int,[I_stmt])
#data I_stmt = IASS      (Int,Int,I_expr)
#            | IWHILE    (I_expr,I_stmt)
#            | ICOND     (I_expr,I_stmt,I_stmt)
#            | iREAD_I   (Int,Int)
#            | iPRINT_I  I_expr
#            | iREAD_B   (Int,Int)
#            | iPRINT_B  I_expr
#            | iRETURN   I_expr
#            | iBLOCK    ([I_fbody],Int,[I_stmt])
#data I_expr = INUM      Int
#            | IBOOL     Bool
#            | IID       (Int,Int)
#            | IAPP      (I_opn,[I_expr])
#data I_opn = ICALL      (String,Int)
#           | IADD | IMUL | ISUB | IDIV | INEG
#           | ILT  | ILE  | IGT  | IGE  | IEQ 
#           | INOT | IAND | IOR;
#
#Notice that the declarations have gone (they are actually "used" in the 
#construction of the symbol tables as they are built for use in the various 
#parts of the tree) they are replaced by the bodies of the functions.
#
#The functions are supplied with a label (to which the caller jumps),
#the number of arguments it expects and the code of the body which is 
#a program block.  A program block indicates how many cells it is 
#necessary to allocate for local storage.  These are numbers needed in 
#the generation of the entry and return code sequence of the function.
#
#The call of a function requires the label and the number of levels. 
#While all variable are replaced by the offset and level calculated from 
#the symbol table.
#
#
#The intermediate representation is:
#
#  IPROG
#    ([IFUN
#        ("fn1"
#        ,[]
#        ,1
#        ,[ICOND(IAPP (IEQ,[IID (0,-4),INUM 0])
#         ,IASS(0,1,INUM 1)
#         ,IASS(0,1,IAPP(IMUL,[IID (1,1)
#                             ,IAPP(ICALL ("fn1",1),[IAPP(ISUB,[IID (0,-4)
#                                                             ,INUM 1])])])))
#         ,IRETURN (IID (0,1))
#         ])]
#    ,2
#    ,[IREAD_I (0,1)
#     ,IREAD_I (0,2)
#     ,IPRINT_I (IAPP (ICALL ("fn1",0),[IID (0,2)]))
#     ])
#
#From here it is relatively easy to generate the required stack machine code.
