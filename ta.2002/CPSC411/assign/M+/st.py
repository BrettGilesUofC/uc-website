
stab=[ {"vars":2,"args":0,"dict":{ "x":("var","int",1),
             "f":("fun",["int","int"],"int",'code_label_f'),
             "v":("var","int",2)}},
     {"vars":1,"args":2,"dict":{ "y":("var","int",-5),
             "z":("var","int",-4),
             "x":("var","int",1),
             "g":("fun",["int","int"],"int","code_label_g")}}, 
    {"vars":1,"args":2,"dict":{ "a":("var","int",-5),
             "b":("var","int",-4),
             "y":("var","int",1)}}
    ]

def lookup(symt, lk):
      for level in range(len(symt)-1,-1,-1):
            if symt[level]["dict"].has_key(lk):
                  if symt[level]["dict"][lk][0] == "var":
                        return (level, symt[level]["dict"][lk][1],symt[level]["dict"][lk][2])
                  else:
                         return (level, symt[level]["dict"][lk][1],symt[level]["dict"][lk][2],symt[level]["dict"][lk][3])


def insert(symt,symv):
      '''Does not handle overloading!, symv=('var'|'arg',symname,type) or
      ('fun',symname,arglisttypes,type)'''
      symentry=symt[len(symt)-1] # ease of use
      if symentry['dict'].has_key(symv[1]):
            raise "Duplicate error"
      if symv[0] == 'arg': #nb, requires reversing order of args as passed in.
            symentry['dict'][symv[1]]=('var',-(4+symentry['args']),symv[2])
            symentry['args']+=1
      if symv[0] == 'var':
            symentry['vars']+=1
            symentry['dict'][symv[1]]=('var',symentry['vars'],symv[2])
      if symv[0] == 'fun':
            symentry['dict'][symv[1]]=('fun',symv[2],symv[3],getlabel( symv[1]))


def getlabel(s):
      return s
