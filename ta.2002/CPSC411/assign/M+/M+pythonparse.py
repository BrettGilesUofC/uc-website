#
# PSEUDO CODE - YOU MAY NEED TO MAKE MANY MANY CHANGES FOR THIS TO WORK!
#
#  - Items not done: Names for most rules
#                    Checking what the comparision of tokens really is (i.e. to type,...)
#
# Assumes:  You are using python data spec as on this website and
#           plex and pyacc as from links on this website.

def p_empty(t):
      'empty:'
      pass

def p_prog(t):
      'prog:block'
      t[0]=t[1]



      'block:declarations programbody'
      t[0]=prog(t[1],t[2])



      'programbody:BEGIN progstmts END'
      t[0]=t[2]


def p_progstmts(t):
      'progstmts:progstmt SEMICOLON progstmts'
      t[0]=[t[1]]+t[3] # catenate the first element onto the front of the list


def p_progstmts_empty(t):
      'progstmts:empty'
      t[0]=[]



      '''progstmt:IF expr THEN progstmt ELSE progstmt
                 |READ ID
                 |WHILE expr DO progstmt
                 |...'''
      if t[1]==IF:  #May not be the exact comparison syntax needed...
            t[0]=cond_stmt(t[2],t[4],t[6])
      elif t[1] == READ:  #May not be the exact comparison syntax needed...
            t[0]=read_stmt(t[2])
            ...



      'declarations:empty'
      t[0]=[]

      'declarations:declaration SEMICOLON declarations'
      t[0]=[t[1]]+t[3]



      '''declaration:var_declaration
                    | fun_declaration'''
      t[0]=t[1]


      'var_declaration: VAR basicvardeclaration'
      t[0]=decl('var',varid=(t[2])[0], vartype=t[2][1])



      'basicvardeclaration:identifier COLON type'
      t[0]=(t[1],t[2])




      'fundec:FUN identifier paramlist COLON type CLPAR funblock CRPAR'
      t[0]=decl('fun', funid=t[2],funargdecs=t[3],funtype=t[5],funprog=t[7])



      'funblock:declarations fun_body'
      t[0]=prog(t[1],t[2])


      'fun_body: BEGIN progstmts RETURN expr SEMICOLON END'
      rstmt=ret_stmt(t[4])
      t[0]=t[2].append(rstmt)

      
