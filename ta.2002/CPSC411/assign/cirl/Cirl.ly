\section{Parser module}\label{sec:parser}

\begin{verbatim}

> {
> module Cirl where
> import Tokens
> }

\end{verbatim} 
First thing to declare is the name of your parser,
and the type of the tokens the parser reads.

\begin{verbatim}

> %name cirl
> %tokentype { Token }
>
> %token
>	CRL		{TkCirl _}

	SEP		{TkSep _}

>	MEM		{TkMem _}
>	CAST		{TkCast _}
>	CONST		{TkConst _}
>	GOTO		{TkGoto _}
>	GLOBAL		{TkGlobal _}
>	FUNCDEF		{TkFuncDef _}
>	IF  		{TkIf _}
>	TYPE		{TkType _ $$}

\end{verbatim} 
See the tokens section for a description of type.
\begin{verbatim}

>	'('	 	  {TkSym _ $$}
>	')' 		  {TkSym _ $$}
>	'['	 	  {TkSym _ $$}
>	']' 		  {TkSym _ $$}
>	'{'	 	  {TkSym _ $$}
>	'}' 		  {TkSym _ $$}
>	','	 	  {TkSym _ $$}
>	';' 		  {TkSym _ $$}
>	':' 		  {TkSym _ $$}
>	BKSL 		  {TkSym _ '\\'}
>	'$' 		  {TkSym _ $$}
>	ASSIGN	 	  {TkAssign _ }
>	MOVE 		  {TkMove _ }
>	TEMP	 	  {TkTemp _ $$ }
>	REF 		  {TkRef _ $$ }
>	LABEL	 	  {TkLabel _ $$ }
>	IRFUN 		  {TkFun _ $$ }
>	NUM 		  {TkNum _ $$ }
>
>	"+" 		  {TkOpn _ $$}
>	"-" 		  {TkOpn _ $$}
>	"*" 		  {TkOpn _ $$}
>	"/" 		  {TkOpn _ $$}
>	"=" 		  {TkOpn _ $$}
>	"<" 		  {TkOpn _ $$}
>	">" 		  {TkOpn _ $$}
>	MOD		  {TkOpn _ "mod"}
>	AND 		  {TkOpn _ "&&"}
>	OR 		  {TkOpn _ "||"}
>	NOT 		  {TkOpn _ "not"}
>	XOR 		  {TkOpn _ "xor"}
>	NEQ 		  {TkOpn _ "=/="}
>	LEQ 		  {TkOpn _ "=<"}
>	GEQ 		  {TkOpn _ ">="}
>	LS 		  {TkOpn _ "<<"}
>	RS 		  {TkOpn _ ">>"}
> %%


\end{verbatim} 


The left hand side are the names of the terminals or tokens,
and the right hand side is how to pattern match them.


Now we have the production rules.

> irprog	:	program_id  data_segment function_segment stmt 
> 		{ Irprog $1 $2 $3 $4 }

The first part, program id consists of a token, 'CIRL' followed by an identifier - known
as a 'temp' token.

> program_id : CRL TEMP {ProgId $2}

Note that in data_spec_list we reverse the order of the list as found. Not important as
these are just definitions. Similarly, we do the same reversal for function definitions.
A data_spec is defined as an id followed by a type. Examples would be:

\begin{verbatim}

i int;
j bool;

> data_segment : GLOBAL '[' data_spec_list ']' {$2}
> data_spec_list ::{[DataSpec]}
> data_spec_list : {- empty -}      {[]}
> 	| data_spec_list ';' data_spec  {$2 : $1}
> data_spec : TEMP TYPE { Dspec $1 $2}

\end{verbatim} 
Next we have the function area, which consists of a list of function definitions. 
These are typically a name, the keyword function in square brakets, an optional list
of arguments in square brackets and a cirl statement in parenthesis. 

First the list\ldots
\begin{verbatim}

> function_segment :: {[FunctionSpec]}
> function_segment :{- empty -}      {[]}
> 	| function_segment ';' function_spec  {$2 : $1} 

\end{verbatim} 
Then the function spec itself.
\begin{verbatim}

> function_spec : TEMP '[' FUNCDEF ']' '[' argument_list ']' '(' stmts ')'  {Function $1 $9 $6}
>	| TEMP '[' FUNCDEF ']'  '(' stmts ')'   	{Function $1 $6 []}

\end{verbatim} 
Now we begin the exciting parts, the statment definitions. There are 6 basic statments in
CIRL and as usual, a list of statements is a statment. As such, stmt will always return
a list of statements, which naturally, may be empty or the singleton. 

Unlike functions and data specifications though, we need to keep the list of statements
in the same order we have found them in. As such we do the horrible thing of 
appending each new statement to the end of the current list  
- THIS IS SOMETHING TO FIX - FIND OUT HOW TO 
REVERSE ONCE AT END OF PARSE!!!!

\begin{verbatim}

>

> stmts::{[Statement]}
> stmts:  {- empty -}  {[]}
>        | stmts ';' stmt {$1 ++ [$3]}
>
> stmt::{Statement}
> stmt   : LABEL  {Label $1}
>        | pointer_exp MOVE exp  {Move $1 $3}
>        | TEMP ASSIGN exp  {Assign $1 $3}
>        | GOTO LABEL  {Jump $2}
>        | IF exp GOTO LABEL  {Cjump $2 $4}

\end{verbatim} 
Pointer expressions are a special type of expression.

\begin{verbatim}

> pointer_exp::{PointerExp}
> pointer_exp : REF {Ref $1}
>     | TEMP {PtrTemp $1}
>     | pointer_exp BKSL exp {PtrCompound $1 $3}
 
\end{verbatim} 

Next, the other large data type, expression.

\begin{verbatim}

> exp 	: MEM '[' pointer_exp ']' ':' TYPE   	{Mem $3 %6}
>	| IRFUN argument_list			{Efunc $1 $2}
>     	| type_op argument_list			{Op $1 $2 }
>	| CONST typespec '(' value ')'		{Econst $2 $4}
>	| '{' stmts '}' exp			{Eseq $2 $4 }
>	| TEMP					{Etemp $1}

\end{verbatim} 

Type operations are typically coercions to a specific type.

\begin{verbatim}

> type_op :: {TypeOperation}
> type_op : LABEL typespec		{Label $1 $2 }
>         | opn typespec		{Topn $1 $2}

This creates a conflict. Why is it here in the first place?
	  | CONST typespec		{Tconst $2}

>	  | CAST typespec		{Tcast $2}
>
> typespec::{TypeSpec}
> typespec : '[' typespec_codom ']'     {Codomain $2}
>

> typespec_codom::{TypeSpecCodomain}
> typespec_codom : TYPE ':' typespec_dom  {CodomDom $1 $3 }
>		| TYPE				{CodomType $1}
>	    
> typespec_dom::{TypeSpecDomain}
> typespec_dom : TYPE ',' typespec_dom  {DomDom $1 $3}
> 		| TYPE			{DomType $1}

\end{verbatim} 

The operations are the standard set: plus, minus etc.

\begin{verbatim}

> opn::{TypeOp}
> opn: "+" 		{Add}
>	| "-"		{Sub}
>	| "*"		{Mul}
>	| "/"		{Div}
>	| "="		{Teq}
>	| "<"		{Tlt}
>	| ">"		{Tgt}
>	| MOD		{Mod}
>	| AND		{And}
>	| OR 		{Or}
>	| NOT		{Not}
>	| XOR		{Xor}
>	| NEQ		{Tneq}
>	| LEQ		{Tle}
>	| GEQ		{Tge}
>	| LS 		{Tls}
>	| RS		{Trs}

\end{verbatim} 

\begin{verbatim}

> value::{Int}
> value:NUM 	{$1}
>

\end{verbatim} 

\begin{verbatim}

> argument_list::{[Expression]}
> argument_list: '(' arguments ')' 	{reverse $2}

> arguments::{[Expression]}
> arguments: {- empty -}    		{[]}
>          | arguments1			{$1}

> arguments1::{[Expression]}
> arguments1 : exp			{[$1]}
>       | arguments1 ',' exp		{$3:$1} 

>
>

\end{verbatim} 


An Irprog is a program id, a data segment, a function segment and a statment.

> {
> happyError :: [Token] -> a
> happyError _ = error "Parse error"
>

> data Irprog = Irprog ProgramId DataSegment FunctionSegment Stmt

Program id is just a string. Semantically we ignore the "CIRL"

> data ProgramId = ProgId String

Data segment consistst of a list of data specs, each of which consists of two strings, the
identifier and the type.

> data DataSpec = Dspec String String 

A function specification is it's name, a CIRL stmt and a possibly empty argument list.
\begin{verbatim}

> data FunctionSpec = Function String Statement [Argument] 

\end{verbatim} 
For the statement, we have a much more complicated data type.
\begin{verbatim}

> data Statement = Label String
>      | Assign String Expression
>      | Jump String
>      | Cjump Expression String
>      | Move Expression Expression
>      | Eval Expression		   	

\end{verbatim} 

Our pointer datatype is recursive.

\begin{verbatim}

> data PointerExp = Ref String | PtrTemp String
>       | PtrCompound PointerExp Expression

\end{verbatim} 

The expression data type allows for memory pointing, function calls, constants,
statements in front of the expression or temporaries.

\begin{verbatim}

> data Expression = 	Mem PointerExp String
>	  	|	Efunc String Arguments
>		|	Op TypeOp Arguments
>		|	Econst TypeSpec Int
>		| 	Eseq [Statement] Expression	
>		| 	Etemp String

\end{verbatim} 

For type specifications, we create three data types to differentiate
between the general type specification, the co-domain and the domain.

\begin{verbatim}

> data TypeSpec = 	Codomain TypeSpecCodomain
>
> data TypeSpecCodomain = 	CodomDom String TypeSpecDomain
>		|	CodomType String

> data TypeSpecDomain = 	DomDom String TypeSpecDomain
>		|	DomType String

\end{verbatim} 

The operation type is a set of enumerated values, corresponding to the
possible tokens.

\begin{verbatim}

> data TypeOp = Add | Sub | Mul | Div | Mod | And | Or
>	| Not | Xor | Teq | Tneq | Tlt | Tgt | Tle
>	| Tge | Tls | Trs
>	deriving (Show, Enum)
>
> }