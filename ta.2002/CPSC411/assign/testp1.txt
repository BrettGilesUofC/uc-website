/* a single do statment, some calculations */
do
begin
  read x;
  y := (x+7)*4-3;
  print y*2;
  read x;
end
until x