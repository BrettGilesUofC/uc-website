#CPSC411 -- Datatypes for M+ in python


#data M_prog = M_prog ([M_decl],[M_stmt])

class prog:
      def __init__(self,decs,stmts):
            self.decs = decs  # a list
            self.stmts = stmts # a list

      def __repr__(self):
            "Possible representation(prettyprint) included in this one only."
            rep="["
            for dec in self.decs: rep = rep + dec.__repr__()+","
            rep = rep[:-1] +"],["
            for stmt in self.stmts: rep = rep + stmt.__repr__()+","
            rep = rep[:-1] +"]"
            return "prog("+rep + ")"

      
#data M_decl = M_var (string,M_type)
#            | M_fun (string ,[(string,M_type)],M_type,M_prog)

class decl:
      def __init__(self,type,varid=None,vartype=None,
                   funid=None,funargdecs=None,funtype=None,funprog=None):
            self.type=type   # VAR or FUN
            self.varid=varid  # the id
            self.vartype=vartype # Bool or Int

            self.funid=funid #The id in case it's a function
            self.funargdecs=funargdecs # a list of argument declarations
            self.funtype=funtype
            self.funprog=funprog  # an instance of prog

      def __repr__(self):
            ret=""
            if self.type =="var":
                  return "(VAR:"+self.varid+","+self.vartype+")"
            else:
                  ret = ret+ "(Fun: "+self.funid +",["
                  for d in self.funargdecs:
                        ret = ret+ d[0]+':'+d[1] +","
                  ret = ret[:-1] + "],"+self.funtype +"," + self.funprog.__repr__()+")"
                  return ret
            
#data M_stmt = M_ass (string,M_expr)
#            | M_while (M_expr,M_stmt)
#            | M_cond (M_expr,M_stmt,M_stmt) 
#            | M_read string
#            | M_print M_expr
#            | M_return M_expr
#            | M_block ([M_decl],[M_stmt])

class stmt:
      def __init__(self):
            "A base class to make life easier - use it or do it like decl."
            pass

class ass_stmt(stmt):
      def __init__(self,id,exp):
            self.id=id
            self.exp = exp

      def __repr__(self):
            return "(Assign:,"+self.id+","+self.exp.__repr__()+")"

class while_stmt(stmt):
      def __init__(self,exp,stmt):
            self.exp = exp
            self.stmt = stmt
            
      def __repr__(self):
            return "(While:,"+self.exp.__repr__()+","+self.stmt.__repr__()+")"

class cond_stmt(stmt):
      def __init__(self,exp,tstmt,fstmt):
            self.exp = exp
            self.tstmt=tstmt
            self.fstmt=fstmt

      def __repr__(self):
            return "(If:,"+self.exp.__repr__()+","+self.tstmt.__repr__()+","+self.fstmt.__repr__()+")"

class read_stmt(stmt):
      def __init__(self,id):
            self.id=id

      def __repr__(self):
            return "(Read:,"+self.id+")"

class print_stmt(stmt):
      def __init__(self,exp):
            self.exp=exp
            
      def __repr__(self):
            return "(Print,"+self.exp.__repr__()+")"


class ret_stmt(stmt):
      def __init__(self,exp):
            self.exp=exp

      def __repr__(self):
            return "(Return,"+self.exp.__repr__()+")"

class block_stmt(stmt):
      def __init__(self,decs,stmts):
            self.decs = decs
            self.stmts = stmts

      def __repr__(self):
            rep="["
            for dec in self.decs: rep = rep + dec.__repr__()+","
            rep = rep[:-1] +"],["
            for stmt in self.stmts: rep = rep + stmt.__repr__()+","
            rep = rep[:-1] +"]"
            return "block("+rep + ")"

      
#data M_type = M_int | M_bool
# Not needed in python - use a string or enumerated value  if you want.

#data M_expr = M_num Int
#            | M_bl Bool
#            | M_id String
#            | M_app (M_operation,[M_expr])

class expr:
      def __init__(self, numv=None, bval=None, id=None, apply=None):
            self.numv=numv
            self.bval=bval
            self.id=id
            self.apply=apply

      def __repr__(self):
            if self.numv <>None :
                  return "Num=%d"%self.numv
            if self.bval <>None : return "Bool=%d"%self.bval
            if self.id <>None : return "ID="+self.id
            #else, apply
            
            appfn=self.apply[0]
            appfnparms=self.apply[1]
            ret = "(Apply %s to [%s])"%(appfn,appfnparms.__repr__())
            return ret

#data M_operation =  M_fn String | M_add | M_mul | M_sub | M_div | M_neg
#                | M_lt | M_le | M_gt | M_ge | M_eq | M_not | M_and | M_or;

class args:
      def __init__(self,arglist,exist="yes"):
            self.arglist=arglist
            self.exist=exist
            

      def notthere(self):
            return self.exist=="no"

      def __repr__(self):
            rep = ""
            for a in self.arglist:
                  rep = rep + a.__repr__()+","

            return rep[:-1]
                  
