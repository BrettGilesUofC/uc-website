

import lex

# Reserved words
reserved = (
    'BEGIN','DO', 'ELSE', 'END', 'FALSE', 'FUN', 'IF', 'INT', 'BOOL', 'NOT', 'PRINT', 'READ', 'RETURN', 'THEN', 'TRUE', 'VAR', 'WHILE'
    )

tokens = reserved + (
    # Literals (identifier, integer constant)
    'ID', 'NUM', 'BVAL',

    # Operators (+,-,*,/,||,&&, <, =<, >, >=, =)
    'ADD', 'SUB', 'MULT', 'DIV', 
    'OR', 'AND',
    'LT', 'LE', 'GT', 'GE', 'EQUAL',
    
    # Assignment (:=)
    'ASSIGN',
    
    # Delimeters ( ) { } , ; :
    'LPAR', 'RPAR',
    'CLPAR', 'CRPAR',
    'COMMA', 'SEMICOLON', 'COLON'
    )

# Completely ignored characters
t_ignore           = ' \t\x0c'

# Newlines
def t_NEWLINE(t):
    r'\n+'
    t.lineno += t.value.count("\n")
    
# Operators
t_ADD             = r'\+'
t_SUB            = r'-'
t_MULT          = r'\*'
t_DIV           = r'\/'
t_OR               = r'\|\|'
t_AND              = r'&&'
t_LT               = r'<'
t_GT               = r'>'
t_LE               = r'=<'
t_GE               = r'>='
t_EQUAL               = r'='

# Assignment operators

t_ASSIGN           = r':='

# Delimeters
t_LPAR          = r'\('
t_RPAR         = r'\)'
t_CLPAR           = r'\{'
t_CRPAR           = r'\}'
t_COMMA            = r','
t_SEMICOLON             = r';'
t_COLON            = r':'

# Identifiers and reserved words

reserved_map = { }
for r in reserved:
    reserved_map[r.lower()] = r

def t_BVAL(t):
    r'true|false'
    if t.value=='true': t.value=1
    else: t.value=0
    return t

def t_ID(t):
    r'[A-Za-z_][\w_]*'
    t.type = reserved_map.get(t.value,"ID")
    return t

# Integer literal
def t_NUM(t):
    r'\d+'
    t.value=int(t.value)
    return t

# Comments
def t_comment(t):
    r'%(.)*'

    
def t_error(t):
    print "Illegal character %s" % repr(t.value[0])
    t.skip(1)
    
lex.lex()
#if __name__ == "__main__":
#    lex.runmain(lexer)

    



