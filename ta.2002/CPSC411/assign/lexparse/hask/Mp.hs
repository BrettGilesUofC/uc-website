-- parser produced by Happy Version 1.11

module Mp where
import Tokens

data HappyAbsSyn t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 t14 t15 t16 t17 t18 t19 t20 t21 t22 t23 t24 t25 t26 t27 t28 t29 t30 t31
	= HappyTerminal Token
	| HappyErrorToken Int
	| HappyAbsSyn4 t4
	| HappyAbsSyn5 t5
	| HappyAbsSyn6 t6
	| HappyAbsSyn7 t7
	| HappyAbsSyn8 t8
	| HappyAbsSyn9 t9
	| HappyAbsSyn10 t10
	| HappyAbsSyn11 t11
	| HappyAbsSyn12 t12
	| HappyAbsSyn13 t13
	| HappyAbsSyn14 t14
	| HappyAbsSyn15 t15
	| HappyAbsSyn16 t16
	| HappyAbsSyn17 t17
	| HappyAbsSyn18 t18
	| HappyAbsSyn19 t19
	| HappyAbsSyn20 t20
	| HappyAbsSyn21 t21
	| HappyAbsSyn22 t22
	| HappyAbsSyn23 t23
	| HappyAbsSyn24 t24
	| HappyAbsSyn25 t25
	| HappyAbsSyn26 t26
	| HappyAbsSyn27 t27
	| HappyAbsSyn28 t28
	| HappyAbsSyn29 t29
	| HappyAbsSyn30 t30
	| HappyAbsSyn31 t31

action_0 (43) = happyShift action_7
action_0 (44) = happyShift action_8
action_0 (4) = happyGoto action_9
action_0 (5) = happyGoto action_2
action_0 (6) = happyGoto action_3
action_0 (7) = happyGoto action_4
action_0 (8) = happyGoto action_5
action_0 (10) = happyGoto action_6
action_0 _ = happyReduce_4

action_1 (43) = happyShift action_7
action_1 (44) = happyShift action_8
action_1 (5) = happyGoto action_2
action_1 (6) = happyGoto action_3
action_1 (7) = happyGoto action_4
action_1 (8) = happyGoto action_5
action_1 (10) = happyGoto action_6
action_1 _ = happyFail

action_2 _ = happyReduce_1

action_3 (38) = happyShift action_15
action_3 (16) = happyGoto action_14
action_3 _ = happyFail

action_4 (51) = happyShift action_13
action_4 _ = happyFail

action_5 _ = happyReduce_5

action_6 _ = happyReduce_6

action_7 (68) = happyShift action_12
action_7 (9) = happyGoto action_11
action_7 _ = happyFail

action_8 (68) = happyShift action_10
action_8 _ = happyFail

action_9 (69) = happyAccept
action_9 _ = happyFail

action_10 (46) = happyShift action_27
action_10 (12) = happyGoto action_26
action_10 _ = happyFail

action_11 _ = happyReduce_7

action_12 (52) = happyShift action_25
action_12 _ = happyFail

action_13 (43) = happyShift action_7
action_13 (44) = happyShift action_8
action_13 (6) = happyGoto action_24
action_13 (7) = happyGoto action_4
action_13 (8) = happyGoto action_5
action_13 (10) = happyGoto action_6
action_13 _ = happyReduce_4

action_14 _ = happyReduce_2

action_15 (32) = happyShift action_18
action_15 (34) = happyShift action_19
action_15 (36) = happyShift action_20
action_15 (40) = happyShift action_21
action_15 (48) = happyShift action_22
action_15 (68) = happyShift action_23
action_15 (18) = happyGoto action_16
action_15 (19) = happyGoto action_17
action_15 _ = happyReduce_21

action_16 (39) = happyShift action_53
action_16 _ = happyFail

action_17 (51) = happyShift action_52
action_17 _ = happyFail

action_18 (46) = happyShift action_43
action_18 (54) = happyShift action_44
action_18 (62) = happyShift action_45
action_18 (66) = happyShift action_46
action_18 (67) = happyShift action_47
action_18 (68) = happyShift action_48
action_18 (20) = happyGoto action_51
action_18 (21) = happyGoto action_38
action_18 (22) = happyGoto action_39
action_18 (24) = happyGoto action_40
action_18 (26) = happyGoto action_41
action_18 (28) = happyGoto action_42
action_18 _ = happyFail

action_19 (46) = happyShift action_43
action_19 (54) = happyShift action_44
action_19 (62) = happyShift action_45
action_19 (66) = happyShift action_46
action_19 (67) = happyShift action_47
action_19 (68) = happyShift action_48
action_19 (20) = happyGoto action_50
action_19 (21) = happyGoto action_38
action_19 (22) = happyGoto action_39
action_19 (24) = happyGoto action_40
action_19 (26) = happyGoto action_41
action_19 (28) = happyGoto action_42
action_19 _ = happyFail

action_20 (68) = happyShift action_49
action_20 _ = happyFail

action_21 (46) = happyShift action_43
action_21 (54) = happyShift action_44
action_21 (62) = happyShift action_45
action_21 (66) = happyShift action_46
action_21 (67) = happyShift action_47
action_21 (68) = happyShift action_48
action_21 (20) = happyGoto action_37
action_21 (21) = happyGoto action_38
action_21 (22) = happyGoto action_39
action_21 (24) = happyGoto action_40
action_21 (26) = happyGoto action_41
action_21 (28) = happyGoto action_42
action_21 _ = happyFail

action_22 (43) = happyShift action_7
action_22 (44) = happyShift action_8
action_22 (5) = happyGoto action_36
action_22 (6) = happyGoto action_3
action_22 (7) = happyGoto action_4
action_22 (8) = happyGoto action_5
action_22 (10) = happyGoto action_6
action_22 _ = happyReduce_4

action_23 (65) = happyShift action_35
action_23 _ = happyFail

action_24 _ = happyReduce_3

action_25 (41) = happyShift action_33
action_25 (42) = happyShift action_34
action_25 (15) = happyGoto action_32
action_25 _ = happyFail

action_26 (52) = happyShift action_31
action_26 _ = happyFail

action_27 (68) = happyShift action_12
action_27 (9) = happyGoto action_28
action_27 (13) = happyGoto action_29
action_27 (14) = happyGoto action_30
action_27 _ = happyReduce_13

action_28 _ = happyReduce_15

action_29 (47) = happyShift action_80
action_29 _ = happyFail

action_30 (50) = happyShift action_79
action_30 _ = happyReduce_12

action_31 (41) = happyShift action_33
action_31 (42) = happyShift action_34
action_31 (15) = happyGoto action_78
action_31 _ = happyFail

action_32 _ = happyReduce_8

action_33 _ = happyReduce_16

action_34 _ = happyReduce_17

action_35 (46) = happyShift action_43
action_35 (54) = happyShift action_44
action_35 (62) = happyShift action_45
action_35 (66) = happyShift action_46
action_35 (67) = happyShift action_47
action_35 (68) = happyShift action_48
action_35 (20) = happyGoto action_77
action_35 (21) = happyGoto action_38
action_35 (22) = happyGoto action_39
action_35 (24) = happyGoto action_40
action_35 (26) = happyGoto action_41
action_35 (28) = happyGoto action_42
action_35 _ = happyFail

action_36 (49) = happyShift action_76
action_36 _ = happyFail

action_37 (61) = happyShift action_56
action_37 _ = happyReduce_26

action_38 (60) = happyShift action_75
action_38 _ = happyReduce_29

action_39 _ = happyReduce_31

action_40 (53) = happyShift action_68
action_40 (54) = happyShift action_69
action_40 (57) = happyShift action_70
action_40 (58) = happyShift action_71
action_40 (59) = happyShift action_72
action_40 (63) = happyShift action_73
action_40 (64) = happyShift action_74
action_40 (23) = happyGoto action_66
action_40 (25) = happyGoto action_67
action_40 _ = happyReduce_34

action_41 (55) = happyShift action_64
action_41 (56) = happyShift action_65
action_41 (27) = happyGoto action_63
action_41 _ = happyReduce_41

action_42 _ = happyReduce_45

action_43 (46) = happyShift action_43
action_43 (54) = happyShift action_44
action_43 (62) = happyShift action_45
action_43 (66) = happyShift action_46
action_43 (67) = happyShift action_47
action_43 (68) = happyShift action_48
action_43 (20) = happyGoto action_62
action_43 (21) = happyGoto action_38
action_43 (22) = happyGoto action_39
action_43 (24) = happyGoto action_40
action_43 (26) = happyGoto action_41
action_43 (28) = happyGoto action_42
action_43 _ = happyFail

action_44 (46) = happyShift action_43
action_44 (54) = happyShift action_44
action_44 (66) = happyShift action_46
action_44 (67) = happyShift action_47
action_44 (68) = happyShift action_48
action_44 (28) = happyGoto action_61
action_44 _ = happyFail

action_45 (46) = happyShift action_43
action_45 (54) = happyShift action_44
action_45 (62) = happyShift action_45
action_45 (66) = happyShift action_46
action_45 (67) = happyShift action_47
action_45 (68) = happyShift action_48
action_45 (22) = happyGoto action_60
action_45 (24) = happyGoto action_40
action_45 (26) = happyGoto action_41
action_45 (28) = happyGoto action_42
action_45 _ = happyFail

action_46 _ = happyReduce_50

action_47 _ = happyReduce_51

action_48 (46) = happyShift action_59
action_48 (29) = happyGoto action_58
action_48 _ = happyReduce_54

action_49 _ = happyReduce_24

action_50 (35) = happyShift action_57
action_50 (61) = happyShift action_56
action_50 _ = happyFail

action_51 (33) = happyShift action_55
action_51 (61) = happyShift action_56
action_51 _ = happyFail

action_52 (32) = happyShift action_18
action_52 (34) = happyShift action_19
action_52 (36) = happyShift action_20
action_52 (40) = happyShift action_21
action_52 (48) = happyShift action_22
action_52 (68) = happyShift action_23
action_52 (18) = happyGoto action_54
action_52 (19) = happyGoto action_17
action_52 _ = happyReduce_21

action_53 _ = happyReduce_18

action_54 _ = happyReduce_20

action_55 (32) = happyShift action_18
action_55 (34) = happyShift action_19
action_55 (36) = happyShift action_20
action_55 (40) = happyShift action_21
action_55 (48) = happyShift action_22
action_55 (68) = happyShift action_23
action_55 (19) = happyGoto action_93
action_55 _ = happyFail

action_56 (46) = happyShift action_43
action_56 (54) = happyShift action_44
action_56 (62) = happyShift action_45
action_56 (66) = happyShift action_46
action_56 (67) = happyShift action_47
action_56 (68) = happyShift action_48
action_56 (21) = happyGoto action_92
action_56 (22) = happyGoto action_39
action_56 (24) = happyGoto action_40
action_56 (26) = happyGoto action_41
action_56 (28) = happyGoto action_42
action_56 _ = happyFail

action_57 (32) = happyShift action_18
action_57 (34) = happyShift action_19
action_57 (36) = happyShift action_20
action_57 (40) = happyShift action_21
action_57 (48) = happyShift action_22
action_57 (68) = happyShift action_23
action_57 (19) = happyGoto action_91
action_57 _ = happyFail

action_58 _ = happyReduce_49

action_59 (46) = happyShift action_43
action_59 (54) = happyShift action_44
action_59 (62) = happyShift action_45
action_59 (66) = happyShift action_46
action_59 (67) = happyShift action_47
action_59 (68) = happyShift action_48
action_59 (20) = happyGoto action_88
action_59 (21) = happyGoto action_38
action_59 (22) = happyGoto action_39
action_59 (24) = happyGoto action_40
action_59 (26) = happyGoto action_41
action_59 (28) = happyGoto action_42
action_59 (30) = happyGoto action_89
action_59 (31) = happyGoto action_90
action_59 _ = happyReduce_56

action_60 _ = happyReduce_32

action_61 _ = happyReduce_52

action_62 (47) = happyShift action_87
action_62 (61) = happyShift action_56
action_62 _ = happyFail

action_63 (46) = happyShift action_43
action_63 (54) = happyShift action_44
action_63 (66) = happyShift action_46
action_63 (67) = happyShift action_47
action_63 (68) = happyShift action_48
action_63 (28) = happyGoto action_86
action_63 _ = happyFail

action_64 _ = happyReduce_46

action_65 _ = happyReduce_47

action_66 (46) = happyShift action_43
action_66 (54) = happyShift action_44
action_66 (66) = happyShift action_46
action_66 (67) = happyShift action_47
action_66 (68) = happyShift action_48
action_66 (24) = happyGoto action_85
action_66 (26) = happyGoto action_41
action_66 (28) = happyGoto action_42
action_66 _ = happyFail

action_67 (46) = happyShift action_43
action_67 (54) = happyShift action_44
action_67 (66) = happyShift action_46
action_67 (67) = happyShift action_47
action_67 (68) = happyShift action_48
action_67 (26) = happyGoto action_84
action_67 (28) = happyGoto action_42
action_67 _ = happyFail

action_68 _ = happyReduce_42

action_69 _ = happyReduce_43

action_70 _ = happyReduce_35

action_71 _ = happyReduce_36

action_72 _ = happyReduce_37

action_73 _ = happyReduce_38

action_74 _ = happyReduce_39

action_75 (46) = happyShift action_43
action_75 (54) = happyShift action_44
action_75 (62) = happyShift action_45
action_75 (66) = happyShift action_46
action_75 (67) = happyShift action_47
action_75 (68) = happyShift action_48
action_75 (22) = happyGoto action_83
action_75 (24) = happyGoto action_40
action_75 (26) = happyGoto action_41
action_75 (28) = happyGoto action_42
action_75 _ = happyFail

action_76 _ = happyReduce_27

action_77 (61) = happyShift action_56
action_77 _ = happyReduce_25

action_78 (48) = happyShift action_82
action_78 _ = happyFail

action_79 (68) = happyShift action_12
action_79 (9) = happyGoto action_81
action_79 _ = happyFail

action_80 _ = happyReduce_11

action_81 _ = happyReduce_14

action_82 (43) = happyShift action_7
action_82 (44) = happyShift action_8
action_82 (6) = happyGoto action_97
action_82 (7) = happyGoto action_4
action_82 (8) = happyGoto action_5
action_82 (10) = happyGoto action_6
action_82 (11) = happyGoto action_98
action_82 _ = happyReduce_4

action_83 _ = happyReduce_30

action_84 (55) = happyShift action_64
action_84 (56) = happyShift action_65
action_84 (27) = happyGoto action_63
action_84 _ = happyReduce_40

action_85 (53) = happyShift action_68
action_85 (54) = happyShift action_69
action_85 (25) = happyGoto action_67
action_85 _ = happyReduce_33

action_86 _ = happyReduce_44

action_87 _ = happyReduce_48

action_88 (61) = happyShift action_56
action_88 _ = happyReduce_58

action_89 (47) = happyShift action_96
action_89 _ = happyFail

action_90 (50) = happyShift action_95
action_90 _ = happyReduce_55

action_91 _ = happyReduce_23

action_92 (60) = happyShift action_75
action_92 _ = happyReduce_28

action_93 (37) = happyShift action_94
action_93 _ = happyFail

action_94 (32) = happyShift action_18
action_94 (34) = happyShift action_19
action_94 (36) = happyShift action_20
action_94 (40) = happyShift action_21
action_94 (48) = happyShift action_22
action_94 (68) = happyShift action_23
action_94 (19) = happyGoto action_103
action_94 _ = happyFail

action_95 (46) = happyShift action_43
action_95 (54) = happyShift action_44
action_95 (62) = happyShift action_45
action_95 (66) = happyShift action_46
action_95 (67) = happyShift action_47
action_95 (68) = happyShift action_48
action_95 (20) = happyGoto action_102
action_95 (21) = happyGoto action_38
action_95 (22) = happyGoto action_39
action_95 (24) = happyGoto action_40
action_95 (26) = happyGoto action_41
action_95 (28) = happyGoto action_42
action_95 _ = happyFail

action_96 _ = happyReduce_53

action_97 (38) = happyShift action_101
action_97 (17) = happyGoto action_100
action_97 _ = happyFail

action_98 (49) = happyShift action_99
action_98 _ = happyFail

action_99 _ = happyReduce_9

action_100 _ = happyReduce_10

action_101 (32) = happyShift action_18
action_101 (34) = happyShift action_19
action_101 (36) = happyShift action_20
action_101 (40) = happyShift action_21
action_101 (48) = happyShift action_22
action_101 (68) = happyShift action_23
action_101 (18) = happyGoto action_104
action_101 (19) = happyGoto action_17
action_101 _ = happyReduce_21

action_102 (61) = happyShift action_56
action_102 _ = happyReduce_57

action_103 _ = happyReduce_22

action_104 (45) = happyShift action_105
action_104 _ = happyFail

action_105 (46) = happyShift action_43
action_105 (54) = happyShift action_44
action_105 (62) = happyShift action_45
action_105 (66) = happyShift action_46
action_105 (67) = happyShift action_47
action_105 (68) = happyShift action_48
action_105 (20) = happyGoto action_106
action_105 (21) = happyGoto action_38
action_105 (22) = happyGoto action_39
action_105 (24) = happyGoto action_40
action_105 (26) = happyGoto action_41
action_105 (28) = happyGoto action_42
action_105 _ = happyFail

action_106 (51) = happyShift action_107
action_106 (61) = happyShift action_56
action_106 _ = happyFail

action_107 (39) = happyShift action_108
action_107 _ = happyFail

action_108 _ = happyReduce_19

happyReduce_1 = happySpecReduce_1 4 happyReduction_1
happyReduction_1 (HappyAbsSyn5  happy_var_1)
	 =  HappyAbsSyn4
		 (happy_var_1
	)
happyReduction_1 _  = notHappyAtAll 

happyReduce_2 = happySpecReduce_2 5 happyReduction_2
happyReduction_2 (HappyAbsSyn16  happy_var_2)
	(HappyAbsSyn6  happy_var_1)
	 =  HappyAbsSyn5
		 (Mprog (happy_var_1) (happy_var_2)
	)
happyReduction_2 _ _  = notHappyAtAll 

happyReduce_3 = happySpecReduce_3 6 happyReduction_3
happyReduction_3 (HappyAbsSyn6  happy_var_3)
	_
	(HappyAbsSyn7  happy_var_1)
	 =  HappyAbsSyn6
		 (happy_var_1:happy_var_3
	)
happyReduction_3 _ _ _  = notHappyAtAll 

happyReduce_4 = happySpecReduce_0 6 happyReduction_4
happyReduction_4  =  HappyAbsSyn6
		 ([]::[Mdecl]
	)

happyReduce_5 = happySpecReduce_1 7 happyReduction_5
happyReduction_5 (HappyAbsSyn8  happy_var_1)
	 =  HappyAbsSyn7
		 (happy_var_1
	)
happyReduction_5 _  = notHappyAtAll 

happyReduce_6 = happySpecReduce_1 7 happyReduction_6
happyReduction_6 (HappyAbsSyn10  happy_var_1)
	 =  HappyAbsSyn7
		 (happy_var_1
	)
happyReduction_6 _  = notHappyAtAll 

happyReduce_7 = happySpecReduce_2 8 happyReduction_7
happyReduction_7 (HappyAbsSyn9  happy_var_2)
	_
	 =  HappyAbsSyn8
		 (happy_var_2
	)
happyReduction_7 _ _  = notHappyAtAll 

happyReduce_8 = happySpecReduce_3 9 happyReduction_8
happyReduction_8 (HappyAbsSyn15  happy_var_3)
	_
	(HappyTerminal (TkIdentifier happy_var_1))
	 =  HappyAbsSyn9
		 (Mvar happy_var_1 happy_var_3
	)
happyReduction_8 _ _ _  = notHappyAtAll 

happyReduce_9 = happyReduce 8 10 happyReduction_9
happyReduction_9 (_ `HappyStk`
	(HappyAbsSyn11  happy_var_7) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn15  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn12  happy_var_3) `HappyStk`
	(HappyTerminal (TkIdentifier happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn10
		 (Mfun happy_var_2 happy_var_3 happy_var_5 happy_var_7
	) `HappyStk` happyRest

happyReduce_10 = happySpecReduce_2 11 happyReduction_10
happyReduction_10 (HappyAbsSyn17  happy_var_2)
	(HappyAbsSyn6  happy_var_1)
	 =  HappyAbsSyn11
		 (Mprog happy_var_1 happy_var_2
	)
happyReduction_10 _ _  = notHappyAtAll 

happyReduce_11 = happySpecReduce_3 12 happyReduction_11
happyReduction_11 _
	(HappyAbsSyn13  happy_var_2)
	_
	 =  HappyAbsSyn12
		 (happy_var_2
	)
happyReduction_11 _ _ _  = notHappyAtAll 

happyReduce_12 = happySpecReduce_1 13 happyReduction_12
happyReduction_12 (HappyAbsSyn14  happy_var_1)
	 =  HappyAbsSyn13
		 (reverse happy_var_1
	)
happyReduction_12 _  = notHappyAtAll 

happyReduce_13 = happySpecReduce_0 13 happyReduction_13
happyReduction_13  =  HappyAbsSyn13
		 ([]::[Mdecl]
	)

happyReduce_14 = happySpecReduce_3 14 happyReduction_14
happyReduction_14 (HappyAbsSyn9  happy_var_3)
	_
	(HappyAbsSyn14  happy_var_1)
	 =  HappyAbsSyn14
		 (happy_var_3:happy_var_1
	)
happyReduction_14 _ _ _  = notHappyAtAll 

happyReduce_15 = happySpecReduce_1 14 happyReduction_15
happyReduction_15 (HappyAbsSyn9  happy_var_1)
	 =  HappyAbsSyn14
		 ([happy_var_1]
	)
happyReduction_15 _  = notHappyAtAll 

happyReduce_16 = happySpecReduce_1 15 happyReduction_16
happyReduction_16 _
	 =  HappyAbsSyn15
		 (Mint
	)

happyReduce_17 = happySpecReduce_1 15 happyReduction_17
happyReduction_17 _
	 =  HappyAbsSyn15
		 (Mbool
	)

happyReduce_18 = happySpecReduce_3 16 happyReduction_18
happyReduction_18 _
	(HappyAbsSyn18  happy_var_2)
	_
	 =  HappyAbsSyn16
		 (happy_var_2
	)
happyReduction_18 _ _ _  = notHappyAtAll 

happyReduce_19 = happyReduce 6 17 happyReduction_19
happyReduction_19 (_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn20  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn18  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn17
		 (happy_var_2 ++ [Mreturn happy_var_4]
	) `HappyStk` happyRest

happyReduce_20 = happySpecReduce_3 18 happyReduction_20
happyReduction_20 (HappyAbsSyn18  happy_var_3)
	_
	(HappyAbsSyn19  happy_var_1)
	 =  HappyAbsSyn18
		 (happy_var_1:happy_var_3
	)
happyReduction_20 _ _ _  = notHappyAtAll 

happyReduce_21 = happySpecReduce_0 18 happyReduction_21
happyReduction_21  =  HappyAbsSyn18
		 ([]::[Mstmt]
	)

happyReduce_22 = happyReduce 6 19 happyReduction_22
happyReduction_22 ((HappyAbsSyn19  happy_var_6) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn19  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn20  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn19
		 (Mcond happy_var_2 happy_var_4 happy_var_6
	) `HappyStk` happyRest

happyReduce_23 = happyReduce 4 19 happyReduction_23
happyReduction_23 ((HappyAbsSyn19  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn20  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn19
		 (Mwhile happy_var_2 happy_var_4
	) `HappyStk` happyRest

happyReduce_24 = happySpecReduce_2 19 happyReduction_24
happyReduction_24 (HappyTerminal (TkIdentifier happy_var_2))
	_
	 =  HappyAbsSyn19
		 (Mread happy_var_2
	)
happyReduction_24 _ _  = notHappyAtAll 

happyReduce_25 = happySpecReduce_3 19 happyReduction_25
happyReduction_25 (HappyAbsSyn20  happy_var_3)
	_
	(HappyTerminal (TkIdentifier happy_var_1))
	 =  HappyAbsSyn19
		 (Mass happy_var_1 happy_var_3
	)
happyReduction_25 _ _ _  = notHappyAtAll 

happyReduce_26 = happySpecReduce_2 19 happyReduction_26
happyReduction_26 (HappyAbsSyn20  happy_var_2)
	_
	 =  HappyAbsSyn19
		 (Mprint happy_var_2
	)
happyReduction_26 _ _  = notHappyAtAll 

happyReduce_27 = happySpecReduce_3 19 happyReduction_27
happyReduction_27 _
	(HappyAbsSyn5  happy_var_2)
	_
	 =  HappyAbsSyn19
		 (prgtoblk (happy_var_2)
	)
happyReduction_27 _ _ _  = notHappyAtAll 

happyReduce_28 = happySpecReduce_3 20 happyReduction_28
happyReduction_28 (HappyAbsSyn21  happy_var_3)
	_
	(HappyAbsSyn20  happy_var_1)
	 =  HappyAbsSyn20
		 (Mapp Mor [happy_var_1,happy_var_3]
	)
happyReduction_28 _ _ _  = notHappyAtAll 

happyReduce_29 = happySpecReduce_1 20 happyReduction_29
happyReduction_29 (HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn20
		 (happy_var_1
	)
happyReduction_29 _  = notHappyAtAll 

happyReduce_30 = happySpecReduce_3 21 happyReduction_30
happyReduction_30 (HappyAbsSyn22  happy_var_3)
	_
	(HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn21
		 (Mapp Mand [happy_var_1,happy_var_3]
	)
happyReduction_30 _ _ _  = notHappyAtAll 

happyReduce_31 = happySpecReduce_1 21 happyReduction_31
happyReduction_31 (HappyAbsSyn22  happy_var_1)
	 =  HappyAbsSyn21
		 (happy_var_1
	)
happyReduction_31 _  = notHappyAtAll 

happyReduce_32 = happySpecReduce_2 22 happyReduction_32
happyReduction_32 (HappyAbsSyn22  happy_var_2)
	_
	 =  HappyAbsSyn22
		 (Mapp Mnot [happy_var_2]
	)
happyReduction_32 _ _  = notHappyAtAll 

happyReduce_33 = happySpecReduce_3 22 happyReduction_33
happyReduction_33 (HappyAbsSyn24  happy_var_3)
	(HappyAbsSyn23  happy_var_2)
	(HappyAbsSyn24  happy_var_1)
	 =  HappyAbsSyn22
		 (Mapp happy_var_2 [happy_var_1,happy_var_3]
	)
happyReduction_33 _ _ _  = notHappyAtAll 

happyReduce_34 = happySpecReduce_1 22 happyReduction_34
happyReduction_34 (HappyAbsSyn24  happy_var_1)
	 =  HappyAbsSyn22
		 (happy_var_1
	)
happyReduction_34 _  = notHappyAtAll 

happyReduce_35 = happySpecReduce_1 23 happyReduction_35
happyReduction_35 _
	 =  HappyAbsSyn23
		 (Meq
	)

happyReduce_36 = happySpecReduce_1 23 happyReduction_36
happyReduction_36 _
	 =  HappyAbsSyn23
		 (Mlt
	)

happyReduce_37 = happySpecReduce_1 23 happyReduction_37
happyReduction_37 _
	 =  HappyAbsSyn23
		 (Mgt
	)

happyReduce_38 = happySpecReduce_1 23 happyReduction_38
happyReduction_38 _
	 =  HappyAbsSyn23
		 (Mle
	)

happyReduce_39 = happySpecReduce_1 23 happyReduction_39
happyReduction_39 _
	 =  HappyAbsSyn23
		 (Mge
	)

happyReduce_40 = happySpecReduce_3 24 happyReduction_40
happyReduction_40 (HappyAbsSyn26  happy_var_3)
	(HappyAbsSyn25  happy_var_2)
	(HappyAbsSyn24  happy_var_1)
	 =  HappyAbsSyn24
		 (Mapp happy_var_2 [happy_var_1,happy_var_3]
	)
happyReduction_40 _ _ _  = notHappyAtAll 

happyReduce_41 = happySpecReduce_1 24 happyReduction_41
happyReduction_41 (HappyAbsSyn26  happy_var_1)
	 =  HappyAbsSyn24
		 (happy_var_1
	)
happyReduction_41 _  = notHappyAtAll 

happyReduce_42 = happySpecReduce_1 25 happyReduction_42
happyReduction_42 _
	 =  HappyAbsSyn25
		 (Madd
	)

happyReduce_43 = happySpecReduce_1 25 happyReduction_43
happyReduction_43 _
	 =  HappyAbsSyn25
		 (Msub
	)

happyReduce_44 = happySpecReduce_3 26 happyReduction_44
happyReduction_44 (HappyAbsSyn28  happy_var_3)
	(HappyAbsSyn27  happy_var_2)
	(HappyAbsSyn26  happy_var_1)
	 =  HappyAbsSyn26
		 (Mapp happy_var_2 [happy_var_1,happy_var_3]
	)
happyReduction_44 _ _ _  = notHappyAtAll 

happyReduce_45 = happySpecReduce_1 26 happyReduction_45
happyReduction_45 (HappyAbsSyn28  happy_var_1)
	 =  HappyAbsSyn26
		 (happy_var_1
	)
happyReduction_45 _  = notHappyAtAll 

happyReduce_46 = happySpecReduce_1 27 happyReduction_46
happyReduction_46 _
	 =  HappyAbsSyn27
		 (Mmul
	)

happyReduce_47 = happySpecReduce_1 27 happyReduction_47
happyReduction_47 _
	 =  HappyAbsSyn27
		 (Mdiv
	)

happyReduce_48 = happySpecReduce_3 28 happyReduction_48
happyReduction_48 _
	(HappyAbsSyn20  happy_var_2)
	_
	 =  HappyAbsSyn28
		 (happy_var_2
	)
happyReduction_48 _ _ _  = notHappyAtAll 

happyReduce_49 = happySpecReduce_2 28 happyReduction_49
happyReduction_49 (HappyAbsSyn29  happy_var_2)
	(HappyTerminal (TkIdentifier happy_var_1))
	 =  HappyAbsSyn28
		 (idorapp happy_var_1 happy_var_2
	)
happyReduction_49 _ _  = notHappyAtAll 

happyReduce_50 = happySpecReduce_1 28 happyReduction_50
happyReduction_50 (HappyTerminal (TkNum happy_var_1))
	 =  HappyAbsSyn28
		 (Mnum happy_var_1
	)
happyReduction_50 _  = notHappyAtAll 

happyReduce_51 = happySpecReduce_1 28 happyReduction_51
happyReduction_51 (HappyTerminal (TkBoolean happy_var_1))
	 =  HappyAbsSyn28
		 (Mbl happy_var_1
	)
happyReduction_51 _  = notHappyAtAll 

happyReduce_52 = happySpecReduce_2 28 happyReduction_52
happyReduction_52 (HappyAbsSyn28  happy_var_2)
	_
	 =  HappyAbsSyn28
		 (Mapp Mneg [happy_var_2]
	)
happyReduction_52 _ _  = notHappyAtAll 

happyReduce_53 = happySpecReduce_3 29 happyReduction_53
happyReduction_53 _
	(HappyAbsSyn30  happy_var_2)
	_
	 =  HappyAbsSyn29
		 (happy_var_2
	)
happyReduction_53 _ _ _  = notHappyAtAll 

happyReduce_54 = happySpecReduce_0 29 happyReduction_54
happyReduction_54  =  HappyAbsSyn29
		 ([]::[Mexpr]
	)

happyReduce_55 = happySpecReduce_1 30 happyReduction_55
happyReduction_55 (HappyAbsSyn31  happy_var_1)
	 =  HappyAbsSyn30
		 (reverse happy_var_1
	)
happyReduction_55 _  = notHappyAtAll 

happyReduce_56 = happySpecReduce_0 30 happyReduction_56
happyReduction_56  =  HappyAbsSyn30
		 ([]::[Mexpr]
	)

happyReduce_57 = happySpecReduce_3 31 happyReduction_57
happyReduction_57 (HappyAbsSyn20  happy_var_3)
	_
	(HappyAbsSyn31  happy_var_1)
	 =  HappyAbsSyn31
		 (happy_var_3:happy_var_1
	)
happyReduction_57 _ _ _  = notHappyAtAll 

happyReduce_58 = happySpecReduce_1 31 happyReduction_58
happyReduction_58 (HappyAbsSyn20  happy_var_1)
	 =  HappyAbsSyn31
		 ([happy_var_1]
	)
happyReduction_58 _  = notHappyAtAll 

happyNewToken action sts stk [] =
	action 69 69 (error "reading EOF!") (HappyState action) sts stk []

happyNewToken action sts stk (tk:tks) =
	let cont i = action i i tk (HappyState action) sts stk tks in
	case tk of {
	TkIf -> cont 32;
	TkThen -> cont 33;
	TkWhile -> cont 34;
	TkDo -> cont 35;
	TkRead -> cont 36;
	TkElse -> cont 37;
	TkBegin -> cont 38;
	TkEnd -> cont 39;
	TkPrint -> cont 40;
	TkInt -> cont 41;
	TkBool -> cont 42;
	TkVar -> cont 43;
	TkFun -> cont 44;
	TkReturn -> cont 45;
	TkSym '(' -> cont 46;
	TkSym ')' -> cont 47;
	TkSym '{' -> cont 48;
	TkSym '}' -> cont 49;
	TkSym ',' -> cont 50;
	TkSym ';' -> cont 51;
	TkSym ':' -> cont 52;
	TkOpn  "+" -> cont 53;
	TkOpn  "-" -> cont 54;
	TkOpn  "*" -> cont 55;
	TkOpn  "/" -> cont 56;
	TkOpn  "=" -> cont 57;
	TkOpn  "<" -> cont 58;
	TkOpn  ">" -> cont 59;
	TkOpn  "&&" -> cont 60;
	TkOpn  "||" -> cont 61;
	TkOpn  "not" -> cont 62;
	TkOpn  "=<" -> cont 63;
	TkOpn  ">=" -> cont 64;
	TkAssign -> cont 65;
	TkNum happy_dollar_dollar -> cont 66;
	TkBoolean happy_dollar_dollar -> cont 67;
	TkIdentifier happy_dollar_dollar -> cont 68;
	}

happyThen = (thenE)
happyReturn = (returnE)
happyThen1 m k tks = (thenE) m (\a -> k a tks)
happyReturn1 = \a tks -> (returnE) a

mp tks = happyThen (happyParse action_0 tks) (\x -> case x of {HappyAbsSyn4 z -> happyReturn z; _other -> notHappyAtAll })

happyError t = failE "Parse error"

data Mprog = Mprog  [Mdecl]  [Mstmt]
	
instance Show Mprog where
   show (Mprog a b) = "MProg("++showList a (", "++ showList b ")")
data Mdecl = Mvar  String  Mtype
              | Mfun  String  [Mdecl]  Mtype Mprog
instance Show Mdecl where
   show (Mvar a b) = "( Var , "++a++", "++show b++")"
   show (Mfun a bs c d) = "( Fun, "++a++", "++showList bs (", "++show c++", "++show d++")")
data Mstmt = Mass  String  Mexpr
              | Mwhile  Mexpr  Mstmt
              | Mcond  Mexpr  Mstmt  Mstmt 
              | Mread  String
              | Mprint  Mexpr
              | Mreturn  Mexpr
              | Mblock  [Mdecl]  [Mstmt]
instance Show Mstmt where
    show (Mass s e) = "(Assign, "++s++"=("++show e++"))"
    show (Mwhile e st) = "(While, "++show e++", "++show st++")"
    show (Mcond e s1 s2) = "(Cond, "++show e++", "++show s1++", "++show s2++")"
    show (Mread s) = "(Read, "++s++")"
    show (Mprint e) = "(Print, "++show e++")"
    show (Mreturn e) = "(Return, "++show e++")"
    show (Mblock ds ss) = "(Block, "++showList ds (","++showList ss ")")


data Mtype = Mint | Mbool
instance Show Mtype where
   show (Mint) = "Mint"
   show (Mbool) = "Mbool"
data Mexpr = Mnum  Int
              | Mbl  Bool
              | Mid  String
              | Mapp  Moperation  [Mexpr]
instance  Show Mexpr where
    show (Mnum i) = "Num="++show i
    show (Mbl b) = "Bool="++show b
    show (Mid s) = "ID="++s
    show (Mapp op es) = "(Apply ,"++show op++", "++showList es ")"

data Moperation 
              = Mfn  String
              | Madd | Mmul | Msub | Mdiv | Mneg
              | Mlt | Mle | Mgt | Mge | Meq | Mneq | Mnot | Mand | Mor
instance Show Moperation where
    show (Mfn s) = s
    show (Madd) = "Madd"
    show  (Mmul)= "Mmul"
    show  (Msub)= "Msub"
    show  (Mdiv)= "Mdiv"
    show  (Mneg)= "Mneg"
    show  (Mlt)= "Mlt"
    show  (Mle)= "Mle"
    show  (Mgt)= "Mgt"
    show  (Mge)= "Mge"
    show  (Meq)= "Meq"
    show  (Mneq)= "Mneq"
    show  (Mnot)= "Mnot"
    show  (Mand)= "Mand"
    show  (Mor)= "Mor"

data E a = Ok a | Failed String
instance (Show a)=> Show (E a) where
   show (Ok a) = show a
   show (Failed s) = "Error:"++s

thenE:: E a -> ( a -> E b) -> E b
m `thenE` k =
    case m of
         Ok a -> k a
         Failed e -> Failed e

returnE:: a -> E a
returnE a = Ok a

failE::String -> E a
failE err = Failed err

applyE::E a -> (a -> b) ->  E b
applyE m f = m `thenE` (returnE.f)

idorapp::String -> [Mexpr] -> Mexpr
idorapp a [] = Mid a
idorapp a b = Mapp (Mfn a) b

prgtoblk::Mprog -> Mstmt
prgtoblk (Mprog a b) = Mblock a b
parseMp = mp.tokens
{-# LINE 1 "GenericTemplate.hs" #-}
-- $Id: Mp.hs,v 1.1 2002/07/26 16:59:11 gilesb Exp $

{-# LINE 15 "GenericTemplate.hs" #-}






















































infixr 9 `HappyStk`
data HappyStk a = HappyStk a (HappyStk a)

-----------------------------------------------------------------------------
-- starting the parse

happyParse start_state = happyNewToken start_state notHappyAtAll notHappyAtAll

-----------------------------------------------------------------------------
-- Accepting the parse

happyAccept j tk st sts (HappyStk ans _) = 

					   (happyReturn1 ans)

-----------------------------------------------------------------------------
-- Arrays only: do the next action

{-# LINE 138 "GenericTemplate.hs" #-}


-----------------------------------------------------------------------------
-- HappyState data type (not arrays)



newtype HappyState b c = HappyState
        (Int ->                    -- token number
         Int ->                    -- token number (yes, again)
         b ->                           -- token semantic value
         HappyState b c ->              -- current state
         [HappyState b c] ->            -- state stack
         c)



-----------------------------------------------------------------------------
-- Shifting a token

happyShift new_state (1) tk st sts stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--     trace "shifting the error token" $
     new_state i i tk (HappyState (new_state)) ((st):(sts)) (stk)

happyShift new_state i tk st sts stk =
     happyNewToken new_state ((st):(sts)) ((HappyTerminal (tk))`HappyStk`stk)

-- happyReduce is specialised for the common cases.

happySpecReduce_0 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_0 nt fn j tk st@((HappyState (action))) sts stk
     = action nt j tk st ((st):(sts)) (fn `HappyStk` stk)

happySpecReduce_1 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_1 nt fn j tk _ sts@(((st@(HappyState (action))):(_))) (v1`HappyStk`stk')
     = action nt j tk st sts (fn v1 `HappyStk` stk')

happySpecReduce_2 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_2 nt fn j tk _ ((_):(sts@(((st@(HappyState (action))):(_))))) (v1`HappyStk`v2`HappyStk`stk')
     = action nt j tk st sts (fn v1 v2 `HappyStk` stk')

happySpecReduce_3 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_3 nt fn j tk _ ((_):(((_):(sts@(((st@(HappyState (action))):(_))))))) (v1`HappyStk`v2`HappyStk`v3`HappyStk`stk')
     = action nt j tk st sts (fn v1 v2 v3 `HappyStk` stk')

happyReduce k i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyReduce k nt fn j tk st sts stk = action nt j tk st1 sts1 (fn stk)
       where sts1@(((st1@(HappyState (action))):(_))) = happyDrop k ((st):(sts))

happyMonadReduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonadReduce k nt fn j tk st sts stk =
        happyThen1 (fn stk) (\r -> action nt j tk st1 sts1 (r `HappyStk` drop_stk))
       where sts1@(((st1@(HappyState (action))):(_))) = happyDrop k ((st):(sts))
             drop_stk = happyDropStk k stk

happyDrop (0) l = l
happyDrop n ((_):(t)) = happyDrop (n - ((1) :: Int)) t

happyDropStk (0) l = l
happyDropStk n (x `HappyStk` xs) = happyDropStk (n - ((1)::Int)) xs

-----------------------------------------------------------------------------
-- Moving to a new state after a reduction









happyGoto action j tk st = action j j tk (HappyState action)


-----------------------------------------------------------------------------
-- Error recovery ((1) is the error token)

-- parse error if we are in recovery and we fail again
happyFail  (1) tk old_st _ stk =
--	trace "failing" $ 
    	happyError


{-  We don't need state discarding for our restricted implementation of
    "error".  In fact, it can cause some bogus parses, so I've disabled it
    for now --SDM

-- discard a state
happyFail  (1) tk old_st (((HappyState (action))):(sts)) 
						(saved_tok `HappyStk` _ `HappyStk` stk) =
--	trace ("discarding state, depth " ++ show (length stk))  $
	action (1) (1) tk (HappyState (action)) sts ((saved_tok`HappyStk`stk))
-}

-- Enter error recovery: generate an error token,
--                       save the old token and carry on.
happyFail  i tk (HappyState (action)) sts stk =
--      trace "entering error recovery" $
	action (1) (1) tk (HappyState (action)) sts ( (HappyErrorToken (i)) `HappyStk` stk)

-- Internal happy errors:

notHappyAtAll = error "Internal Happy error\n"

-----------------------------------------------------------------------------
-- Hack to get the typechecker to accept our action functions







-----------------------------------------------------------------------------
-- Don't inline any functions from the template.  GHC has a nasty habit
-- of deciding to inline happyGoto everywhere, which increases the size of
-- the generated parser quite a bit.









{-# NOINLINE happyShift #-}
{-# NOINLINE happySpecReduce_0 #-}
{-# NOINLINE happySpecReduce_1 #-}
{-# NOINLINE happySpecReduce_2 #-}
{-# NOINLINE happySpecReduce_3 #-}
{-# NOINLINE happyReduce #-}
{-# NOINLINE happyMonadReduce #-}
{-# NOINLINE happyGoto #-}
{-# NOINLINE happyFail #-}

-- end of Happy Template.
