> {
> module Mp where
> import Tokens
> }
> %name mp
> %tokentype { Token }
> 
> %token
> 	IF	{TkIf }
> 	THEN	{TkThen }
> 	WHILE	{TkWhile }
> 	DO	{TkDo }
> 	READ	{TkRead }
> 	ELSE	{TkElse }
> 	BEGIN	{TkBegin }
> 	END	{TkEnd }
> 	PRINT	{TkPrint }
> 	INT	{TkInt }
> 	BOOL	{TkBool }
> 	VAR	{TkVar }
> 	FUN	{TkFun }
> 	RETURN	{TkReturn }
> 	'('	{TkSym '('}
> 	')'	{TkSym ')'}
> 	'{'	{TkSym '{'}
> 	'}'	{TkSym '}'}
> 	',' 	{TkSym ','}
> 	';' 	{TkSym ';'}
> 	':'	{TkSym ':'}
> 	'+'	{TkOpn  "+"}
> 	'-' 	{TkOpn  "-"}
> 	'*' 	{TkOpn  "*"}
> 	'/' 	{TkOpn  "/"}
> 	'=' 	{TkOpn  "="}
> 	'<' 	{TkOpn  "<"}
> 	'>' 	{TkOpn  ">"}
> 	AND 	{TkOpn  "&&"}
> 	OR 	{TkOpn  "||"}
> 	NOT 	{TkOpn  "not"}
> 	LEQ 	{TkOpn  "=<"}
> 	GEQ 	{TkOpn  ">="}
> 	ASSIGN 	{TkAssign  }
> 	NUM	{TkNum $$}
> 	BOOLEAN {TkBoolean $$}
> 	ID 	{TkIdentifier $$}
> 
> %monad {E} { thenE} { returnE }
> %%
> prog : block {$1}
> 
> block : declarations program_body { Mprog ($1) ($2)}
> 
> declarations : declaration ';' declarations { $1:$3 }
>               | {- empty -}			    {[]::[Mdecl]}
> 
> declaration : var_declaration {$1}
>              | fun_declaration {$1}

> 
> var_declaration : VAR basic_var_declaration {$2}
> 
> basic_var_declaration : ID ':' type {Mvar $1 $3}
> 
> fun_declaration : FUN ID param_list ':' type '{' fun_block '}' 
> 	{Mfun $2 $3 $5 $7}
> 
> fun_block : declarations fun_body  {Mprog $1 $2}
> 
> param_list : '(' parameters ')' {$2}
> 
> parameters : parameters1  {reverse $1}
>             | {- empty -} {[]::[Mdecl]}
> 
> parameters1 : parameters1 ',' basic_var_declaration {$3:$1}
>              | basic_var_declaration	                {[$1]}
> 
> type : INT   {Mint}
>       | BOOL {Mbool}
> 
> program_body : BEGIN prog_stmts END {$2}
> 
> fun_body : BEGIN prog_stmts RETURN expr ';' END {$2 ++ [Mreturn $4]}
> 
> prog_stmts : prog_stmt ';' prog_stmts {$1:$3}
>             | {- empty -}                   {[]::[Mstmt]}      
>                                         
> prog_stmt : IF expr THEN prog_stmt ELSE prog_stmt {Mcond $2 $4 $6}
>            | WHILE expr DO prog_stmt   {Mwhile $2 $4}
>            | READ ID			 {Mread $2}
>            | ID ASSIGN expr		 {Mass $1 $3}
>            | PRINT expr		 {Mprint $2}
>            | '{' block '}'	  	 {prgtoblk ($2)}
> 
> expr :  expr OR bint_term  {Mapp Mor [$1,$3]}
>        | bint_term		{$1}
> 
> bint_term : bint_term AND bint_factor {Mapp Mand [$1,$3]}
>            | bint_factor {$1}
> 
> bint_factor : NOT bint_factor  {Mapp Mnot [$2]}
>              | int_expr compare_op int_expr  {Mapp $2 [$1,$3]}
>              | int_expr {$1}
> 
> compare_op : '=' {Meq} | '<' {Mlt} | '>' {Mgt} | LEQ {Mle}  |GEQ {Mge} 
> 
> int_expr : int_expr addop int_term {Mapp $2 [$1,$3]}
>           | int_term {$1}
> 
> addop : '+' {Madd} | '-' {Msub}
> 
> int_term : int_term mulop int_factor {Mapp $2 [$1,$3]}
>           | int_factor {$1}
> 
> mulop : '*' {Mmul} | '/' {Mdiv}
> 
> int_factor : '(' expr ')' {$2}
>             | ID argument_list {idorapp $1 $2}
>             | NUM              {Mnum $1}
>             | BOOLEAN          {Mbl $1}
>             | '-' int_factor   {Mapp Mneg [$2]}
> 
> argument_list : '(' arguments ')' {$2}
>                | {- empty -}  {[]::[Mexpr]}
> 
> arguments : arguments1 {reverse $1}
>            | {- empty -} {[]::[Mexpr]}
> 
> arguments1 : arguments1 ',' expr {$3:$1}  
>             | expr  {[$1]}
> 
> {

> happyError t = failE "Parse error"
> 
> data Mprog = Mprog  [Mdecl]  [Mstmt]
> 	
> instance Show Mprog where
>    show (Mprog a b) = "MProg("++showList a (", "++ showList b ")")
>
> data Mdecl = Mvar  String  Mtype
>               | Mfun  String  [Mdecl]  Mtype Mprog
>
> instance Show Mdecl where
>    show (Mvar a b) = "( Var , "++a++", "++show b++")"
>    show (Mfun a bs c d) = "( Fun, "++a++", "++showList bs (", "++show c++", "++show d++")")
>
> data Mstmt = Mass  String  Mexpr
>               | Mwhile  Mexpr  Mstmt
>               | Mcond  Mexpr  Mstmt  Mstmt 
>               | Mread  String
>               | Mprint  Mexpr
>               | Mreturn  Mexpr
>               | Mblock  [Mdecl]  [Mstmt]
> instance Show Mstmt where
>     show (Mass s e) = "(Assign, "++s++"=("++show e++"))"
>     show (Mwhile e st) = "(While, "++show e++", "++show st++")"
>     show (Mcond e s1 s2) = "(Cond, "++show e++", "++show s1++", "++show s2++")"
>     show (Mread s) = "(Read, "++s++")"
>     show (Mprint e) = "(Print, "++show e++")"
>     show (Mreturn e) = "(Return, "++show e++")"
>     show (Mblock ds ss) = "(Block, "++showList ds (","++showList ss ")")


> data Mtype = Mint | Mbool
> instance Show Mtype where
>    show (Mint) = "Mint"
>    show (Mbool) = "Mbool"
> data Mexpr = Mnum  Int
>               | Mbl  Bool
>               | Mid  String
>               | Mapp  Moperation  [Mexpr]
> instance  Show Mexpr where
>     show (Mnum i) = "Num="++show i
>     show (Mbl b) = "Bool="++show b
>     show (Mid s) = "ID="++s
>     show (Mapp op es) = "(Apply ,"++show op++", "++showList es ")"

> data Moperation 
>               = Mfn  String
>               | Madd | Mmul | Msub | Mdiv | Mneg
>               | Mlt | Mle | Mgt | Mge | Meq | Mneq | Mnot | Mand | Mor
> instance Show Moperation where
>     show (Mfn s) = s
>     show (Madd) = "Madd"
>     show  (Mmul)= "Mmul"
>     show  (Msub)= "Msub"
>     show  (Mdiv)= "Mdiv"
>     show  (Mneg)= "Mneg"
>     show  (Mlt)= "Mlt"
>     show  (Mle)= "Mle"
>     show  (Mgt)= "Mgt"
>     show  (Mge)= "Mge"
>     show  (Meq)= "Meq"
>     show  (Mneq)= "Mneq"
>     show  (Mnot)= "Mnot"
>     show  (Mand)= "Mand"
>     show  (Mor)= "Mor"

> data E a = Ok a | Failed String
> instance (Show a)=> Show (E a) where
>    show (Ok a) = show a
>    show (Failed s) = "Error:"++s

> thenE:: E a -> ( a -> E b) -> E b
> m `thenE` k =
>     case m of
>          Ok a -> k a
>          Failed e -> Failed e

> returnE:: a -> E a
> returnE a = Ok a

> failE::String -> E a
> failE err = Failed err

> applyE::E a -> (a -> b) ->  E b
> applyE m f = m `thenE` (returnE.f)

> idorapp::String -> [Mexpr] -> Mexpr
> idorapp a [] = Mid a
> idorapp a b = Mapp (Mfn a) b

> prgtoblk::Mprog -> Mstmt
> prgtoblk (Mprog a b) = Mblock a b
>
> parseMp = mp.tokens

> }
