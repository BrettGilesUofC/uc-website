%{
#include <ctype.h>
#include <stdio.h>
#include <string.h>
  /*#define YYDEBUG 1*/
#include "datatype.h"
%}

/* The types of tokens and nonterminals */

%union
{
  char	* idname;
  char	* operation;
  char  * mtype;
  int    numval;
  stmt * mstmt;
  prog * mprog;
  decllist * mdecls ;
  decl * mdecl;
  expression * mexpression ;
  stmtlist * mstmtlist;
  expressionlist * mexpressionlist;
}

%nonassoc UMINUS  	

/* The terminal definitions */

%token 	<idname>	ID
%token 	<numval> 	NUM BVAL

%token 			ADD SUB MUL DIV     
%token 			AND OR NOT     
%token 			EQUAL LT GT LE GE      
%token 			ASSIGN  
%token 			LPAR RPAR CLPAR CRPAR   
%token 			COLON SEMICOLON COMMA RETURN  
%token 			IF THEN WHILE DO READ ELSE 
%token 			BEG END PRINT INT BOOL VAR FUN     

/* The nonterminal definitions */

%type	<idname>	identifier


%type 	<mprog>		prog   block fun_block
%type   <mdecls>	declarations parameters parameters1 param_list 
%type   <mdecl>         declaration var_declaration fun_declaration basic_var_declaration 
%type 	<mstmt>		prog_stmt 
%type   <mstmtlist>	prog_stmts fun_body program_body
%type 	<mtype>		type
%type	<mexpression>	expr bint_term bint_factor int_expr int_term int_factor 
%type 	<mexpressionlist>     arguments argument_list arguments1

%type	<operation>		addop mulop compare_op

%%

prog
	:  block				
		{$$=$1;	
		printprog($$);}
	;
block 
	:  declarations program_body		
		{
	$$ = ( prog *) malloc (sizeof( prog));
	if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		
   	$$->decls = $1;
	$$->stmts = $2;
		}
        ;

declarations
	:  declaration SEMICOLON declarations	
		{
		  $$=(decllist *) malloc (sizeof (decllist));
		  $$->d = $1;
		  $$->decls = $3;
		}	
        |					
		{
		$$=NULL;                /* end of declaration list */	
		} 
        ;

declaration
	:  var_declaration			
		{
		$$=$1;
		}
        |  fun_declaration			
		{
		$$=$1;
		}
        ;

var_declaration
        :  VAR basic_var_declaration		
		{
		$$=$2;
		}
  	;


basic_var_declaration
	:  identifier COLON type		
		{
		$$ = ( decl *) malloc (sizeof( decl));
		if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		
		strcpy($$->kind,"VAR");
		$$->id = $1  ;
		$$->type = $3;
		}
        ;


fun_declaration
	:  FUN identifier param_list COLON type CLPAR fun_block CRPAR
		{
		$$ = ( decl *) malloc (sizeof( decl));
		if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		
		strcpy($$->kind,"FUN");
		$$->id = $2 ; 
		$$->type = $5;
		$$->funargs=$3;
		$$->funbody=$7;
		}
        ;

fun_block
        :  declarations fun_body		
		{
		
		$$ = (prog *) malloc (sizeof( prog));
		if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		
		$$->decls = $1;
		$$->stmts = $2;

		}
        ;

param_list
        :  LPAR parameters RPAR			
		{
		$$=$2;	
		}
        ;

parameters
        :  parameters1				
		{
		$$=$1;
		}
        | 					
		{
		$$=NULL;
		}
        ;

parameters1
        :  parameters1 COMMA basic_var_declaration	
		{
		  decllist * traverse;
		  traverse = $1;
		  if (traverse == NULL)
		    {
		      $$=(decllist *) malloc (sizeof (decllist));
		      $$->d = $3;
		      $$->decls = NULL;
		    }
		  else
		    {
		      while (traverse->decls != NULL)
			{
			  traverse=traverse->decls;
			}
		      traverse->decls=(decllist *) malloc (sizeof (decllist));
		      traverse->decls->d = $3;
		      traverse->decls->decls = NULL;
		      $$=$1;
		    }
		}
        |  basic_var_declaration			
		{
		  $$=(decllist *) malloc (sizeof (decllist));
		  $$->d = $1;	
		  $$->decls = NULL;
		}
        ;

identifier		
        :  ID					
		{
		$$=yylval.idname;            // value returned from lexer 
		}
        ;

type 
	:  INT					
		{
		  $$=strdup("INT");
		  if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}

		}
        |  BOOL					
		{
		  $$=strdup("BOOL");
		  if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}

		}
        ;

program_body
	:  BEG prog_stmts END		
		{
		  $$=$2;	
		}
	;
fun_body
	:  BEG prog_stmts RETURN expr SEMICOLON END	
               { 

		  stmtlist * traverse;
		  stmt * ret;
		  ret  = ( stmt *) malloc (sizeof( stmt));
		  
		  ret->exp = $4;
		  ret->kind = RETURNstatement;
		  traverse = $2;
		  if (traverse == NULL)
		    {
		      $$=(stmtlist *) malloc (sizeof (stmtlist));
		      if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;};
		      $$->astmt = ret;
		      $$->stmts = NULL;
		    }
		  else
		    {
		      while (traverse->stmts != NULL)
			{
			  traverse=traverse->stmts;
			}
		      traverse->stmts=(stmtlist *) malloc (sizeof (stmtlist));
		      traverse->stmts->astmt = ret;
		      traverse->stmts->stmts = NULL;
		      $$=$2;
		    }
	       }

        ;


prog_stmts
        :  prog_stmt SEMICOLON prog_stmts
		{
		  
		  $$  = ( stmtlist *) malloc (sizeof( stmtlist));
		  if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		  
		  $$->astmt = $1;
		  $$->stmts = $3;
		}
        | 					
		{
		  $$=NULL;                /* end of statement list */
		}
        ;


prog_stmt 
	:  IF expr THEN prog_stmt ELSE prog_stmt	
		{

		$$ = ( stmt *) malloc (sizeof( stmt));
		if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		
		$$->kind = CONDstatement;
		$$->exp = $2;
		$$->stmt1=$4;
		$$->stmt2=$6;
		}

        |  WHILE expr DO prog_stmt			
		{
		$$ = ( stmt *) malloc (sizeof( stmt));
		if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		
		$$->kind = WHILEstatement;
		$$->exp = $2;
		$$->stmt1=$4;
		}
        |  READ ID				
		{
		$$ = ( stmt *) malloc (sizeof( stmt));
		if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		
		$$->kind = READstatement;
		$$->id = $2;
		}


        |  ID ASSIGN expr				
		{
		$$ = ( stmt *) malloc (sizeof( stmt));
		if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		
		$$->kind = ASSIGNstatement;
		$$->id = $1;
		$$->exp = $3;
		}


        |  PRINT expr				
		{
		  $$ = ( stmt *) malloc (sizeof( stmt));
		if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		
		$$->kind = PRINTstatement;
		$$->exp = $2;
		}
        |  CLPAR block CRPAR				
		{
		   $$ = ( stmt *) malloc (sizeof( stmt));
		if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		
		$$->kind = READstatement;
		$$->block = $2;
		}
        ;


expr 
	:  expr OR bint_term				
		{
		  
		  $$ = ( expression *) malloc (sizeof( expression));
		  if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		  
		  $$->kind = APPLYexp;
		  $$->applyop= strdup("OR");
		  $$->exps=newexprlist($1,$3);

		    }

        |  bint_term					

		{
		$$=$1;
		}
        ;

bint_term 
        :  bint_term AND bint_factor			
		{
		   
		  $$ = ( expression *) malloc (sizeof( expression));
		  if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		  
		  $$->kind = APPLYexp;
		  $$->applyop= strdup("AND");
		  $$->exps=newexprlist($1,$3);
		}
	
        |  bint_factor					
		{
		
		$$=$1;
		}
	;

bint_factor 
 	:  NOT bint_factor				
		{
		  $$ = ( expression *) malloc (sizeof( expression));
		  if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		  
		  $$->kind = APPLYexp;
		  $$->applyop= strdup("NOT");
		  $$->exps=newexprlist($2,NULL);
		}


        |  int_expr compare_op int_expr			
		{
		  $$ = ( expression *) malloc (sizeof( expression));
		  if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		  
		  $$->kind = APPLYexp;
		  $$->applyop= strdup($2);
		  $$->exps=newexprlist($1,$3);


		}
        |  int_expr					
		{
	
		$$=$1;
		}
 	;

compare_op
	:  EQUAL 				
		{
		  $$=strdup("=");			
		
		}	
        |  LT 					
		{
		  $$=strdup("<");			
		
		}
        |  GT 					
		{
		  $$=strdup(">");			
		}
        |  LE 					
		{
		  $$=strdup("<=");			
		}
        |  GE					
		{
		  $$=strdup(">=");			
		}
        ;


int_expr 
	:  int_expr addop int_term		
		{
		  $$ = ( expression *) malloc (sizeof( expression));
		  if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		  
		  $$->kind = APPLYexp;
		  $$->applyop= strdup($2);
		  $$->exps=newexprlist($1,$3);
		}

        |  int_term				
		{
		$$=$1;
		}
 	;

addop
	:  ADD 					
		{
		  $$=strdup("+");
		
		}
	|  SUB					
		{
		  $$=strdup("-");
		}
	;



int_term 
	:  int_term mulop int_factor		
		{
		  $$ = ( expression *) malloc (sizeof( expression));
		  if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		  
		  $$->kind = APPLYexp;
		  $$->applyop= strdup($2);
		  $$->exps=newexprlist($1,$3);
		
		}
        |  int_factor				
		{
		//printf(" 46");

		$$ = $1;
		}
	;

mulop   
	:  MUL 					
		{
		   $$=strdup("*");
		}
        |  DIV					
		{
		   $$=strdup("*");
		}
	;

int_factor 
	:  LPAR expr RPAR			
		{
		$$=$2;
		}

        |  identifier argument_list			
		{
		  $$ = ( expression *) malloc (sizeof( expression));
		  if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		  if ($2 == NULL)  /* identifier */
		    {
		    
		    $$->kind = IDexp;
		    $$->id = $1;
		  }
		else            /* function call */
	          {
		    $$->kind = APPLYexp;
		    $$->applyop = $1;
		    $$->exps=$2;
		  }
		}

        |  NUM					
               {
		 $$ = ( expression *) malloc (sizeof( expression));
		 if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		 
		 $$->kind=NUMexp;
		 $$->numval = yylval.numval;
		}

        |  BVAL					
		{
		 $$ = ( expression *) malloc (sizeof( expression));
		 if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		 
		 $$->kind=NUMexp;
		 $$->boolval = yylval.numval;
		}

        |  SUB int_factor %prec UMINUS 
		{
		  $$ = ( expression *) malloc (sizeof( expression));
		  if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		  
		  $$->kind = APPLYexp;
		  $$->applyop= strdup("NEG");
		  $$->exps=newexprlist($2,NULL);

		}
	;


argument_list 
	:  LPAR arguments RPAR			
		{
		  $$=$2;
		}
        |  					
		{
		  $$=NULL;
		}
	;

arguments 
	:  arguments1				
		{

		  $$=$1;
		}
| 				/* functions with no parameters */	
		{
		  $$=( expressionlist *) malloc (sizeof( expressionlist));
		  if ($$ == NULL) {fprintf (stderr, "Out of Memory\n");YYABORT;}
		  $$->exp = NULL;
		  $$->exps = NULL;  
		}
	;

arguments1 
	:  arguments1 COMMA expr		
		{
		  expressionlist * traverse;
		  
		  traverse = $1;
		  if (traverse == NULL)
		    {
		      $$ = newexprlist($3,NULL);
		    }
		  else
		    {
		      while (traverse->exps != NULL)
			{
			  traverse=traverse->exps;
			}
		      traverse->exps=(expressionlist *) malloc (sizeof (expressionlist));
		      traverse->exps->exp = $3;
		      traverse->exps->exps = NULL;
		      $$=$1;
		    }

		}
        |  expr					
		{
		$$=newexprlist($1,NULL);	
		}
	;

%%

/* start of programs */

/*#include "lex.yy.c"*/

main()
{
 
  /*  yydebug=1;*/
 yyparse();
  printf("Did the parse");

}
int
yyerror(char *s)
{
 fprintf(stderr,"%s\n",s);
 return 0;
}

yywrap(void)
{
  return 1;
}




