typedef enum {IDexp, NUMexp, BOOLexp, APPLYexp} expressionkind;
typedef enum {CONDstatement, WHILEstatement, BLOCKstatement, ASSIGNstatement,
	      READstatement, PRINTstatement, RETURNstatement} statementkind;
 struct prog ;
 struct  decllist;
 struct  decl;
 struct  expression;
 struct  stmt;

 struct  stmtlist;
 struct  expressionlist;

typedef struct stmtlist  /*Keeps in reverse order */
{
  struct stmt * astmt;
  struct stmtlist * stmts;
} stmtlist;


typedef struct expressionlist /*Keeps in reverse order */
{
   struct expression * exp;
   struct expressionlist * exps;
} expressionlist;

typedef    struct prog
{
   struct decllist * decls;
   struct stmtlist * stmts;
}prog;

typedef struct decllist
{
   struct decl * d;
  struct  decllist * decls;
}decllist;

typedef struct decl
{
      char kind [4];
      char    * id;
      char    * type;
       struct decllist * funargs;
       struct prog * funbody;

}decl;


typedef struct stmt
{
  statementkind kind;
  char * id;            /* Used for assign and read */
   struct expression * exp;     /* Used for return, assign, print, cond, while */
 struct   stmt * stmt1;         /* Used for while, cond */
 struct   stmt * stmt2;         /* Used for cond */
 struct   prog * block;         /* Used only for a block statment */

}stmt;
 
typedef struct expression
{
  expressionkind kind;
  char     * id;
  int      numval;
  int      boolval;
  char * applyop;
 struct   expressionlist * exps;
}expression;


expressionlist* newexprlist(expression * first, expression* second);
		  
void printprog(prog * aprog);

  
void printdecls(decllist * dlist);


void printstmts(stmtlist * slist);


void printexps(expressionlist * elist);

void printdecl(decl * d);

void printstmt(stmt * s);

void printexp(expression * e);
