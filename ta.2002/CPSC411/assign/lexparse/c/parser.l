%{
#include <stdlib.h>
#include <string.h>
#include <stdio.h>                                    
#include "datatype.h"
#include "y.tab.h"
int incomment;
%}

  
ws    [ \t\n]+
id    [a-z][a-z0-9]* 
num   [0-9][0-9]*
%s COMMENT
%%

<INITIAL>"+"       { return(ADD);        }
<INITIAL>"-"       { return(SUB);        }
<INITIAL>"*"       { return(MUL);        }
<INITIAL>"/"       { return(DIV);        }

<INITIAL>"&&"      { return(AND);        }
<INITIAL>"||"      { return(OR);         }
<INITIAL>"not"     { return(NOT);        }

<INITIAL>"="       { return(EQUAL);      }
<INITIAL>"<"       { return(LT);         }
<INITIAL>">"       { return(GT);         }
<INITIAL>"=<"      { return(LE);         }
<INITIAL>">="      { return(GE);         }

<INITIAL>":="      { return(ASSIGN);     }

<INITIAL>"("       { return(LPAR);       }
<INITIAL>")"       { return(RPAR);       }
<INITIAL>"{"       { return(CLPAR);      }
<INITIAL>"}"       { return(CRPAR);      }

<INITIAL>":"       { return(COLON);      }
<INITIAL>";"       { return(SEMICOLON);  }
<INITIAL>","       { return(COMMA);      }

<INITIAL>"if"      { return(IF);         }
<INITIAL>"then"    { return(THEN);       }
<INITIAL>"while"   { return(WHILE);      }
<INITIAL>"do"      { return(DO);         }
<INITIAL>"read"    { return(READ);       }
<INITIAL>"else"    { return(ELSE);       }
<INITIAL>"begin"   { return(BEG);        }
<INITIAL>"end"     { return(END);        }
<INITIAL>"print"   { return(PRINT);      }
<INITIAL>"int"     { return(INT);        }
<INITIAL>"bool"    { return(BOOL);       }
<INITIAL>"var"     { return(VAR);        }
<INITIAL>"fun"     { return(FUN);        }
<INITIAL>"return"  { return(RETURN);     }


<INITIAL>"/*"      { incomment ++; BEGIN COMMENT;}
 

<INITIAL>"%".*       { }

<INITIAL>{ws}      { }

<INITIAL>{id}      { yylval.idname = strdup(yytext); return(ID); }

<INITIAL>{num}     { yylval.numval =   atoi(yytext); return(NUM); }

<INITIAL>.         { printf("ERROR: unmatched %s",yytext); }

<COMMENT>"/*"  { incomment ++; }
<COMMENT>.  { }
<COMMENT>"\n"  { }
<COMMENT>"*/"  { incomment --; if (incomment == 0) {BEGIN INITIAL;}; }


%%

















