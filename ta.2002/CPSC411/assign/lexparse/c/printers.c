#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "datatype.h"

void printprog(prog * aprog)
{
  printf("prog ([");
  printdecls(aprog->decls);
  printf("],[");
  printstmts(aprog->stmts);
  printf("])\n");
}
  
void printdecls(decllist * dlist)
{
  if (dlist==NULL) return;
  printdecl(dlist->d);
  if (dlist->decls != NULL)
    {
      printf (",");
      printdecls(dlist->decls);
    }
}

void printstmts(stmtlist * slist)
{
  if (slist==NULL) return;
  printstmt(slist->astmt);
    if (slist->stmts != NULL)
    {
      printf (",");
      printstmts(slist->stmts);
    }
}

void printexps(expressionlist * elist)
{
  if (elist==NULL) return;
  printexp(elist->exp);
    if (elist->exps != NULL)
    {
      printf (",");
      printexps(elist->exps);
    }
}

void printdecl(decl * d)
{
  if (strcmp(d->kind,"VAR") == 0)
    {
      printf ("(Var,%s,%s)",d->id, d->type);
    }
  else
    {
      printf("(Fun,%s,[",d->id,d->type);
      printdecls(d->funargs);
      printf("],Funprog=(");
      printprog(d->funbody);
      printf("))\n");
    }
}

void printstmt(stmt * s)
{
  switch (s->kind)
    {
    case CONDstatement:
      printf("Cond(");
      printexp(s->exp);
      printf(",");
      printstmt(s->stmt1);
      printf(",");
      printstmt(s->stmt2);
      printf(")\n");
      break;
      
    case WHILEstatement: 
      printf("While(");
      printexp(s->exp);
      printf(",");
      printstmt(s->stmt1);
      printf(")\n");
      break;
    case  BLOCKstatement: 
      printf("Block{");
      printprog(s->block);
      printf("}\n");
      break;
    case  ASSIGNstatement: 
      printf("Assign(%s=(",s->id);
      printexp(s->exp);
      printf("))\n");
      break;
    case  READstatement: 
      printf("Read(%s)\n",s->id);
      break;
    case  PRINTstatement: 
      printf("Print(");
      printexp(s->exp);
      printf(")\n");
      break;
    case  RETURNstatement:
      printf("Return(");
      printexp(s->exp);
      printf(")\n");
      break;
    default:
      printf("Unknown");
      break;
    }
}

void printexp(expression * e)
{
 switch (e->kind)
    {
    case IDexp:
      printf("ID=%s",e->id);
      break;
    case  NUMexp:
      printf("NUM=%d",e->numval);
      break;

    case  BOOLexp: 
      printf("BOOL=%s",e->boolval == 0 ?"false":"true");
      break;

    case  APPLYexp:
      printf("Apply(%s to [",e->applyop);
      printexps(e->exps);
      printf("])\n");
      break;
    default:
      printf("Bad exp");
      break;
    }
}
