#!/usr/bin/python
# bring in our objects and the lexer.
#
from calcObjects import *
from calcLexer import *
# get sys so we can access stdin
import sys

# alias the stdin file desciptor
program=sys.stdin

# create a scanner object
scanner = calcScanner()

# read the file in, tokenize it and create the parse tree.
#
parsedprogram = prog(scanner.tokenize(program.read()))
#
# generate the code
parsedprogram.gencode()
