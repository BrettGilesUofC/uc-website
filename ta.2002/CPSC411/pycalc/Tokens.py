#!/usr/bin/python
#
#  This class is used to define the general token used in lexing the input.
#  Typically, the type is a single character for things such as operations (+,- etc)
#  and is a string for other tokens ("num", "p" )
#
#  We use the optional value attr in the case of a number type and assign the
#  value of the number to it. It is returned by the method intval. If we had other
#  value based tokens, they could all be stored in attr or in other variables if desired.



class Token:
	def __init__(self, type, attr=None):
		self.type = type
		self.attr = attr

	#
	# 
	#  __cmp__	required for GenericParser, required for
	#			GenericASTMatcher only if your ASTs are
	#			heterogeneous (i.e., AST nodes and tokens)
	#  __repr__	recommended for nice error messages in GenericParser
	# 
	#  
	def __cmp__(self, o):
		return cmp(self.type, o)

        def intval(self):
              return self.attr
	def __repr__(self):
		return  self.type

