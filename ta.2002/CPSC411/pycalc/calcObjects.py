#!/usr/bin/python
#
# See the doc string for each class. This series of classes reflect the
# production rules for the calculator grammer. Each class has two methods.
# The __init__ method which is called upon object creation, parse the token list to
# create the item to be returned. The gencode methods recursivly write the code for
# each item.
#
# Note this data structure is more like the Haskell one rather than the general tree
# as presented in calculator.c and in class. Each class contains whichever elements it may
# be and optionaly some element to describe which production rule was used. For example,
# consider term. The production rule is:
#
#     term -> SUB term
#          | factor morefactors
#
# In the __init__ method for term, we check the first token. If it is "-", we know we are
# using the first rule. We set the flag value (negterm) to 1 (TRUE) and create a term with
# the remaining tokens. If it is not, we set negterm to 0 (FALSE) and create a factor and
# morefactors.
# The gencode method for term checks the flag value first and then generates code based upon
# whether we have stored a term or a factor with morefactors.  Pictorially, the statment
# print (3+4)*5 would parse out as:
#
# stmt->ex+->te+->fac->ex+->te+->fac->num(3)
#         |    |         |    |
#         |    |         |    +->mf(iam==0)
#         |    |         +->mt(op+)+->te+->fac->num(4)
#         |    |                   |    |
#         |    |                   |    +->mf(iam==0)
#         |    |                   +->mt(iam==0)
#         |    +->mf(op*)+->fac->num(6)
#         |              |
#         |              +->mf(iam==0)
#         |
#         +->mt.iam==0 
def mtoken(tkn,tlist,opt='no'):
      """tkn is the one you are looking for. tlist is the
      global list. opt is a parameter that states whether we
      should fail out if we don't get the token"""
      if tkn == tlist[0]: tlist.pop(0)
      else:
            if opt == 'no':
                  print "Unexpected Token"
                  raise SyntaxError    # A built in python exception
      

class prog:
      """A class that holds a parsed program. Its only data is a list of
      statments."
      def __init__(self,tlist):
            """Initialization of this class takes a list of tokens and
            parses out the statments."""
            self.stmtlist=[]      
            while len(tlist) > 0 :
                  s1 = stmt(tlist)
                  self.stmtlist.append(s1)
                  if len(tlist) > 0:
                        mtoken('semi',tlist,opt='yes')

      def gencode(self):
            """Just go through the list, generating code for each one."""
            for s in self.stmtlist:
                  s.gencode()


class stmt:
      """Statement contains one expression"""
      def __init__(self,tlist):
            mtoken('p',tlist)
            self.ex = expr(tlist)

      def gencode(self):
            self.ex.gencode();
            print "PRINT"
            

class expr:
      """An expression is a term and moreterms. moreterms may be null, see the
      moreterms class."""
      def __init__(self,tlist):
            self.te = term(tlist)
            self.mt = moreterms(tlist)


      def gencode(self):
            self.te.gencode();
            self.mt.gencode();

class term:
      """We handle the two cases, the negation of a term or a factor followed
      by more factors."""
      def __init__(self,tlist):
            if tlist[0] == '-' :
                  self.negterm=1
                  mtoken('-',tlist)
                  self.te = term(tlist)
            else:
                  self.negterm=0
                  self.fac = factor(tlist)
                  self.mf = morefacs(tlist)


      def gencode(self):
            if self.negterm == 1 :
                  print "cPUSH 0"
                  self.te.gencode();
                  print "OP2 -"
            else:
                  self.fac.gencode();
                  self.mf.gencode();
            
class factor:
      """A factor is either an expression in parenthesis or it is a number. We
      determine that by checking against the possible first set. '(' is the only
      legal start to a parenthesized expression. If it is not that, we must be a
      number."""
      def __init__(self,tlist):
            if tlist[0] == '(' :
                  self.parterm=1
                  mtoken('(',tlist)
                  self.ex = expr(tlist)
                  mtoken(')',tlist)
                  
            else:
                  self.parterm=0
                  if tlist[0] == 'num':
                        self.val = tlist[0].intval()
                  mtoken('num',tlist)    # even if we didn't find one.


      def gencode(self):
            if self.parterm == 1 :
                  self.ex.gencode();
            else:
                  print "cPUSH %d"%self.val
                  
            

            
class morefacs:
      """This is a nullable production, so we must first check if we are out
      of tokens. If so, we note that this is null. Otherwise, this must start
      with either a * or /. Again, if those are not found, we signal this to be
      null. When they are found, we process the next item, which is a factor and
      look for more factors."""
      def __init__(self,tlist):
            if len(tlist) > 0 :
                  if tlist[0] == '*' or tlist[0] == '/' :
                        self.iam = 1
                        self.op  = tlist[0]
                        mtoken(tlist[0],tlist)   # Guaranteed to work on non empty list :)
                        self.fac = factor(tlist)
                        self.mf = morefacs(tlist)
                  
                  else:
                        self.iam =0  # No more factors :(
            else:
                  self.iam =0  # No more factors :(


      def gencode(self):
            if self.iam == 1 :
                  self.fac.gencode();
                  print "OP2 %s"%self.op
                  self.mf.gencode();



            
class moreterms:
      """This is a nullable production, so we must first check if we are out
      of tokens. If so, we note that this is null. Otherwise, this must start
      with either a + or -. Again, if those are not found, we signal this to be
      null. When they are found, we process the next item, which is a term and
      look for more terms."""
      def __init__(self,tlist):
            if len(tlist) > 0 :
                  if tlist[0] == '+' or tlist[0] == '-' :
                        self.iam = 1
                        self.op  = tlist[0]
                        mtoken(tlist[0],tlist)   # Guaranteed to work on non empty list :)
                        self.te = term(tlist)
                        self.mt = moreterms(tlist)
                        
                  else:
                        self.iam =0  # No more terms :(
            else:
                  self.iam =0  # No more anything :(



      def gencode(self):
            if self.iam == 1 :
                  self.te.gencode();
                  print "OP2 %s"%self.op
                  self.mt.gencode();
