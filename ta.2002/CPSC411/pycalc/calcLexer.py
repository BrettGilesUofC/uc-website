#!/usr/bin/python
#
#  Import the module needed for generic scanning and lexing. 
from spark import *

#  Bring in our token class.

from Tokens import Token

# As per the documentation, we derive a class from the GenericScanner. This has a tokenize
# method that returns a list of tokens. patterns for tokens are defined by creating a method
# called "t_<something>" with a raw pattern string as the doc string for the method. Order
# matters.  See SPARK.ps for further details.

class calcScanner(GenericScanner):
      def __init__(self):
            GenericScanner.__init__(self)

      def tokenize(self,input):
            # set up a class variable that holds a list of the tokens.
            self.rv=[]
            GenericScanner.tokenize(self,input)
            return self.rv

      # Ignore whitespace.
      def t_whitespace(self,s):
            r' \s+ '
            pass

      def t_op(self, s):
            r' \+|\-|\*|\/ '
            self.rv.append(Token(type=s))

      def t_print(self,s):
            r' print '
            self.rv.append(Token(type='p'))

      def t_pars(self, s):
            r' \)|\( '
            self.rv.append(Token(type=s))
            
      def t_semi(self,s):
            r' \; '
            self.rv.append(Token(type='semi'))

      def t_num(self,s):
            r' \d+ '
            self.rv.append(Token(type='num',attr=int(s)))
            
