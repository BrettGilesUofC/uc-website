
  module CalcTokens where
  import Alex

  tkprint  p s = TkPrint		

  sym p s = TkSym  (head s) 		

  numb  p s = TkNum  ((read s)::Int)	

  opn  p s = TkOpn  (head s) 		

  data Token = TkPrint
 	| TkSym  Char
 	| TkNum  Int
 	| TkOpn  Char
 	| TkErr Posn
 	deriving(Eq,Show)


  tokens:: String -> [Token]
  tokens inp = scan tokens_scan inp
  
  tokens_scan:: Scan Token
  tokens_scan = load_scan(tokens_acts,stop_act) tokens_lx
 	where
 	stop_act p "" = []
 	stop_act p inp = [TkErr p]
 

  tokens_acts = [("numb",numb),("opn",opn),("sym",sym),("tkprint",tkprint)]

  tokens_lx :: [(Bool, [(Int,String,[Int],Maybe((Char,Char),[(Char,Bool)]),Maybe Int)], Int, ((Char,Char),[(Char,Int)]))]
  tokens_lx = [lx__0_0,lx__1_0,lx__2_0,lx__3_0,lx__4_0,lx__5_0,lx__6_0,lx__7_0,lx__8_0,lx__9_0]
  lx__0_0 :: (Bool, [(Int,String,[Int],Maybe((Char,Char),[(Char,Bool)]),Maybe Int)], Int, ((Char,Char),[(Char,Int)]))
  lx__0_0 = (False,[],-1,(('\t','p'),[('\t',1),('\n',1),('\v',1),('\f',1),('\r',1),(' ',1),('(',7),(')',7),('*',9),('+',9),('-',9),('/',9),('0',8),('1',8),('2',8),('3',8),('4',8),('5',8),('6',8),('7',8),('8',8),('9',8),(';',7),('p',2)]))
  lx__1_0 :: (Bool, [(Int,String,[Int],Maybe((Char,Char),[(Char,Bool)]),Maybe Int)], Int, ((Char,Char),[(Char,Int)]))
  lx__1_0 = (True,[(0,"",[],Nothing,Nothing)],-1,(('\t',' '),[('\t',1),('\n',1),('\v',1),('\f',1),('\r',1),(' ',1)]))
  lx__2_0 :: (Bool, [(Int,String,[Int],Maybe((Char,Char),[(Char,Bool)]),Maybe Int)], Int, ((Char,Char),[(Char,Int)]))
  lx__2_0 = (False,[],-1,(('r','r'),[('r',3)]))
  lx__3_0 :: (Bool, [(Int,String,[Int],Maybe((Char,Char),[(Char,Bool)]),Maybe Int)], Int, ((Char,Char),[(Char,Int)]))
  lx__3_0 = (False,[],-1,(('i','i'),[('i',4)]))
  lx__4_0 :: (Bool, [(Int,String,[Int],Maybe((Char,Char),[(Char,Bool)]),Maybe Int)], Int, ((Char,Char),[(Char,Int)]))
  lx__4_0 = (False,[],-1,(('n','n'),[('n',5)]))
  lx__5_0 :: (Bool, [(Int,String,[Int],Maybe((Char,Char),[(Char,Bool)]),Maybe Int)], Int, ((Char,Char),[(Char,Int)]))
  lx__5_0 = (False,[],-1,(('t','t'),[('t',6)]))
  lx__6_0 :: (Bool, [(Int,String,[Int],Maybe((Char,Char),[(Char,Bool)]),Maybe Int)], Int, ((Char,Char),[(Char,Int)]))
  lx__6_0 = (True,[(1,"tkprint",[],Nothing,Nothing)],-1,(('0','0'),[]))
  lx__7_0 :: (Bool, [(Int,String,[Int],Maybe((Char,Char),[(Char,Bool)]),Maybe Int)], Int, ((Char,Char),[(Char,Int)]))
  lx__7_0 = (True,[(2,"sym",[],Nothing,Nothing)],-1,(('0','0'),[]))
  lx__8_0 :: (Bool, [(Int,String,[Int],Maybe((Char,Char),[(Char,Bool)]),Maybe Int)], Int, ((Char,Char),[(Char,Int)]))
  lx__8_0 = (True,[(3,"numb",[],Nothing,Nothing)],-1,(('0','9'),[('0',8),('1',8),('2',8),('3',8),('4',8),('5',8),('6',8),('7',8),('8',8),('9',8)]))
  lx__9_0 :: (Bool, [(Int,String,[Int],Maybe((Char,Char),[(Char,Bool)]),Maybe Int)], Int, ((Char,Char),[(Char,Int)]))
  lx__9_0 = (True,[(4,"opn",[],Nothing,Nothing)],-1,(('0','0'),[]))

