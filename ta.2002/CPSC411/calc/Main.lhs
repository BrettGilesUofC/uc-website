\documentclass{article}

\usepackage{verbatim}

\title{Demo Calculator}

\author{Brett G. Giles}
\date{\today}

\newcommand{\smalltt}[1]{\texttt{\small#1}}
\newenvironment{code}{\small\verbatim}{\normalsize\endverbatim}

\begin{document}
\maketitle

\section{Tokenizer}
\input{./CalcTokens.lx}
\section{Program for code generation}
\subsection{Data Types}
Our code begins with importing the tokenizer and declaring the datatypes for
the language. The datatypes are built directly from the grammers production rules.

\begin{verbatim}

> 
> module Main where
> import CalcTokens
> type Prog =  [Statement]
> data Statement = Print Expression
>    deriving(Show,Eq)
> data Expression = Exp Term Moreterms
>    deriving(Show,Eq)
> data Term = Trm Factor Morefactors
>    | Sub Term
>    deriving(Show,Eq)
> data Factor = Pars Expression
>    | Fnum Int
>    deriving(Show,Eq)

> data Moreterms = Addop Term Moreterms
>    | Subop Term Moreterms
>    | Nomoreterms
>    deriving(Show,Eq)

> data Morefactors = Mulop Factor Morefactors
>    | Divop Factor Morefactors
>    | Nomorefacs
>    deriving(Show,Eq)

\end{verbatim} 
\subsection{Recursive Descent Parsing}
In this section we define the parsing functions. The top level function, parse, returns
a Prog, which is a list of statements. Each of the other functions, one for each of 
the production rules left hand sides, returns a tuple of its corresponding
data type and a list of the rest of the tokens. 
\begin{verbatim}

>
> parse::[Token] -> Prog
>
> stmt::[Token] -> (Statement,[Token])
> expr::[Token]-> (Expression,[Token])
> term::[Token]-> (Term,[Token])
> fact::[Token]-> (Factor,[Token])
> moreterms::[Token]-> (Moreterms,[Token])
> morefactors::[Token]-> (Morefactors,[Token])
>

\end{verbatim} 

The actual definitions of the functions is reflective of the production rules. We
typically either examine the first token to determine which production rule to use, or
in the case of higher level rules such as expression, just call the sub rules.

\begin{verbatim}

> parse [] =  []
> parse ts = s1:(parse ts') where
>	(s1,ts')=stmt ts

> stmt [] = error "Expecting Statement - not <empty>"
> stmt ((TkPrint):ts) = (Print ex, remsemi ts') where
>	 (ex,ts') = expr ts
>        remsemi ((TkSym ';'):tks) = tks
>        remsemi [] = []
>        remsemi _ = error "Missing Semicolon after statement"
> stmt _ = error "Invalid token found"

> expr [] = error "Expecting Expression - not <empty>"
> expr ts = (Exp term1 mterms , ts'') where
>	  (term1, ts') = term ts
>         (mterms, ts'') = moreterms ts'
>
> term []  = error "Expecting term - not <empty>"
> term ((TkOpn '-'):ts) = (Sub term1 , ts') where
>	  (term1, ts') = term ts
> term ts = (Trm factor1 morefacts,ts'') where
>	  (factor1, ts') = fact ts
>         (morefacts, ts'') = morefactors ts'

> fact []  = error "Expecting Factor - not <empty>"
> fact ((TkSym '('):ts) = (Pars exp1 , rempar ts') where
>	  (exp1, ts') = expr ts
>         rempar ((TkSym ')'):ts) = ts
>         rempar _ = error "Missing Parenthesis"
>
> fact ((TkNum i):ts) = (Fnum i, ts)
> fact _ = error "Missing Factor"
> 
> moreterms [] = (Nomoreterms, [])
> moreterms ((TkOpn '+'):ts) = (Addop term1 mterms , ts'') where
>	  (term1, ts') = term ts
>         (mterms, ts'') = moreterms ts'

> moreterms ((TkOpn '-'):ts) = (Subop term1 mterms , ts'') where
>	  (term1, ts') = term ts
>         (mterms, ts'') = moreterms ts'
>
> moreterms ts = (Nomoreterms, ts)

> morefactors [] = (Nomorefacs, [])
> morefactors ((TkOpn '*'):ts) = (Mulop factor1 mfactors , ts'') where
>	  (factor1, ts') = fact ts
>         (mfactors, ts'') = morefactors ts'

> morefactors ((TkOpn '/'):ts) = (Divop factor1 mfactors , ts'') where
>	  (factor1, ts') = fact ts
>         (mfactors, ts'') = morefactors ts'
>
> morefactors ts = (Nomorefacs, ts)

\end{verbatim} 

\subsection{Code Generation}
Our last step is creating the code.  Each of the data types has a 
seperate function to create the corresponding code, returning a list of 
Strings. Pattern matching is used in the definitions of the functions to 
determine which code is written.
\begin{verbatim}

>
>   
> codegenP::Prog -> [String]
> codegenS::Statement -> [String]
> codegenE::Expression -> [String]
> codegenT::Term -> [String]
> codegenF::Factor -> [String]
> codegenMT::Moreterms -> [String]
> codegenMF::Morefactors -> [String]

> codegenP [] = []
> codegenP (x:xs) = codegenS x ++ codegenP xs

>
> codegenS (Print ex) = (codegenE ex) ++ ["PRINT"]
>
> codegenE (Exp te mt) = codegenT te ++ codegenMT mt
>
> codegenT (Trm fa mf) = codegenF fa ++ codegenMF mf
> codegenT (Sub te) = "cPUSH 0" : codegenT te    ++ ["OP2 -"]

> codegenF (Pars ex) = codegenE ex
> codegenF (Fnum i) = ["cPUSH "++show i] -- convert i to string
>
> codegenMT (Addop te mt) = codegenT te ++ ["OP2 +"] ++ codegenMT mt
> codegenMT (Subop te mt) = codegenT te ++ ["OP2 -"] ++ codegenMT mt
> codegenMT (Nomoreterms) = []

> codegenMF (Mulop fa mf) = codegenF fa ++ ["OP2 *"] ++ codegenMF mf
> codegenMF (Divop fa mf) = codegenF fa ++ ["OP2 /"] ++ codegenMF mf
> codegenMF (Nomorefacs) = []

\end{verbatim} 

\subsection{Main routine}

We then create the main routine by combining the functions together with
the input and output functions.

\begin{verbatim}

> makeCode::[Char] -> [IO()]
> makeCode = (map putStrLn).(codegenP.(parse.tokens))


> main   :: IO ()
> main = do c <- getContents
>	    sequence_ (makeCode c)


\end{verbatim} 



\end{document}

