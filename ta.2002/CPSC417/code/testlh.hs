import Lheap

a :: LeftistHeap Char

a = empty::(LeftistHeap Char)

b = insert 'm' a

c = insert 'f' b

d = insert 'r' c

e = insert 'p' d

f = insert 'n' e

g = insert 'g' f

h = insert 'h' g

i = insert 'z' h

j = insert 'd' i

k = insert 'e' j

l = insert 'c' k

m = insert 'b' l

n = insert 'a' m

o = insert 'i' n

p = insert 'j' o

q = insert 'k' p

r = insert 'r' q

s = insert 's' r

t = insert 't' s
u = insert 'a' t
v = insert 'v' u
w = insert 'b' v
x = insert 'a' w
y = insert 'y' x
z = insert 'z' y