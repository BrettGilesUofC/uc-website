\documentclass{article}

\usepackage{verbatim}

\title{CPSC 417: Assignment 1 possible solutions}

\author{Brett Giles et. al.}
\date{\today}

For simplicities sake we group the solutions to the assignments via
a module statement.
\begin{verbatim}

> module Ass1 where

\end{verbatim}

Thefirst function we are required to create is one that will append two
lists. We define it as type $[\alpha]\rightarrow[\alpha]\rightarrow[\alpha]$ 
meaning that will will take two lists of the same type and produce a third of 
that type. 

We have two cases for our definition. Appending an empty list to any list, 
resulting in that list, and a recursive definition where we use the 
list constructor $:$ to create the result list.
\begin{verbatim}

> append ::  [a] -> [a] -> [a]
> append [] ys      = ys
> append (x:xs) ys      = x : (append xs ys)

\end{verbatim}

Our next function is to take a list of lists into a single list. We use list 
comprehension, where the $xs$ will iterate over the $xss$ and then the $x$
will iterate over the $xs$. 
\begin {verbatim}

> flatten :: [[a]] -> [a]
> flatten xss = [x| xs<-xss, x<-xs]

\end {verbatim} 

If you don't use comprehensions, another way to define this function is via
pattern matching. Note the base case is $[]$ for the first one, not $[[]]$. The 
expression $[[]]$ is a list of a single element, the empty list.

\begin {verbatim}

 flatten [] = []
 flatten (x:xs) = append x (flatten xs)

\end {verbatim} 

Finally, if you had access to the prelude, you could define it via other functions.

\begin {verbatim}

  flatten           = foldr (++) []

\end {verbatim} 

Our next function, greaterinlist, is an obvious candiate for comprehensions once again.

\begin {verbatim}

> greaterinlist :: Integer->[Integer]->[Integer]
> greaterinlist x ys = [z | z<-ys, z > x]

\end {verbatim} 

Again, if pattern matching is used, this could be defined using a base case of any 
integer with the empty list, and a guarded pattern of an Int and a list.

\begin {verbatim}
 greaterinlist _ [] = []
 greaterinlist x (y:ys) 
     | y>x = y:(greaterinlist x ys)
     | otherwise  = greaterinlist x ys
\end {verbatim} 

We define our next function, msplit, in terms of two simpler functions, takeOdd,
and takeEven. Each of these are defined in terms of pattern matching. Note that 
each have two base cases as the recursive step removes two elements from the list.

\begin {verbatim}

> takeOdd :: [a] -> [a]
> takeOdd [] = []
> takeOdd [x] = [x]
> takeOdd (x:xs) = x : takeOdd ys 
>    where y:ys = xs

> takeEven :: [a] -> [a]
> takeEven [] = []
> takeEven [x] = []
> takeEven (x:xs) = y : takeEven ys 
>    where y:ys = xs

\end {verbatim} 

This leaves us with a nice trivial definition of the desired function.

\begin {verbatim}

> msplit::[a] -> ([a],[a])
> msplit xs = (takeOdd xs, takeEven xs)

\end {verbatim} 

A more direct definition can be done in a single step of course.



\begin{verbatim}
msplit         :: [a] -> ([a],[a])
msplit []       = ([],[])
msplit [x]      = ([x],[])
msplit (x:y:ys) = (x:o, y:e) 
                  where (o,e) = msplit ys
\end{verbatim} 


We now want to merge two lists of integers such that we always select the 
least first element at each step. That is 
\[mergeint ([1,2,45,3],[7,48,2]) = [1,2,7,45,3,48,2]\]

We use pattern matching, where we handle two base cases, either the first or
second list being empty. The recursive case uses guards to cons the least element
to the mergeInt of the remaining elements.
\begin {verbatim}

> mergeInt ::([Integer], [Integer]) -> [Integer]
> mergeInt ([],ys) = ys
> mergeInt (xs,[]) = xs
> mergeInt (x:xs,y:ys) 
>     | x < y = x:(mergeInt(xs,y:ys))
>     | otherwise = y:mergeInt(x:xs,ys)

\end {verbatim} 

We can now use our previous functions to define a merge sort on integers. Note 
that we have defined a splitting function above and therefore can use it in our
definition of the sort.

We need to handle two base cases. The empty list and singleton lists return those
same lists. After that, we use a standard recursive definition of the mergesort.

\begin {verbatim}
 
> mergesortInt::[Integer] -> [Integer]
> mergesortInt [] = []
> mergesortInt (x:[]) = [x]
> mergesortInt xs	= mergeInt (mergesortInt first, mergesortInt second)
>		  where (first,second) = msplit xs

\end {verbatim} 

Alternatively, if we had access to the prelude length function, we could have 
defined the function using guards as follows.

\begin {verbatim}
 mergesortInt xs
   | len xs < 2	= xs
   | otherwise  =  mergeInt (mergesortInt first, mergesortInt second)
		  where (first,second) = msplit xs
\end {verbatim} 



As stated in the question, we need a predicate that will test if two Integers differ
by at most 1. This is straightforward at this point.
\begin{verbatim}

> nbr ::  Integer -> Integer -> Bool
> nbr m n = abs (m-n) <= 1

\end{verbatim}

We now need to define $group$. We create the function $listify$ to assist us.
$listify$ will take a list and return a new list that has lists of size one as
it's elements. This is easiest to do via a list comprehension.

It's definition is:
\begin{verbatim}

> listify::[a]->[[a]]
> listify xs = [[x]|x<-xs]

\end{verbatim} 

Next we define $group'$ which is our precursor to $group$. It will have a 
similar signature to $group$.

\begin{verbatim}

> group' :: (a->a->Bool)->[[a]]->[[a]]

\end{verbatim} 

We want $group'$ to do the same thing as $group$, just on a list of lists. This is
fairly straighforward.
First, if the list is empty, it only makes sense to return the empty
list. Also, we note that the predicate requires two elements, so the single element
list is another base case.
\begin{verbatim}

> group' _ [] = []
> group' _ [q] = [q]

\end{verbatim} 

Finally, the recursive step.
\begin{verbatim}

> group' p (x1:x2:xs)
>     | p (last x1) (head x2) = group' (p) ((x1++x2):xs)
>     | otherwise  = x1 : (group' (p) (x2:xs))

\end{verbatim} 

Finally after all this preamble, we can define $group$.

> group :: (a->a->Bool)->[a]->[[a]]
> group p xs = group' p (listify xs)

\end{verbatim} 

Create a type synonym for matrices.

\begin{verbatim}

> type Matrix = [[Integer]]

\end{verbatim}

xzipWith will report an error 
if the lists are not the same length

\begin{verbatim}

> xzipWith                :: (a->b->c) -> [a]->[b]->[c]
> xzipWith f [] []         = []
> xzipWith f (x:xs) (y:ys) = f x y : xzipWith f xs ys

\end{verbatim} 


If we reach this point, we have a problem, the lengths of the list differ.
\begin{verbatim}

> xzipWith _ _ _ = error "xzipWith: list lengths differ"

\end{verbatim}

This will be the code for adding two matrices together. Recurse through
the list of lists, adding elements in the same position. Error if the matrices are different sizes.

\begin{verbatim}

> matrixAdd              :: Matrix -> Matrix -> Matrix
> matrixAdd [] []         = []
> matrixAdd (a:as) (b:bs) = xzipWith (+) a b : matrixAdd as bs

\end{verbatim}

Quickly define some functions for \texttt{head} and \texttt{tail}
of a list and the \texttt{map} function.

\begin{verbatim}
head'      :: [a] -> a
head' (x:_) = x
head' _     = error "head: can't take head of an empty list"

tail'       :: [a] -> [a]
tail' (_:xs) = xs
tail' _      = error "tail: can't take tail of an empty list"

map'         :: (a->b) -> [a] -> [b]
map' f []     = []
map' f (x:xs) = f x : map' f xs
\end{verbatim}

The rest of this here we will use for matrix multiplication.
First a function to transpose a matrix. To transpose a matrix
take all the elements of the first column and that is your first
row. Similarly for the second column, third column, etc. until
there are no more columns.  

\begin{verbatim}
transpose   :: Matrix -> Matrix
transpose [] = []  -- transpose of [] is [] 
transpose a  | head' a == [] = [] -- no more columns
             | otherwise = map' head' a : transpose (map' tail' a)
\end{verbatim}

\texttt{dotprod} calculates the dot product of two vectors.

\begin{verbatim}
dotprod :: Num a => [a] -> [a] -> [a]
dotprod  = xzipWith (*) 
\end{verbatim}

\texttt{matrixProd} will simply call \texttt{matrixProd'} with the
transpose of its second argument.

\begin{verbatim}
matrixProd    :: Matrix -> Matrix -> Matrix
matrixProd a b = matrixProd' a (transpose b)
\end{verbatim}

\texttt{flatten([sum (dotprod a b)] : matrixProd2 [a] bs)} will calculate
the first row of A $\times$ B. The rest of the rows then no longer depend
on \texttt{a} and so we calculate the rest of A $\times$ B with the
command \texttt{matrixProd' as (b:bs)}.

\begin{verbatim}
matrixProd'              :: Matrix -> Matrix -> Matrix
matrixProd' _ []          = []
matrixProd' [] _          = []
matrixProd' (a:as) (b:bs) = flatten([sum (dotprod a b)] : matrixProd' [a] bs)
                            : matrixProd' as (b:bs)
\end{verbatim}

\begin{verbatim}
	
> data ETree	= Var String
> 		| Opn String [ETree]
	
> varsof :: ETree -> [String]

\end{verbatim} 	
Given an ETree, we recurse throuth it, with the base case being a "Var"

\begin{verbatim}
> varsof (Var string)		= [string]
> varsof (Opn string etrees)	= flatten [varsof etree | etree <- etrees]
	
\end{verbatim} 




\end{document}

