

The identity function

> iden =(\x -> x)


A konstant function. 

> kon = (\x y -> x)


A sharing function. Applies f and g to x and then...?

> shar = (\ f g x -> (f x) (g x))


So what does this do?


> mystery = shar kon kon