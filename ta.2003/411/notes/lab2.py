#!/usr/bin/python
#
import lex, sys

tokens=('PRINT', 'LPAR', 'RPAR', 'SEMICOLON', 
        'NUM', 'ADD', 'SUB', 'MULT', 'DIV')

# Ignore whitespace.
t_ignore =    '\n\t '

t_ADD =   r'\+'
t_SUB =   r'-'
t_MULT =  r'\*'
t_DIV =   r'\/'

t_LPAR =   r'\('
t_RPAR =   r'\)'

t_SEMICOLON = r' ; '

t_PRINT =   r' print '

def t_NUM(t):
      r' \d+ '
      t.value=int(t.value)
      return t
   
  
def t_error(t):
      print "Illegal character %s" % repr(t.value[0])
      t.skip(1)

lex.lex()

if __name__ == "__main__":
      data = sys.stdin.read()

      lex.input(data)

      while 1:
            tok = lex.token()
            if not tok: break      # No more input
            print tok.type,
            if tok.type == "NUM": print "(%s)"%tok.value,
            print

