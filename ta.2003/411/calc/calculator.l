%pointer
%{
#define OTHER 1
#define PRINT 257
#define NUM 258
#define LPAR 259
#define RPAR 260
#define ADD 261
#define SUB 262
#define MULT 263
#define DIV 264
#define SEMICOLON 265
#define ERROR 0

%}

BLANK " "
TAB "\t"
NEWLINE "\n"
DIGITS [0-9]+
%%
({BLANK}|{TAB}|{NEWLINE})+  /* ignore whitespace */	 ;
print  	 return PRINT;
{DIGITS}  return NUM;
"("	 return LPAR;
")"	return RPAR;
"+"	 return ADD;
-	return SUB;
"*"	return MULT;
"/"	return DIV;
;       return SEMICOLON;
.	return OTHER;

%%

