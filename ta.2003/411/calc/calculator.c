#include <stdio.h>
#include <stdlib.h>
#define OTHER 1
#define PRINT 257
#define NUM 258
#define LPAR 259
#define RPAR 260
#define ADD 261
#define SUB 262
#define MULT 263
#define DIV 264
#define SEMICOLON 265
#define ERROR 0

extern char  *yytext;
extern int yylex(void);

#define PROGNODE 1001
#define SLISTNODE 1002
#define PRINTNODE 1003
#define EXPRNODE 1004
#define OPNODE 1005
#define NUMNODE 1006


typedef struct stree {
  int  nodetype;
  char op;
  int value;
  struct stree * dependants;
  struct stree * siblings;
} syntree;

syntree * theSyntaxTree;

syntree * prog();
syntree * stmtlist();
syntree * stmt();
syntree * expr();
syntree * term();
syntree * factor();
syntree * morefactors();
syntree * moreterms();
void matchtoken();
int currtoken;

syntree * makeNumNode(int value);
syntree * makeOpNode(char op, syntree* deps, syntree* sibs);
syntree * makeExprNode(syntree* terms, syntree* otherterms);
syntree * makePrintNode(syntree* expression);
syntree * makeSlistNode(syntree* statement, syntree* slist);
syntree * makeProgNode(syntree* slist);


int debugMe;

syntree * prog()
{
  debugMe &&  printf("prog\n");
  currtoken = getnexttoken();
  return makeProgNode(stmtlist());
}

syntree * stmtlist()
{
  syntree * stmtNode;
  syntree * slistNode;
  slistNode=NULL;
  debugMe && printf("stmtlist\n");
  stmtNode = stmt();
  if (currtoken == SEMICOLON)
  {
    matchtoken(SEMICOLON);
    slistNode = stmtlist();
  };
  return makeSlistNode(stmtNode,slistNode);
}

syntree * stmt()
{
  debugMe && printf("stmt\n");
  matchtoken(PRINT);
  return makePrintNode(expr());
}
syntree * expr()
{  
  syntree * theTerm, * additionalTerms;
  debugMe && printf("expr\n");
  theTerm=term();
  additionalTerms=moreterms();
  return makeExprNode(theTerm,additionalTerms);
}
syntree * term()
{
  syntree * theFactor, * additionalFactors;
  debugMe &&printf("term\n");

  if (currtoken == SUB)
  {
    matchtoken(SUB);
    return makeOpNode('n',term(),NULL);
  }
  else
  {
    theFactor=factor();
    additionalFactors=morefactors();
    return makeExprNode(theFactor,additionalFactors);
  }
}
syntree * factor()
{
  debugMe && printf("factor\n");
  if ( currtoken == LPAR)
  {
    syntree * ret;
    matchtoken(LPAR);
    ret = expr();
    matchtoken(RPAR);
    return ret;
  }
  else 
  {
    int val = atoi(yytext);
    matchtoken(NUM);
    return makeNumNode(val);
  }
}
syntree * morefactors()
{
  syntree * theFactor, * additionalFactors;
  debugMe && printf("morefactors");
  if (currtoken == MULT)
  {
    matchtoken(MULT);
    theFactor=factor();
    additionalFactors=morefactors();
 
    return makeOpNode('*',theFactor,additionalFactors);
  }  
  if (currtoken == DIV)
  {
    matchtoken(DIV);
    theFactor=factor();
    additionalFactors=morefactors();
 
    return makeOpNode('/',theFactor,additionalFactors);
  }
  else debugMe &&printf("(end mf)\n");
  return NULL;
}

syntree * moreterms()
{
  syntree * theTerm, * additionalTerms;
  debugMe && printf("moreterms");
  if (currtoken == ADD)
  {
    matchtoken(ADD);
    theTerm=term();
    additionalTerms=moreterms();

    return makeOpNode('+',theTerm,additionalTerms);
  }
  else   if (currtoken == SUB)
  {
    matchtoken(SUB);
    theTerm=term();
    additionalTerms=moreterms();

    return makeOpNode('-',theTerm,additionalTerms);
  }
  else debugMe && printf("(end mt)\n");
  return NULL;
}

void printcode(syntree * tr)
{
  if (tr == NULL) return;
  switch (tr->nodetype)
  {  
  case PROGNODE:
    printcode(tr->dependants);
    printf("echo program end\n");
    break;
  case SLISTNODE:
    printcode(tr->dependants);
    printcode(tr->siblings);
    break;
  case PRINTNODE:
    printcode(tr->dependants);
    printf("PRINT\n");
    printcode(tr->siblings);
    break;
  case EXPRNODE:
    printcode(tr->dependants);
    printcode(tr->siblings);
    break;
  case OPNODE:
    
    switch(tr->op)
      {
      case '+':case '*':
	printcode(tr->dependants);
	printf("OP2 %c\n",tr->op);
	printcode(tr->siblings);
	break;
      case  '-': case '/':
	printcode(tr->dependants);
	printf("OP2 %c\n",tr->op);
	printcode(tr->siblings);
	break;
      case 'n':
	printf("cPUSH 0\n");
	printcode(tr->dependants);
	printf("OP2 -\n");
	break;
      default:
	printf("Error - invalid op code:'%c'\n",tr->op);
      }
    break;
  case NUMNODE:
    printf("cPUSH %d\n",tr->value);
    printcode(tr->siblings);
    break;
  }
}

void writeCode()
{
  debugMe && printf("Code Gen called\n");
  printcode(theSyntaxTree);
}


int getnexttoken( void)
{
  return yylex();
}
void syntaxerror(int tok)
{
  printf("abandoning at token %d, %s\n", tok, yytext);
  exit (4);
}
void matchtoken(int token)
{
  if (currtoken == token)
  {
    currtoken = getnexttoken();
    debugMe && printf("Matching %d, new token is %d, '%s'\n",token, currtoken,yytext);
  }
  else
  {
    syntaxerror(currtoken);
  }
}

syntree * makeNumNode(int value)
{
  syntree* rval;
  rval = malloc(sizeof(syntree));
  rval->nodetype = NUMNODE;
  rval->value= value;
  rval->op = '\0';
  rval->dependants = rval->siblings = NULL;
  return rval;
}

syntree * makeOpNode(char op, syntree* deps, syntree* sibs)
{  
  syntree* rval;
  rval = malloc(sizeof(syntree));
  rval->nodetype = OPNODE;
  rval->value= 0;
  rval->op = op;
  rval->dependants = deps;
  rval->siblings = sibs;
  return rval;
}

syntree * makeExprNode(syntree* theTerm, syntree* otherterms)
{  
  syntree* rval;
  rval = malloc(sizeof(syntree));
  rval->nodetype = EXPRNODE;
  rval->value= 0;
  rval->op = '\0';
  rval->dependants = theTerm;
  rval->siblings = otherterms;
  return rval;
}

syntree * makePrintNode(syntree* expression)
{  
  syntree* rval;
  rval = malloc(sizeof(syntree));
  rval->nodetype = PRINTNODE;
  rval->value= 0;
  rval->op = '\0';
  rval->dependants = expression;
  rval->siblings = NULL;
  return rval;
}


syntree * makeSlistNode(syntree* statement,syntree* slist)
{  
  syntree* rval;
  if (slist == NULL)
    {
      rval = malloc(sizeof(syntree));
      rval->nodetype = SLISTNODE;
      rval->value= 0;
      rval->op = '\0';
      rval->dependants = statement;
      rval->siblings = NULL;
    }
  else
    {
      if (statement != NULL)
	{
	  statement->siblings = slist->dependants;
	  slist->dependants = statement;
	  rval = slist;
	}
      else
	rval = slist;
    }

  return rval;
}

syntree * makeProgNode(syntree* slist)
{  
  syntree* rval;
  rval = malloc(sizeof(syntree));
  rval->nodetype = PROGNODE;
  rval->value= 0;
  rval->op = '\0';
  rval->dependants = slist;
  rval->siblings = NULL;
  return rval;
}
int main(int argc, char ** argv)
{
  if (argc > 1) 
    debugMe=1; /* true */
  else
    debugMe = 0; /* false */

  theSyntaxTree = prog();
  writeCode();
 
}

