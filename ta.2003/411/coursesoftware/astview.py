#!/bin/env python
#----------------------------------------------------------------------------
# Name:         astview.py
# Purpose:      

import sys, os
from   wxPython.wx import *
import DOMTreeview

class myWindow(wxWindow):
    def __init__(self,parent,id):
        wxWindow.__init__(self,parent,id,style=wxNO_3D|wxSP_3D)
        #exitID = wxNewId()
        wxStaticText(self,-1,"Question:",wxPoint(20,20))
        self.quest = wxTextCtrl(self,-1,'',wxPoint(120,20),wxSize(400,40),style=wxTE_MULTILINE)
        wxStaticText(self,-1,"Additional:",wxPoint(20,62))
        self.lquest= wxTextCtrl(self,-1,'',wxPoint(120,62),wxSize(400,120),style=wxTE_MULTILINE)
        wxStaticText(self,-1,"Answer:",wxPoint(20,184))
        
        self.ans= wxTextCtrl(self,-1,'<p></p>',wxPoint(120,184),wxSize(400,200),style=wxTE_MULTILINE)
        self.update=wxButton(self,-1,'&Update',wxPoint(60,410))
        self.revert=wxButton(self,-1,'&Revert',wxPoint(180,410))
        self.revert.Enable(0)
        self.settingdata = 0

    def setData(self,short,long,answer):
        self.settingdata=1
        self.quest.SetValue(short)
        self.lquest.SetValue(long)
        self.ans.SetValue(answer)
        self.settingdata=0


#---------------------------------------------------------------------------

class wsAstViewer(wxFrame):

    def __init__(self, parent, id, title):
        wxFrame.__init__(self, parent, -1, title, size = (800, 600),
                         style=wxDEFAULT_FRAME_STYLE|wxNO_FULL_REPAINT_ON_RESIZE)

        EVT_CLOSE(self, self.OnCloseWindow)

        self.Centre(wxBOTH)
#        self.CreateStatusBar(1, wxST_SIZEGRIP)

        splitter = wxSplitterWindow(self, -1, style=wxNO_3D|wxSP_3D)

        # Prevent TreeCtrl from displaying all items after destruction
        self.dying = false
        # make a file dialog
        self.openfile=wxFileDialog(self,
                                   message = "Choose an Abstract Syntax Tree file",
                                   defaultDir = "",
                                   defaultFile = "",
                                   wildcard = "AST files (*.ast)|*.ast",
                                   style = wxOPEN | wxFILE_MUST_EXIST)
        # Make a File menu
        self.mainmenu = wxMenuBar()
        filemenu = wxMenu()
        openLeftID = wxNewId()
        filemenu.Append(openLeftID,
                        'Open &left...', 'Open an abstract syntax tree on the left side')
        openRightID = wxNewId()
        filemenu.Append(openRightID,
                        'Open &right...', 'Open an abstract syntax tree on the right side')
        exitID = wxNewId()
        filemenu.Append(exitID, 'E&xit\tAlt-X', 'Get the heck outta here!')
        EVT_MENU(self, openLeftID, self.OnFileOpenLeft)
        EVT_MENU(self, openRightID, self.OnFileOpenRight)
        EVT_MENU(self, exitID, self.OnFileExit)
        self.mainmenu.Append(filemenu, '&File')
        


        # Make a Help menu
        helpID = wxNewId()
        menu = wxMenu()
        menu.Append(helpID, '&About\tCtrl-H', 'wxPython RULES!!!')
        EVT_MENU(self, helpID, self.OnHelpAbout)
        self.mainmenu.Append(menu, '&Help')
        self.SetMenuBar(self.mainmenu)

        # set the menu accellerator table...
        aTable = wxAcceleratorTable([(wxACCEL_ALT,  ord('X'), exitID),
                                     (wxACCEL_CTRL, ord('H'), helpID)])
        self.SetAcceleratorTable(aTable)


        # Create a TreeCtrl
        trightTreeID = wxNewId()

        self.rightTree =  DOMTreeview.DOMTree(splitter, trightTreeID)


        tID = wxNewId()
        self.leftTree = DOMTreeview.DOMTree(splitter, tID)

        self.leftFile = ''
        self.rightFile = ''
        self.Show(true)
        
        splitter.SplitVertically(self.leftTree, self.rightTree)
        splitter.SetSashPosition(400, true)
        splitter.SetMinimumPaneSize(200)

        self.updateTitle()
        




    def updateTitle(self):
        title = 'AstViewer '
        if self.leftFile <> '':
            title = title + "- Left Side: %s"%self.leftFile
        if self.rightFile <> '':
            title = title + "- Right Side: %s"%self.rightFile
        self.SetTitle(title)
    
    #---------------------------------------------
    # Menu methods
    def OnFileOpenRight(self, *event):
        self.rightFile=self.openAFile(self.rightTree)
        self.updateTitle()

    def OnFileOpenLeft(self, *event):
        self.leftFile=self.openAFile(self.leftTree)
        self.updateTitle()

    def openAFile(self, whichtree):
        if (wxID_OK == self.openfile.ShowModal()):
            whichtree.openFile(self.openfile.GetPath())
            return self.openfile.GetFilename()
        return ''
        

    def OnFileExit(self, *event):
        self.Close()

    def OnPrint(self, *event):
        self.leftTree.printMe()


    def OnHelpAbout(self, event):
        from About import MyAboutBox
        about = MyAboutBox(self)
        about.ShowModal()
        about.Destroy()


    #---------------------------------------------
    def OnCloseWindow(self, event):
        self.dying = true
        self.window = None
        self.mainmenu = None
        if hasattr(self, "tbicon"):
            del self.tbicon
        self.Destroy()




    #---------------------------------------------
    def OnTaskBarClose(self, evt):
        self.Close()

        # because of the way wxTaskBarIcon.PopupMenu is implemented we have to
        # prod the main idle handler a bit to get the window to actually close
        wxGetApp().ProcessIdle()


#---------------------------------------------------------------------------
#---------------------------------------------------------------------------

class MyApp(wxApp):
    def OnInit(self):
        wxInitAllImageHandlers()
        frame = wsAstViewer(None, -1, "show XML ")
        frame.Show(true)
        self.SetTopWindow(frame)

        wxYield()
        return true



#---------------------------------------------------------------------------

def main():
    try:
        demoPath = os.path.split(__file__)[0]
        os.chdir(demoPath)
    except:
        pass
    app = MyApp(0)
    app.MainLoop()


#---------------------------------------------------------------------------



overview = """<html><body>
 <h2>Python</h2>

 Python is an interpreted, interactive, object-oriented programming
 language often compared to Tcl, Perl, Scheme, or Java.

 <p> Python combines remarkable power with very clear syntax. It has
 modules, classes, exceptions, very high level dynamic data types, and
 dynamic typing.  There are interfaces to many system calls and
 libraries, and new built-in modules are easily written in C or
 C++. Python is also usable as an extension language for applications
 that need a programmable interface.  <p>

 <h2>wxWindows</h2>

 wxWindows is a free C++ framework designed to make cross-platform
 programming child's play. Well, almost. wxWindows 2 supports Windows
 3.1/95/98/NT, Unix with GTK/Motif/Lesstif, with a Mac version
 underway. Other ports are under consideration.  <p>

 wxWindows is a set of libraries that allows C++ applications to
 compile and run on several different types of computers, with minimal
 source code changes.  There is one library per supported GUI (such as
 Motif, or Windows). As well as providing a common API (Application
 Programming Interface) for GUI functionality, it provides
 functionality for accessing some commonly-used operating system
 facilities, such as copying or deleting files. wxWindows is a
 'framework' in the sense that it provides a lot of built-in
 functionality, which the application can use or replace as required,
 thus saving a great deal of coding effort. Basic data structures such
 as strings, linked lists and hash tables are also supported.

 <p>
 <h2>wxPython</h2>

 wxPython is a Python extension module that encapsulates the wxWindows
 GUI classes. Currently it is only available for the Win32 and GTK
 ports of wxWindows, but as soon as the other ports are brought up to
 the same level as Win32 and GTK, it should be fairly trivial to
 enable wxPython to be used with the new GUI.

 <p>

 The wxPython extension module attempts to mirror the class heiarchy
 of wxWindows as closely as possible. This means that there is a
 wxFrame class in wxPython that looks, smells, tastes and acts almost
 the same as the wxFrame class in the C++ version. Unfortunately,
 because of differences in the languages, wxPython doesn't match
 wxWindows exactly, but the differences should be easy to absorb
 because they are natural to Python. For example, some methods that
 return multiple values via argument pointers in C++ will return a
 tuple of values in Python.

 <p>

 There is still much to be done for wxPython, many classes still need
 to be mirrored. Also, wxWindows is still somewhat of a moving target
 so it is a bit of an effort just keeping wxPython up to date. On the
 other hand, there are enough of the core classes completed that
 useful applications can be written.

 <p>

 wxPython is close enough to the C++ version that the majority of
 the wxPython documentation is actually just notes attached to the C++
 documents that describe the places where wxPython is different. There
 is also a series of sample programs included, and a series of
 documentation pages that assist the programmer in getting started
 with wxPython.

 """


#----------------------------------------------------------------------------
#----------------------------------------------------------------------------

if __name__ == '__main__':
    main()

#----------------------------------------------------------------------------







