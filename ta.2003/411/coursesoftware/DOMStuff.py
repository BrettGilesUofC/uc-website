
import string, sys
import xml.dom

def getsubtext(xmlnode,rmws = 1):
    retval=''
    if xmlnode.hasChildNodes():
        for cnd in xmlnode.childNodes:
            if cnd.nodeType == xml.dom.Node.TEXT_NODE:
                if rmws:
                    retval = retval + string.strip(cnd.nodeValue)
                else:
                    retval = retval + cnd.nodeValue
    return retval


def makeAttrsDict(xmlnode,basedict={}):
    
    atnodemap = xmlnode.attributes
    if atnodemap:
        for i in range(atnodemap.length):
            atnode = atnodemap.item(i)
            basedict[atnode.nodeName]=atnode.nodeValue
    return basedict
