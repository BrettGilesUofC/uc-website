#!/usr/bin/python

# Print out an xml tree structure.
from xml.dom.ext.reader import Sax2
from xml.dom.NodeFilter import NodeFilter
from xml.dom.ext import Canonicalize
from xml.dom.ext import Print
from DOMStuff import *

class tree:
    treeExpansionLevel=47
    def __init__(self,value,level=1):
        self.value = value
        self.children = []
        self.level = level
        
    def appendChild(self,value):
        newnode=tree(value,self.level+1)
        self.children.append(newnode)
        return newnode
        
    def find(self,value):
        if self.value == value : return self
        foundnode = None
        for child in self.children:
            foundnode = child.find(value)
            if foundnode: break
            raise 'value not found in tree'

    def setExpansion(self,level):
        tree.treeExpansionLevel = level
                
    def getLines(self):
        if self.level < tree.treeExpansionLevel:
            #            print self.value, 'Num children=', len(self.children),'level', self.level
            childrep = []
            for child in self.children[:-1] : # all but the last
                #print 'descending'
                rep = child.getLines()
                #print 'Got %d lines' % len(rep)
                if rep:
                    rep[0] = "+-"+ rep[0]
                    for r in range(1,len(rep)):
                        rep[r] = "| "+ rep[r]
                    childrep.extend(rep)
            try:
                #print 'getting last child'
                rep = self.children[-1].getLines()
                if rep:
                    #print rep
                    rep[0] = "\\-"+ rep[0]
                    #print rep
                    for r in range(1,len(rep)):
                        rep[r] = "  "+ rep[r]

                    childrep.extend(rep)
            except:
                pass
            if len(self.children) == 0:
                childrep.insert(0,"--%s"%self.value)
            else:
                childrep.insert(0,"+-%s"%self.value)
            return childrep
        elif self.level  == tree.treeExpansionLevel:
            if len(self.children) == 0:
                return ["--%s"%self.value]
            else:
                return ["+-%s"%self.value]
        else:
            return []

    def __repr__(self):
        '''return string of tree expanded to # levels in level'''
        return '\n'.join(self.getLines())

        
class compiler_tree:

        
    def __init__(self):
        self.root=None
        
    def openFile(self, filenm):
        self.doc=None
        self.docroot=None
        if self.root: self.root=None
        reader=Sax2.Reader()
        xmlfile=file(filenm,'r')
        self.doc=reader.fromStream(xmlfile)

        self.docroot = self.doc.documentElement
        self.makeTree(self.root,self.docroot)

    def makeTree(self,parent,xmlnode, xtradata=''):
        if xmlnode.hasChildNodes():
            for cnd in xmlnode.childNodes:
                if cnd.nodeType == self.docroot.ELEMENT_NODE:
                    if cnd.nodeName=='ast':
                        for subchild in cnd.childNodes:
                            self.makeTree(parent,subchild)
                    elif cnd.nodeName=='child':
                        attrs=makeAttrsDict(cnd,basedict={'type':''})
                        if attrs['type'] == '':
                            chExtra = ''
                        else:
                            chExtra = "(%s):"%attrs['type']
                        self.makeTree(parent,cnd,xtradata=chExtra)
                    elif cnd.nodeName == 'leaf':
                        attrs=makeAttrsDict(cnd)
                        text=getsubtext(cnd)
                        name = 'Leaf: (%s,%s="%s")'%(attrs['type'],attrs['name'],text)
                        if parent:
                            newnode = self.AppendItem(parent,xtradata+name)
                        else:
                            newnode = parent=self.AddRoot(xtradata+name)
                        self.makeTree(newnode,cnd)
                    elif cnd.nodeName == 'node':
                        attrs=makeAttrsDict(cnd,basedict={'type':'', 'name':''})
                        name = attrs['name']
                        if attrs['type'] <> '':
                            name = name + '(%s)'%(attrs['type'])
                        if parent:
                            newnode = self.AppendItem(parent,xtradata+name)
                        else:
                            newnode = parent=self.AddRoot(xtradata+name)
                        self.makeTree(newnode,cnd)


    def AddRoot(self,string):
        self.root = tree(string)
        return self.root

    def AppendItem(self, treenode, data):
        return treenode.appendChild(data)


    def setExpansion(self,level):
        self.root.setExpansion(level)
                
                    
    def getLines(self):
        return self.root.getLines()

    def __repr__(self):
        return self.root.__repr__()

        

if __name__ == "__main__":
    abc=compiler_tree()
    rt = abc.AddRoot("prog")
    nd1 = abc.AppendItem(rt,"declaration")
    ndd1=abc.AppendItem(nd1,"leaf var")
    ndd2=abc.AppendItem(ndd1,"X")
    abc.AppendItem(nd1,"another")
    nd2 = abc.AppendItem(rt,"ReadStatement")
    nd3 = abc.AppendItem(nd2,"id")
    nd4 = abc.AppendItem(nd3,"arrayvalues")
    nd5= abc.AppendItem(nd4,"Five")
    nd6= abc.AppendItem(nd5,"Six")

    print rt
    rt.setExpansion(3)
    print rt
