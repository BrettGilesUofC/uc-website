#!/usr/bin/python
"""   printast.py
accept an abstract syntax tree in the xml format defined in
class, (node/child/leaf)  and print it. Optionally accept
a  level of expansion.

Usage: printast.py [options] astfile

Options:
    -h / --help
        Print this message and exit.

    -e expansionnum
    --expansion=expansionnum
        Print the <expansionnum> levels only.
        expansionnum must be an integer
        Defaults to 42. (See HitchHikers Guide to Galaxy for why.)
"""
import sys
import getopt
from treeprint import compiler_tree
from xml.sax._exceptions import SAXParseException
def usage(code, msg=''):
    print >> sys.stderr, __doc__
    if msg:
        print >> sys.stderr, msg
    sys.exit(code)


def treeprint(explevel, inpfile):
    asttree = compiler_tree()
    try:
        asttree.openFile(inpfile)
        print
        print inpfile   #Just the name... :)
        print
        asttree.setExpansion(explevel)
        print asttree
    except SAXParseException, x:
        print 'Parsing Exception(file:line:col: err): %s'%x
    except:
        print "Had an error making tree for:",inpfile
        raise


if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'he:', ['help', 'expansion='])
    except getopt.error, msg:
        usage(1, msg)

    explevel=42
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            usage(0)
        elif opt in ('-e', '--expansion'):
            try:
                explevel = int(arg)
            except:
                usage(1,"Can't seem to convert %s to an integer."%arg)

    try:
        thefile = args[0]
    except IndexError:
        usage(1,"Missing a file name.")

    treeprint(explevel, thefile)
