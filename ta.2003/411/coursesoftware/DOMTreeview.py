
import string, sys
from xml.dom.ext.reader import Sax2
from xml.dom.NodeFilter import NodeFilter
from xml.dom.ext import Canonicalize
from xml.dom.ext import Print
from DOMStuff import *

from wxPython.wx import *
class DOMTree(wxTreeCtrl):
    def __init__(self, parent, ID,filenm=None):
        wxTreeCtrl.__init__(self, parent, ID)
        self.parent = parent
        self.TopLevel = self.AddRoot("Abstract Syntax Tree")
        if filenm:
            self.openFile(filenm)


    def openFile(self, filenm):
        self.doc=None
        self.docroot=None
        self.DeleteChildren(self.TopLevel)
        reader=Sax2.Reader()
        xmlfile=file(filenm,'r')
        self.doc=reader.fromStream(xmlfile)

        self.docroot = self.doc.documentElement
        self.makeTree(self.TopLevel,self.docroot)

        #        newelt=self.AppendItem(self.TopLevel,
#                               self.docroot.nodeName,
#                               data=wxTreeItemData(self.docroot))
#        
#        self.makeTree(newelt,self.docroot)

            
    def printMe(self):
        Print (self.doc)

    def makeTree(self,parent,xmlnode, xtradata=''):
        if xmlnode.hasChildNodes():
            for cnd in xmlnode.childNodes:
                if cnd.nodeType == self.docroot.ELEMENT_NODE:
                    if cnd.nodeName=='ast':
                        for subchild in cnd.childNodes:
                            self.makeTree(parent,subchild)
                    elif cnd.nodeName=='child':
                        attrs=makeAttrsDict(cnd,basedict={'type':''})
                        if attrs['type'] == '':
                            chExtra = ''
                        else:
                            chExtra = "(%s):"%attrs['type']
                        self.makeTree(parent,cnd,xtradata=chExtra)
                    elif cnd.nodeName == 'leaf':
                        attrs=makeAttrsDict(cnd)
                        text=getsubtext(cnd)
                        name = 'Leaf: (%s,%s="%s")'%(attrs['type'],attrs['name'],text)
                        newnode = self.AppendItem(parent,xtradata+name,data=wxTreeItemData(cnd))
                        self.makeTree(newnode,cnd)
                    elif cnd.nodeName == 'node':
                        attrs=makeAttrsDict(cnd,basedict={'type':'', 'name':''})
                        name = attrs['name']
                        if attrs['type'] <> '':
                            name = name + '(%s)'%(attrs['type'])
                        newnode = self.AppendItem(parent,xtradata+name,data=wxTreeItemData(cnd))
                        self.makeTree(newnode,cnd)
                






