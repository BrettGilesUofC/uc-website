#!/bin/bash
#Assumes it is being run in a directory containing the student directories. The script will
#combine the files in that directory with a header and a footer, , then convert them to 2up 
# postscript. 
MARKSCRIPT=../markit.sh
ASSIGNNUM=2
MARKSCHEME=../markScheme
if [ $# -ne 0 ] ; then
    PARMS=1
    while [[ $PARMS == 1 ]] ; do
       case $1 in 
	    -m) shift; MARKSCRIPT=$1 ; shift
		    ;;
	    -s) shift; MARKSCHEME=$1 ; shift
		    ;;
	    -a) shift; ASSIGNNUM=$1 ; shift
		    ;;
	    -?) echo "Usage: $0 [-m markscriptname] [-a assignnum] [-s scheme] dirs"
	        exit 0
		    ;;
	    *) PARMS=0
	    ;;
	esac
     done
fi

names=
for dir in $*
do  
    if [[ $dir == 'CVS' ]] ; then continue ; fi
    if [ -d $dir ] ; then
	    printf "\n\n\n\n New Student: (Assignment $ASSIGNNUM 411) $dir\n\n" >$dir.assignment
	    $MARKSCRIPT $dir > $dir/testresults.txt
	    cd $dir
	    rm -f lextab.py* parser.out parsetab.py* *.pyc
	    ls -l >>../$dir.assignment
	    printf "\f" >>../$dir.assignment 
	    for fil in *.py *.txt ; do
		if [[ "X$fil" == "Xlex.py" ]] ; then echo "" >/dev/null
		elif [[ "X$fil" == "Xyacc.py" ]] ; then echo "" >/dev/null
		elif [[ "X$fil" == "Xparsetab.py" ]] ; then echo "" >/dev/null
		elif [[ "X$fil" == "Xparser.out" ]] ; then echo "" >/dev/null
		elif [[ "X$fil" == "XCVS" ]] ; then echo "" >/dev/null
		else 
		    echo $fil
                    printf "\f************Start of $fil************\n" >> ../$dir.assignment 
		    cat $fil >>../$dir.assignment 
                    printf "*************End of $fil*************\n" >> ../$dir.assignment 
		fi
	    done
	    rm testresults.txt
	    cd ..
	    printf "\f\n\n\n\n" >>$dir.assignment
	    printf "\tMarks for Assignment $ASSIGNNUM for :$dir\n\n\n"  >>$dir.assignment
	    cat $MARKSCHEME >>$dir.assignment

	    names="$names $dir.assignment"
   fi
done

enscript -A 2 -2r -Ge -p assignment.ps $names
rm -f  *.assignment



