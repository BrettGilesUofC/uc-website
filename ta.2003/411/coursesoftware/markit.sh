#!/bin/bash
INPDIR=../../testsin 
OUTDIR=../../testsout

for dir in $*
do
  if [[ $dir == 'CVS' ]] ; then continue ; fi
  if [ -d $dir ] ; then
    cd $dir
    if [ -e mpm.py ] ; then
	for file in `ls $INPDIR`; do
	  if [[ $file == 'CVS' ]] ; then continue ; fi
	
	  python mpm.py < $INPDIR/$file 2>&1 > ${file%.m+-}.ast
	  if [[ $file == 'test1.m+-' ]] ; then
	     echo " "
	     echo "test1.ast"
	     cat test1.ast
	  else
            if [[ $file == 'test2.m+-' ]] ; then
	      echo " "
	      echo "test2.ast"
              cat test2.ast
	    else printast.py -e 5 ${file%.m+-}.ast
            fi
          fi
        done
    fi
    cd ..
  fi
done



