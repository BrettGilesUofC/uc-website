<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
  <xsl:include href="../../../styles/base.xsl" />
  <xsl:output method="xml" encoding="iso-8859-1" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
      <xsl:apply-templates/>
    </html>
  </xsl:template>
  <xsl:template match="faq">
    <xsl:variable name="faqid">
      <xsl:value-of select="generate-id()" />
    </xsl:variable>
    <head> 
    <xsl:copy-of select="$basecascadingss" />
    <xsl:copy-of select="$basescripts" />
 
          <title>
            <xsl:value-of select="@title" />
            </title>
   </head>
 
    <body>
      <xsl:copy-of select="$basemenu" />
<div class="page">
      <h1>
         <a><xsl:attribute name="name">
         <xsl:value-of select="generate-id(//faq)" />
        </xsl:attribute>
        <xsl:apply-templates select="@title" />
        </a>
        </h1>
        <xsl:apply-templates  select="category" mode="toc" >
            <xsl:sort select="@name" />
            </xsl:apply-templates>
      <h1>The answers.</h1>
      <xsl:apply-templates  select="category" mode="all" >
             <xsl:sort select="@name" />
             </xsl:apply-templates>
             	<address class="center">Last modified by
	  <a href="http://www.cpsc.ucalgary.ca/~gilesb/">Brett Giles</a><br />
	<!-- hhmts start -->Last modified: Sat Feb 22 13:05:45 MST 2003 <!-- hhmts end -->
	</address>
              <p class="center">
      <a href="http://validator.w3.org/check/referer"><img
      src="/~gilesb/images/valid-xhtml10.png"
          alt="Valid XHTML 1.0!" height="31" width="88" /></a>
    </p>
      </div>
      </body>
    </xsl:template>
    <xsl:template match="category" mode="toc">
      <h3><xsl:value-of select="@name" /></h3>
      <ol>
        <xsl:apply-templates select="questionlist/question/short" mode="qlist" />
      </ol>
    </xsl:template>
    <xsl:template match="short"  mode="qlist">
      <li> <a><xsl:attribute name="href">#<xsl:value-of select="generate-id()" /></xsl:attribute>
          <xsl:value-of select="." />
      </a>
      </li>
        </xsl:template>
    <xsl:template match="category" mode="all">
      <h2><xsl:value-of select="@name" /></h2>
      <xsl:apply-templates select="questionlist/question" mode="qans" />
      </xsl:template>
<xsl:template match="question" mode="qans" >
  <xsl:apply-templates select="short" mode="qans" />
      <xsl:apply-templates select="long" />
<h5 class="red">Answer:</h5>
      <xsl:apply-templates select="answer" />
        <a><xsl:attribute name="href">#<xsl:value-of  select="generate-id(//faq)" /></xsl:attribute>
Back to the Top.</a>
      </xsl:template>
      <xsl:template match="short" mode="qans">
        <h4 class="question"><a><xsl:attribute name="name"><xsl:value-of select="generate-id()" /></xsl:attribute>
        <xsl:value-of select="." />
      </a>
</h4>     
      </xsl:template>
      <xsl:template match="@assign"><xsl:value-of select="." /></xsl:template>
      <xsl:template match="@name"><xsl:value-of select="." /></xsl:template>
      <xsl:template match="long|answer">
        <xsl:copy-of  select="./*" />
      </xsl:template>
    </xsl:stylesheet>
