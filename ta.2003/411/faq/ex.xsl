<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

  <xsl:output method="html"/>

  <xsl:template match="/">
    <html>
      <body bgcolor="#FFFFFF">
        <xsl:apply-templates/>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

  <xsl:output method="html"/>
  <xsl:template match="/">
    <html>
      <xsl:apply-templates/>
    </html>
  </xsl:template>
  <xsl:template match="faq">
    <head>
      <title>
        <xsl:apply-templates select="@title" />
        </title>
      </head>
    </xsl:template>
  <xsl:template match="faq">
    <body bgcolor="#FFFFFF">
      <h1>
        <xsl:apply-templates select="@title" />
        </h1>
        <ol>
        <xsl:apply-templates select="short" />
      </ol>
      <xsl:apply-templates />
      </body>
    </xsl:template>
    <xsl:template match="short">
      <li><xsl:value-of select="." /></li>
    </xsl:template>
    <xsl:template match="question">
      <h3>          <xsl:value-of select="short"/></h3>
      <p><xsl:value-of select="long" /></p>
      <xsl:value-of select="answer" />
      </xsl:template>
    </xsl:stylesheet>