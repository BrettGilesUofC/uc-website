'''
Version 1.1 as of 2003-04-01 1:30PM
    Fixed "makeEntry"
    Changed getStorage
    '''

labelgen=0

from mpexcep import semanticError

VARIABLEOFFSETSTART=0
ARGUMENTOFFSETSTART=-4



class stVariableArrayEntry:
    def __init__(self,level,offset, variable_type, dimensions):
        self.level = level
        self.variable_type=variable_type
        self.offset=offset
        self.dimensions = dimensions

    def entry_type(self):
        return self.variable_type

    def i_am_an_array(self): return 1
    def i_am_a_function(self): return 0      

    def retsdata(self):
        return (self.level,self.offset,self.variable_type, self.dimensions)

class stVariableEntry:
    def __init__(self,level,offset,variable_type):
        self.level = level
        self.variable_type=variable_type
        self.offset=offset

    def entry_type(self):
        return self.variable_type

    def i_am_an_array(self): return 0

    def i_am_a_function(self): return 0      

    def retsdata(self):
        return (self.level,self.offset,self.variable_type)

class stArgumentEntry(stVariableArrayEntry): # just to have a different type...
    def __init__(self,level,offset,argument_type,arraydimensions):
        stVariableArrayEntry.__init__(self,level,offset,argument_type,arraydimensions)
    def i_am_an_array(self): return self.dimensions


class stFunctionEntry:
    def __init__(self,level,codelabel,function_type,function_argtypes):
        self.level = level
        self.function_argtypes=function_argtypes
        self.function_type=function_type
        self.code_label=codelabel

    def entry_type(self):
        return self.function_type

    def i_am_an_array(self): return 0

    def i_am_a_function(self): return 1

    def retsdata(self):
        return (self.level,
                self.code_label,
                self.function_argtypes,
                self.function_type)

#doesn't need to be a class really.... Only one instance
class SymbolTable:
    def __init__(self):
        self.__symbol_table={}   # dict of ids -> [stentries]
        self.level=0
        self.identifiers_at_level_list=[[]]  # list of ids at this level
        self.argument_offset_list = []
        self.variable_offset_list = []
        self.array_sizes = []

    def __repr__(self):
        return self.__symbol_table.__repr__() + \
               self.identifiers_at_level_list.__repr__()

    def newscope(self):
        self.level += 1
        self.identifiers_at_level_list.append([])
        self.array_sizes.append([])
        self.argument_offset_list.append(ARGUMENTOFFSETSTART)
        self.variable_offset_list.append(VARIABLEOFFSETSTART)


    def removescope(self):
        if self.level == 0: return
        try:
            for id in self.identifiers_at_level_list[-1]:
                self.__symbol_table[id].pop()
                if 0 == self.__symbol_table[id].length():
                    del self.__symbol_table[id]
            self.identifiers_at_level_list.pop()
            self.array_sizes.pop()
            self.argument_offset_list.pop()
            self.variable_offset_list.pop()
            self.level -= 1
        except:
            print "Internal symbol table error"
            raise

    def storage(self):
        return (self.variable_offset_list[-1],self.array_sizes[-1])

    def lookup(self, lookup_id):
        try:
            sentry = self.__symbol_table[lookup_id][-1]
        except:
            raise semanticError("Identifier not found:"+lookup_id)
        return sentry

    def insert(self,declaration):
        '''Does not handle overloading!,
        declaration is an instance of the class
        declaration and therefore is a var, array, fun or arg'''
        (id, entry) = make_entry(declaration, level)

        if id in self.identifiers_at_level_list[-1]:
            raise semanticError("Duplicate id." + id())

        try:
            self.__symbol_table[d].append(newentry)
        except:
            self.__symbol_table[d] = [newentry]

    def insertbuiltin(self,fname,type,argtypes):
        '''CHANGE REQUIRED: builtins like +, - etc are defined
        on both the int and real types. You must figure out how to
        do this and what changes that will require in this module
        and in your check routines in ast.py'''

        if fname in self.identifiers_at_level_list[-1]:
            raise semanticError("Duplicate builtin.")
        newentry=stFunctionEntry(self.level, 'builtin',type,argtypes)
        try:
            self.__symbol_table[fname].append(newentry)
        except:
            self.__symbol_table[fname] = [newentry]

    def make_entry(declaration):
        if declaration.isvar() and len(declaration.arraydimensions) <> 0:
            newentry = stVariableArrayEntry(self.level,
                                            self.variable_offset_list[-1],
                                            declaration.dtype,
                                            declaration.arraydimensions)
            self.array_sizes[-1].append((self.variable_offset_list[-1],
                                         declaration.arraydimensions))
            self.variable_offset_list[-1] +=1
        elif declaration.isvar():
            newentry = stVariableEntry(self.level,
                                       self.variable_offset_list[-1],
                                       declaration.dtype)
            self.variable_offset_list[-1]+=1
        elif declaration.isfun():
            newentry = stFunctionEntry(self.level,
                                       getlabel( declaration.id()),
                                       declaration.dtype,
                                       declaration.funargtypes())
        else:
            newentry= stArgumentEntry(self.level,
                                      self.argument_offset_list[-1],
                                      declaration.dtype,
                                      declaration.arraydims)
            self.argument_offset_list[-1] -=1
        return (declaration.id(), newentry)


def getlabel(s):
    global labelgen
    labelgen+=1
    return "func_%s_%d"(s,labelgen)
