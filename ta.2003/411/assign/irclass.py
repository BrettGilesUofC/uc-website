#CHANGE REQUIRED
##To use this file, you must do the following:
##    - Document it
##    - Change the various init routines to actually save the data passed
##      in
##    - Add __repr__ methods to all of the classes that print out
##      the data in our standard xml node/child/leaf format.




class iProgram:
    '''Completely done for you, just add comments.'''
    def __init__(self,
                 function_body_list,
                 storage,
                 array_info_list,
                 statement_list):
        self.function_body_list = function_body_list
        self.storage=storage
        self.array_info_list = array_info_list
        self.statement_list=statement_list

        def __repr__(self):
            children=[]
            for function in self.function_body_list:
                children.append(child(function))
            for array in self.array_info_list:
                children.append(child(array))
            for statement in self.statement_list:
                children.append(child(statement))

            return ('<ast>\n'+
                    node('iProgram',
                         leaf('int', 'storage', self.storage) +
                         ''.join(children)) +
                    '</ast>\n')



class iFunctionBody:
    def __init__(self,
                 function_code_start_label
                 ,function_body_list
                 ,number_of_function_arguments
                 ,storage
                 ,array_info_list
                 ,statement_list):
        self.function_code_start_label=function_code_start_label
        self.number_of_function_arguments=number_of_function_arguments
        self.function_body_list = function_body_list
        self.storage=storage
        self.array_info_list = array_info_list
        self.statement_list=statement_list

    def iAmAFunction(self):
        return 1
    def iAmAnArray(self):
        return 0



class iArray:
    def __init__(self, offset, expression_list):
        pass

    def iAmAFunction(self): return 0
    def iAmAnArray(self): return 1


class iAssignVariable:
    def __init__(self,offset,level,expression):
        pass

class iAssignArrayElement:
    def __init__(self,offset,level,arrayaddress, expression):
        pass

class iWhile:
    def __init__(self,expression, statement):
        pass


class iCond:
    def __init__(self,expression, false_statement, true_statement):
        pass

class iReadVariable:
    def __init__(self,offset, level, variable_type):
        pass

class iReadArrayElement:
    def __init__(self,offset, level, variable_type, arrayaddres):
        pass

class iPrint:
    def __init__(self,expression, expression_type):
        pass

class iReturn:
    def __init__(self,expression):
        pass

class iBlock:
    def __init__(self,
                 function_body_list,
                 storage,
                 array_info_list,
                 statement_list):
        self.function_body_list = function_body_list
        self.storage=storage
        self.array_info_list = array_info_list
        self.statement_list=statement_list


class iIntegerExpression:
    def __init__(self,integer_value):
        self.integer_value = integer_value

class iBooleanExpression:
    def __init__(self,boolean_value):
        self.boolean_value = boolean_value

class iFloatExpression:
    def __init__(self,float__value):
        self.integer_value = integer_value

class iSimpleIdentifier:
    def __init__(self,level,offset):
        self.level = level
        self.offset = offset

class iArrayIdentifier:
    def __init__ (self,level,offset,arrayaddress):
        pass

class iFunctionOrOperationApplication:
    def __init__(self, function_or_operation, argument_value_list):
        pass

class iFunction:
    def __init__(self,function_code_label,level):
        self.function_code_label = function_code_label
        self.level=level

class iOperation:
    def __init__(self,operation):
        self.operation = self.operation

