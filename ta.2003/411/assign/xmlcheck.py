#!/usr/bin/python

#from xml.sax import saxutils  #ContentHandler
from xml.sax import make_parser
from xml.sax.handler import feature_namespaces,ContentHandler
import sys
import string

class node:
    def __init__(self, name,typ=None):
        self.name=name
        self.typ=typ
        self.nodes={}
        self.leaves={}


    def addChildNode(self, childtype, node):
        self.nodes[childtype] = node

    def addChildLeaf(self,leafname, leaftype, leafdata):
        self.leaves[leafname]=(leaftype,leafdata)

    def __repr__(self):
        rep = "('''"+self.name +"''',\n'''"+self.typ+ "''',\n'''"
        for x in self.nodes.keys():
            rep = rep+x+":"+self.nodes[x].__repr__()

        for x in self.leaves.keys():
            rep = rep+x+":(leaf):"+self.leaves[x].__repr__()

        return rep


def normalize_whitespace(text):
    "Remove redundant whitespace from a string"
    return ' '.join(text.split())


class astHandler(ContentHandler): 

    def __init__(self):
        self.inleaf = 0
        self.leafdata = ''
        self.leaftype =''
        self.leafname = ''
        self.mainnode=None
        self.anode = None
        self.nodename=''
        self.nodetype=''
        self.childnodes = []   #childtype, node
        self.childtype = ''
        self.childleaves = [] #leafname, leaftype, leafdata
        self.childinnodecount = []

        self.leafInChild = []
        self.nodeInChild = []
        self.firstNode = 1

    def theNode(self):
        return self.anode

    def ignorableWhitespace(self):
        pass
    
    def startElement(self,name,attrs):
        allowatts=0
        if name == 'node':
            try:
                self.nodename = attrs["name"]
                allowatts += 1
            except:
                print "Missing name attribute from 'node' element."
                sys.exit(4)
            try:
                self.nodetype = attrs["type"]
                allowatts+=1
            except:
                self.nodetype = ''
            if len(attrs) <> allowatts:
                print "Invalid attributes in 'node' element. Found:"
                print attrs.keys()
                sys.exit(4)
            self.childinnodecount.append(0)

            if self.firstNode:
                self.firstNode = 0
            else:
                try:
                    nc = self.nodeInChild.pop()
                    nc += 1
                    self.nodeInChild.append(nc)
                except:
                    print "Node not under child???"
                    sys.exit(4)
                    

            
        elif name == 'child':
            allowatts=0
            try:
                self.childtype = attrs["type"]
                allowatts += 1
            except:
                self.childtype = 'Child'
            if len(attrs) <> allowatts:
                print "Invalid attributes in 'child' element. Found:"
                print attrs.keys()
                sys.exit(4)
            try:
                cc = self.childinnodecount.pop()
                cc += 1
                self.childinnodecount.append(cc)
            except:
                print "child not under node...?"
                sys.exit(4)
            self.nodeInChild.append(0)
            self.leafInChild.append(0)

        elif name == 'leaf':
            allowatts = 0
            try:
                self.leafname = attrs["name"]
                self.leaftype = attrs["type"]
                allowatts = 2
            except:
                print "Missing name or type attribute from 'leaf' element."
                sys.exit(4)
            if len(attrs) <> allowatts:
                print "Invalid attributes in 'leaf' element. Found:"
                print attrs.keys()
                sys.exit(4)
            self.leafdata = ''
            self.inleaf = 1
            try:
                lc = self.leafInChild.pop()
                lc += 1
                self.leafInChild.append(lc)
            except:
                print "Leaf not under child???"
                sys.exit(4)
                    

    def characters(self, ch):
        if self.inleaf:
            self.leafdata = self.leafdata + ch
        elif ch.isspace():
            pass
        else:
            print "Data outside of leaf container", ch
            sys.exit(4)

    def endElement(self, name):
        if name == 'leaf':
            if self.leafdata.isspace():
                print 'Leaf without any data.'
                sys.exit(4)
            self.childleaves.append((self.leafname, self.leaftype, self.leafdata))
            self.leafdata = ''
            self.leaftype =''
            self.leafname = ''
            self.inleaf = 0
        elif name == 'child':
            if self.anode:
                self.childnodes.append((self.childtype, self.anode))
                self.anode = None

            self.childtype = ''
            lc = self.leafInChild.pop()
            nc = self.nodeInChild.pop()
            subc = lc+nc
            if subc == 0:
                print 'child element has no node or leaf below it.'
                sys.exit(4)
            if subc >1:
                print 'child element has more than one node or leaf below it.'
                sys.exit(4)
        elif name == 'node':
            self.anode = node(self.nodename, self.nodetype)
            for child in self.childnodes:
                self.anode.addChildNode(child[0],child[1])
            self.childnodes = []
            for lf in self.childleaves:
                self.anode.addChildLeaf(lf[0],lf[1],lf[2])
            self.childleaves = []
            try:
                cc = self.childinnodecount.pop()
            except:
                print 'node  element has no child below it.'
                print self.nodename, self.nodetype
                sys.exit(4)
            if cc == 0:
                print '(Warning) node  element has no child below it.(OK for block only)'
                print self.nodename, self.nodetype
                

def error(self, exception):
      import sys
      sys.stderr.write("%s\n" % exception)

def parseit(filename):
    parser = make_parser()
    parser.setFeature(feature_namespaces, 0)
    gh = astHandler()
    parser.setContentHandler(gh)
    parser.parse(open(filename))
    return gh.theNode()

if __name__ == '__main__':

    # Create a parser
    parser = make_parser()

    # Tell the parser we are not interested in XML namespaces
    parser.setFeature(feature_namespaces, 0)

    # Create the handler
    gh = astHandler()
    #dh = FindIssue('Sandman', '62')

    # Tell the parser to use our handler
    parser.setContentHandler(gh)

    # Parse the input
    parser.parse(sys.stdin)

    print "Looks ok at first glance :)"

    


