#CPSC411 -- Datatypes for M+- in python
#You must fully document this module if your are using it.
#Failure to do so will result in the loss of marks.

from tree import *
from irclass import *
from mpexcep import semanticError

#data M_prog = M_prog ([M_decl],[M_stmt])
# below, ir stands for intermediate representation in all cases.

class prog:
    def __init__(self,declaration_list,statement_list):
        self.declaration_list = declaration_list  # a list
        self.statement_list = statement_list # a list

    def __repr__(self):
        items = []
        for declaration in self.declaration_list:
            items.append(child(declaration,typ='declaration'))
        for stmt in self.statement_list:
            items.append(child(stmt,typ='statement'))
        ret="<ast>\n"+node('prog', ''.join(items)) + "</ast>\n"
        return ret

    def check(self,symbol_table):
        for declaration in self.declaration_list:
            symbol_table.insert(declaration)

        ir_of_array_list=[]
        ir_of_function_body_list=[]
        for declaration in self.declaration_list:
            ir_of_declaration= declaration.check(symbol_table)
            if ir_of_declaration != None :
                if ir_of_declaration.iAmAFunction():
                    ir_of_function_body.append(ir_of_declaration)
                else:
                    ir_of_array_list.append(ir_of_declaration)
        ir_of_statement_list=[]
        for statement in self.statement_list:
            ir_of_statement_list.append(s.check(symbol_table))
        return iProgram(ir_of_function_body_list,
                        symbol_table.storage(),
                        ir_of_array_list,
                        ir_of_statement_list)

    def setfuncrettype(self,rettype):
        for statement in self.statement_list:
            s.settype(rettype)


#data M_decl == M_var (string,M_type,[M_expr])
#            | M_fun (string ,[(string,M_type,Int)],M_type,M_prog)

class decl:
    def __init__(self,typ, id=None,dtype=None,arraydim=None,
                 funargdeclaration_list=None,funprog=None):
        self.typ=typ   # VAR or FUN or ARG
        self.id=id  # the id (for all types)
        self.dtype=dtype # Bool or Int or Real (for all types)
        self.arraydim=arraydim # list of dimensions or in the case of ARG, the number of dimensions.
        self.funargdeclaration_list=funargdeclaration_list # a list of argument declaration_list(with dimensions)
        self.funprog=funprog  # an instance of prog

    def check(self,symbol_table):
        '''CHANGE REQUIRED - you need to write this function'''
        pass

    def id(self):
        return self.id

    def isvar(self):
        return self.typ == 'var'

    def isfun(self):
        return self.typ == 'fun'


    def funargtypes(self):
        ret=[]
        for  x in self.funargdeclaration_list:
            ret.append(x.dtype)
        return ret


    def __repr__(self):
        ret=""
        #print self.typ
        if self.typ =="var":
            item = ''
            #print self.arraydim
            for ad in self.arraydim:
                item = item + child(ad)
            ret = node('Var declaration' ,
                       leaf('string','id',self.id) +
                       item + leaf('string','type',self.dtype))
            return ret
        elif self.typ =="arg":
            dimension=''
            if self.arraydim > 0:
                dimension = leaf('int',
                                 'dimensions',
                                 self.arraydim)
            return node('Argument declaration',
                        leaf('string', 'id',self.id) + dimension +
                        leaf('string','type', self.dtype))
        else:
            argdeclaration_list = ''
            for declaration in self.funargdeclaration_list:
                argdeclaration_list = argdeclaration_list + child(declaration)

            return node('Function Declaration',
                        leaf('string', 'id', self.id) +
                        leaf('string','type',self.dtype) +
                        argdeclaration_list +
                        child(self.funprog))

#data M_stmt = M_ass (string,M_expr)
#            | M_while (M_expr,M_stmt)
#            | M_cond (M_expr,M_stmt,M_stmt) 
#            | M_read string
#            | M_print M_expr
#            | M_return M_expr
#            | M_block ([M_decl],[M_stmt])

class stmt:
    def __init__(self):
        "A base class to make life easier - use it or do it like decl."
        pass

    def settype(self,t):
        pass

class ass_stmt(stmt):
    def __init__(self,id,arrayaddress,exp):
        self.id=id
        self.arrayaddress = arrayaddress
        self.exp = exp

    def skind(self):
        return "ass"

    def __repr__(self):

        addresses = []
        for aa in self.arrayaddress:
            addresses.append(child(aa,typ="array address"))
        return node('assign stmt',
                    leaf('string','id',self.id) +
                    ''.join(addresses) +
                    child(self.exp,typ="assignment value"))

class while_stmt(stmt):
    def __init__(self,exp,stmt):
        self.exp = exp
        self.stmt = stmt

    def __repr__(self):
        return node('While stmt', child(self.exp)+child(self.stmt))

    def skind(self):
        return "while"

    def check(self,symbol_table):
        (ir_of_expression,expression_type)=self.exp.check(symbol_table)
        if expression_type != 'bool' :
            raise semanticError("While expression not of type bool")
        ir_of_statement=self.stmt.check(symbol_table)
        return iWhile(ir_of_expression, ir_of_statement)


class cond_stmt(stmt):
    def __init__(self,exp,true_statement,false_statement):
        self.exp = exp
        self.true_statement=true_statement
        self.false_statement=false_statement

    def __repr__(self):
        return node('IF stmt',
                    child(self.exp)+
                    child(self.true_statement,typ='True statement')+
                    child(self.false_statement,typ='False statement'))

    def skind(self):
        return "cond"

    def check(self,symbol_table):
        (ir_of_expression,expression_type)=self.exp.check(symbol_table)
        if expression_type != 'bool' :
            raise semanticError("If expression not of type bool")
        ir_of_true_statement=self.true_statement.check(symbol_table)
        ir_of_false_statement=self.false_statement.check(symbol_table)
        return iCond(ir_of_expression,ir_of_true_statement,ir_of_false_statement)


class read_stmt(stmt):
    def __init__(self,id,arrayaddress):
        self.id=id
        self.arrayaddress = arrayaddress

    def __repr__(self):
        addresses=[]
        for aa in self.arrayaddress:
            addresses.append(child(aa,typ='array address'))
        return node('Read stmt',
                    leaf('string','id',self.id) + ''.join(addresses))

    def skind(self):
        return "read"

class print_stmt(stmt):
    def __init__(self,exp):
        self.exp=exp

    def __repr__(self):
        return node('Print statement',child(self.exp))

    def skind(self):
        return "print"

 

class ret_stmt(stmt):
    def __init__(self,exp):
        self.exp=exp

    def __repr__(self):
        return node('Return statement',child(self.exp))

    def skind(self):
        return "ret"

    def settype(self,type):
        self.type=type




class block_stmt(stmt):
    def __init__(self,declaration_list,statement_list):
        self.declaration_list = declaration_list
        self.statement_list = statement_list

    def __repr__(self):
        children=[]
        for declaration in self.declaration_list:
            children.append(child(declaration))
        for statement in self.statement_list:
            children.append(child(statement))
        return node('Block statement',''.join(children))

    def skind(self):
        return "block"


    def check(self,symbol_table):
        symbol_table.newscope()
        for declaration in self.declaration_list:
            symbol_table.insert(declaration)

        ir_of_function_body_list=[]
        ir_of_array_list=[]
        for declaration in self.declaration_list:
            ir_of_declaration=declaration.check(symbol_table)
            if ir_of_declaration != None :
                if ir_of_declaration.iAmAFunction():
                    ir_of_function_body_list.append(ir_of_declaration)
                else:
                    ir_of_array_list.append(ir_of_declaration)
        ir_of_statement_list=[]
        for statement in self.statement_list:
            ir_of_statement_list.append(statement.check(symbol_table))
        storage=symbol_table.storage()
        symbol_table.removescope()
        return iBlock(ir_of_function_body_list,
                      storage,
                      ir_of_array_list,
                      ir_of_statement_list)


#data M_type = M_int | M_bool
# Not needed in python - use a string or enumerated value  if you want.

#data M_expr = M_num Int
#            | M_bl Bool
#            | M_id String
#            | M_app (M_operation,[M_expr])

class expr:
    def __init__(self, numv=None,
                 bval=None,
                 rval=None,
                 id=None,
                 arrayaddress=None,
                 apply=None):
        self.numv=numv
        self.bval=bval
        self.rval=rval
        self.id=id
        self.arrayaddress=arrayaddress
        self.apply=apply

    def __repr__(self):
        if self.numv <>None :
            return node('expr',
                        leaf('int', 'int value', self.numv),
                        typ="ival")
        if self.bval <>None :
            return node('expr',
                        leaf('bool', 'bool value', self.bval),
                        typ="bval")
        if self.rval <>None :
            return node('expr',
                        leaf('real', 'real value',self.rval),
                        typ="rval")
        if self.id <>None :
            addresses = []
            for aa in self.arrayaddress:
                addresses.append(child(aa, typ='array address'))
            return node('expr',
                        leaf('string', 'id', self.id) +
                        ''.join(addresses),
                        typ="identifier")

        appfn=self.apply[0]
        appfn = appfn.replace('&&','&amp;&amp;');
        appfn = appfn.replace('<','&lt;')
        appfn = appfn.replace('>','&gt;')

        appfnparms=self.apply[1]

        parms = []
        for p in appfnparms:
            parms.append(child(p,typ='Function parameter'))

        return node('expr',
                    leaf('string','Function name',appfn) +
                    ''.join(parms),
                    typ="apply")

    def check(self,symbol_table):
        if self.numv <> None :
            return (iIntegerExpression(self.numv),'int')
        if self.bval <> None :
            return (iBooleanExpression(self.bval),'bool')
        if self.rval <> None :
            return (iFloatExpression(self.rval),'real')
        #CHANGE REQUIRED - you must code the ones for id.
        if self.id <> None : pass

        #else we are in an apply operation
        #CHANGE REQUIRED - Builtins like +, * etc must work for
        # both integer and real types.
        appop=self.apply[0]   # a string
        appargs=self.apply[1] # A list of  args

        function_entry = symbol_table.lookup(appop)
        fargtypes = function_entry.function_argtypes
        if len(fargtypes) != len(appargs):
            raise semanticError("Bad function call:"+appop)

        irargs = []
        for apparg in appargs:
            irargs.append(apparg.check(symbol_table))
        #CHANGE REQUIRED - This does not handle overloading of builtins.    
        for i in range(len(fargtypes)):
            if fargtypes[i] != irargs[i][1]:
                raise semanticError("%s has bad type in arg %declaration."%(appop,i))
        ir_of_expressions=[]
        for x in irargs:
            ir_of_expressions.append(x[0])

        if function_entry.codelabel == "builtin":
            op=iOperation(appop)
        else:
            op=iFunction(function_entry.codelabel,
                         function_entry.level)

        return (iFunctionOrOperationApplication(op,
                                                ir_of_expressions),
                function_entry.function_type)



