#Keep looking back, more code may appear....

from tree import *

label=0

def duplicateTopOfStack(count=1):
    for i in range(count):
        print "LOAD_R %%sp %% Duplicate to %d items on stack "%count
        print "LOAD_O %d"%(1-count)

def newlabel():
    global label
    label += 1
    return "cd%d"%label

class iProgram:
    def __init__(self,
                 function_body_list,
                 storage,
                 array_info_list,
                 statement_list):
        self.function_body_list = function_body_list
        self.storage=storage
        self.array_info_list = array_info_list
        self.statement_list=statement_list

    def __repr__(self):
        return node('program',leaf('int','storage',self.storage)+
                    childList(self.array_info_list) +
                    childList(self.function_body_list)+
                    childList(self.statement_list))

    def genCode(self,callingLevel):
        #Startup and exit done for you, you must fill in details.
        print "LOAD_R %sp"
        print "LOAD_I %d"%(1-self.storage)
        print "LOAD_R %sp"
        print "STORE_R %fp"
        print "ALLOC %d"%self.storage
        #stuff missing here....

        
        print "HALT"
        # more stuff missing here

        print '''arrbndserror: LOAD_C 'A'
        PRINT_C
        LOAD_C 'r'
        PRINT_C
        LOAD_C 'r'
        PRINT_C
        LOAD_C 'a'
        PRINT_C
        LOAD_C 'y'
        PRINT_C
        LOAD_C ' '
        PRINT_C
        LOAD_C 'b'
        PRINT_C
        LOAD_C 'o'
        PRINT_C
        LOAD_C 'u'
        PRINT_C
        LOAD_C 'n'
        PRINT_C
        LOAD_C 'd'
        PRINT_C
        LOAD_C 's'
        PRINT_C
        LOAD_C ' '
        PRINT_C
        LOAD_C 'e'
        PRINT_C
        LOAD_C 'r'
        PRINT_C
        LOAD_C 'r'
        PRINT_C
        LOAD_C 'o'
        PRINT_C
        LOAD_C 'r'
        PRINT_C
        HALT'''

class iFunctionBody:
    def __init__(self,
                 function_code_start_label
                 ,storage
                 ,number_of_function_arguments
                 ,function_body_list
                 ,array_info_list
                 ,statement_list):
        self.function_code_start_label=function_code_start_label
        self.number_of_function_arguments=number_of_function_arguments
        self.function_body_list = function_body_list
        self.storage=storage
        self.array_info_list = array_info_list
        self.statement_list=statement_list

    def iAmAFunction(self): return 1
    def iAmAnArray(self): return 0

    def __repr__(self):
        return node("Func Dec",
                    leaf('string','codelabel',
                         self.function_code_start_label)+
                    leaf('int','numofargs',
                         self.number_of_function_arguments)+
                    leaf('int','storage',self.storage)+
                    childList(self.array_info_list) +
                    childList(self.function_body_list)+
                    childList(self.statement_list))


    def genCode(self,callingLevel):
        '''As done in classAs done in class, see lab pages for explanation.'''
        # The deallocation pointer first.
        basestorage=self.storage+self.number_of_function_arguments +2
        print "%s: LOAD_I %d"%(self.function_code_start_label,
                               -basestorage) # deallocation    
        print "LOAD_R %sp"
        print "STORE_R %fp"
        print "ALLOC %d"%(self.storage)
        for arr in self.array_info_list:
            arr.genCode(callingLevel)
        for s in self.statement_list:
            s.genCode(callingLevel)

        print "LOAD_R %fp" 
        print "STORE_O %d"%(-(self.number_of_function_arguments+4) )  # return value
        print "LOAD_R %fp" 
        print "LOAD_O -3"  # dyn link
        print "LOAD_R %fp"
        print "STORE_O %d"%(-(self.number_of_function_arguments+2) )  #previous dynamic link
        print "LOAD_R %fp"
        print "LOAD_O -1"  # code ptr
        print "LOAD_R %fp"
        print "STORE_O %d"%(-(self.number_of_function_arguments+3) )  #previous code pointer
        print "LOAD_R %fp"
        print "LOAD_O 0" #dealloc counter
        print "ALLOC_S"
        print "STORE_R %fp"  
        print "JUMP_S"



        for f in self.function_body_list:
            f.genCode(callingLevel+1)

class iArray:
    def __init__(self,  offset, datatype, expression_list):
        self.offset = offset
        self.datatype = datatype
        self.expression_list = expression_list

    def iAmAFunction(self): return 0
    def iAmAnArray(self): return 1

    def genCode(self,callingLevel):
        '''As done in class, see lab pages for explanation.
        Generate code to allocate enough space for the
        array and put the dimensions in the first slots.'''
        # Generate the first dimension and store the sp into
        # the offset.
        self.expression_list[0].genCode(callingLevel)
        print "LOAD_R %sp"
        print "LOAD_R %fp"
        print "STORE_O %d"%self.offset
        # put the other dimensions on top of the stack.
        for dim in self.expression_list[1:]:
            dim.genCode(callingLevel)
            
        #Copy all the dimensions up and multiply them,
        #giving the total amount of space for the array.
        number_of_dims = len(self.expression_list)
        for i in range(number_of_dims):
            print "LOAD_R %sp"
            print "LOAD_O -%d"%(number_of_dims -1) #const offset from tos.
        print (number_of_dims - 1)*"APP MUL\n",

        # Total size now on top of stack. Copy that and adjust
        # the deallocation counter.
        print "LOAD_R %sp"
        print "LOAD_O 0"
        print "LOAD_I %d"%number_of_dims
        print "APP ADD"
        print "LOAD_R %fp"
        print "LOAD_O 0"
        print "APP SUB"
        print "LOAD_R %fp"
        print "STORE_O 0"
        #leaves size on top of stack, so allocate.
        print "ALLOC_S"
    
    def __repr__(self):
        return node("Array decl",
                    leaf('int','offset',self.offset) +
                    leaf('string', 'datatype', self.datatype)+
                    childList(self.expression_list))
    


class iAssignVariable:
    def __init__(self,level,offset,expression):
        self.offset=offset
        self.level=level
        self.expression=expression

    def __repr__(self):
        return node("Assign to var",
                    leaf("int",'level',self.level)+
                    leaf("int",'offset', self.offset)+
                    child(self.expression))

        
class iAssignArrayElement:
    def __init__(self,level,offset,arrayaddressList, expression):
        self.offset=offset
        self.level=level
        self.arrayaddressList = arrayaddressList
        self.expression=expression
    def __repr__(self):
        return node("Assign to arrayElt",
                    leaf("int",'level',self.level)+
                    leaf("int",'offset', self.offset)+
                    childList(self.arrayaddressList)+
                    child(self.expression))


class iWhile:
    def __init__(self,expression, statement):
        self.statement = statement
        self.expression=expression

    def __repr__(self):
        return node("While",
                    child(self.expression)+
                    child(self.statement))


class iCond:
    def __init__(self,expression, true_statement, false_statement):
        self.false_statement = false_statement
        self.true_statement = true_statement
        self.expression=expression
    def __repr__(self):
        return node("If",
                    child(self.expression)+
                    child(self.true_statement, typ="True path")+
                    child(self.false_statement, typ="False path"))

    
 
class iReadVariable:
    def __init__(self,level, offset, variable_type):
        self.offset=offset
        self.level=level
        self.variable_type = variable_type
        

    def __repr__(self):
        return node("Read var",
                    leaf("int",'level',self.level)+
                    leaf("int",'offset', self.offset)+
                    leaf("string",'type', self.variable_type))


class iReadArrayElement:
    def __init__(self,level, offset, variable_type, arrayaddressList):
        self.offset=offset
        self.level=level
        self.variable_type = variable_type
        self.arrayaddressList = arrayaddressList

    def __repr__(self):
        return node("Read arrayElt",
                    leaf("int",'level',self.level)+
                    leaf("int",'offset', self.offset)+
                    leaf("string",'type', self.variable_type)+
                    childList(self.arrayaddressList))

 
class iPrint:
    def __init__(self,expression, expression_type):
        self.expression = expression
        self.expression_type = expression_type

    def __repr__(self):
        return node("Print",
                    leaf("string",'type', self.expression_type)+
                    child(self.expression))



class iReturn:
    def __init__(self,expression):
        self.expression = expression

    def __repr__(self):
        return node("Return",
                    child(self.expression))
    
 
class iBlock:
    def __init__(self,
                 function_body_list,
                 storage,
                 array_info_list,
                 statement_list):
        self.function_body_list = function_body_list
        self.storage=storage
        self.array_info_list = array_info_list
        self.statement_list=statement_list

    def __repr__(self):
        return node('Block',leaf('int','storage',self.storage)+
                    childList(self.array_info_list) +
                    childList(self.function_body_list)+
                    childList(self.statement_list))


class iIntegerExpression:
    def __init__(self,integer_value):
        self.integer_value = integer_value

    def __repr__(self):
        return node('IntExpression',
                    leaf('int','value', self.integer_value))

class iBooleanExpression:
    def __init__(self,boolean_value):
        self.boolean_value = boolean_value

    def __repr__(self):
        return node('BoolExpression',
                    leaf('bool','value', self.boolean_value))

class iFloatExpression:
    def __init__(self,float_value):
        self.float_value = float_value

    def __repr__(self):
        return node('FloatExpression',
                    leaf('real','value', self.float_value))

class iSimpleIdentifier:
    def __init__(self,level,offset):
        self.level = level
        self.offset = offset

    def __repr__(self):
        return node("Simple ID",
                    leaf("int",'level',self.level)+
                    leaf("int",'offset', self.offset))

 
class iArrayIdentifier:
    def __init__ (self,level,offset,arrayaddressList):
        self.level = level
        self.offset = offset
        self.arrayaddressList = arrayaddressList
        
    def __repr__(self):
        return node("Array ID",
                    leaf("int",'level',self.level)+
                    leaf("int",'offset', self.offset)+
                    childList(self.arrayaddressList))

 
class iFunctionOrOperationApplication:
    def __init__(self, function_or_operation, argument_value_list):
        self.function_or_operation = function_or_operation
        self.argument_value_list = argument_value_list

    def __repr__(self):
        return node("Apply",
                    child(self.function_or_operation)+
                    childList(self.argument_value_list))

 

class iFunction:
    def __init__(self,function_code_label,level):
        self.function_code_label = function_code_label
        self.level=level

    def __repr__(self):
        return node("Function",
                    leaf("string","codeLabel",self.function_code_label)+
                    leaf("int","level",self.level))

                

class iOperation:
    def __init__(self,operation,typ):
        self.operation = operation
        self.typ = typ

    def __repr__(self):
        op = self.operation.replace('&&','&amp;&amp;');
        op = op.replace('<','&lt;')
        op = op.replace('>','&gt;')
        return node("Operation",
                    leaf("string","op",op)+
                    leaf("string","type",self.typ))


