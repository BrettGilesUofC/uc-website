#!/usr/bin/python
import lex
from cmntlex import remove_comments
from mpparse import mpparse
from st import *
import sys

from mpexcep import semanticError

inp=remove_comments(sys.stdin.read())
newprog=mpparse(inp)

stab = SymbolTable()

stab.newscope()
stab.insertbuiltin('+','int',['int','int'])
stab.insertbuiltin('+','real',['real','real'])
stab.insertbuiltin('-','int',['int','int'])
stab.insertbuiltin('-','real',['real','real'])
stab.insertbuiltin('*','int',['int','int'])
stab.insertbuiltin('*','real',['real','real'])
stab.insertbuiltin('/','int',['int','int'])
stab.insertbuiltin('/','real',['real','real'])
stab.insertbuiltin('<','bool',['int','int'])
stab.insertbuiltin('<','bool',['real','real'])
stab.insertbuiltin('>','bool',['int','int'])
stab.insertbuiltin('>','bool',['real','real'])
stab.insertbuiltin('=','bool',['int','int'])
stab.insertbuiltin('=','bool',['real','real'])
stab.insertbuiltin('>=','bool',['int','int'])
stab.insertbuiltin('>=','bool',['real','real'])
stab.insertbuiltin('=<','bool',['int','int'])
stab.insertbuiltin('=<','bool',['real','real'])
stab.insertbuiltin('NEG','int',['int'])
stab.insertbuiltin('NEG','real',['real'])
stab.insertbuiltin('||','bool',['bool','bool'])
stab.insertbuiltin('&&','bool',['bool','bool'])
stab.insertbuiltin('not','bool',['bool'])
stab.insertbuiltin('SIZE','int',['real','int'])
stab.insertbuiltin('SIZE','int',['bool','int'])
stab.insertbuiltin('SIZE','int',['int','int'])
stab.insertbuiltin('float','real',['int'])
stab.insertbuiltin('CIEL','int',['real'])
stab.insertbuiltin('FLOOR','int',['real'])

try:
      irpr = newprog.check(stab)
#      print "<ast>"
#      print irpr
#      print "</ast>"
      irpr.genCode(0)
except semanticError, se:
      print se


