#CPSC411 -- Datatypes for M+- in python

from tree import *
from irclass import *
from mpexcep import semanticError

def checkID(st, id, arrayaddress):
    stentry=st.lookup(id)
    iraas = []
    if stentry.num_of_dimensions() <> len(arrayaddress):
        raise semanticError("Indexing error - invalid number of dimensions.")
    if stentry.i_am_an_array():
        for aa in arrayaddress:
            (iraa, aatype,aadims) = aa.check(st)
            iraas.append(iraa)
            if aatype <> 'int' or aadims > 0:
                raise semanticError("Array index must be integer.")

    return (stentry.level,stentry.offset,stentry.entry_type(),iraas)
    

#data M_prog = M_prog ([M_decl],[M_stmt])
# below, ir stands for intermediate representation in all cases.

class prog:
    def __init__(self,declaration_list,statement_list):
        self.declaration_list = declaration_list  # a list
        self.statement_list = statement_list # a list

    def __repr__(self):
        "XML based  representation."
        item = ''
        for declaration in self.declaration_list:
            item = item + child(declaration,typ='declaration')
        for stmt in self.statement_list:
            item  = item + child(stmt,typ='statement')
        ret="<ast>\n"+node('prog', item) + "</ast>\n"
        return ret

    def check(self,symbol_table):
        for declaration in self.declaration_list:
            symbol_table.insert(declaration)

        ir_of_array_list=[]
        ir_of_function_body_list=[]
        for declaration in self.declaration_list:
            ir_of_declaration= declaration.check(symbol_table)
            if ir_of_declaration != None :
                if ir_of_declaration.iAmAFunction():
                    ir_of_function_body_list.append(ir_of_declaration)
                else:
                    ir_of_array_list.append(ir_of_declaration)
        ir_of_statement_list=[]
        for statement in self.statement_list:
            ir_of_statement_list.append(statement.check(symbol_table))
        varstore = symbol_table.storage()
        return iProgram(ir_of_function_body_list,
                        varstore,
                        ir_of_array_list,
                        ir_of_statement_list)


#data M_decl == M_var (string,M_type,[M_expr])
#            | M_fun (string ,[(string,M_type,Int)],M_type,M_prog)

# old m+            M_var (string,M_type)
#            | M_fun (string ,[(string,M_type)],M_type,M_prog)

class decl:
    def __init__(self,typ, id=None,dtype=None,arraydim=None,
                 funargdeclaration_list=None,funprog=None):
        self.typ=typ   # VAR or FUN or ARG
        self.id=id  # the id (for all types)
        self.dtype=dtype # Bool or Int or Real (for all types)
        self.arraydim=arraydim # list of dimensions or in the case of ARG, the number of dimensions.
        self.funargdeclaration_list=funargdeclaration_list # a list of argument declaration_list(with dimensions)
        self.funprog=funprog  # an instance of prog

    def check(self,symbol_table):
        if self.typ == 'var':
            if len(self.arraydim) == 0:
                return None
            else:
                ir_of_expression_list=[]
                for dimension in self.arraydim:
                    (ir_of_expression, expression_type,expr_dims) \
                                       = dimension.check(symbol_table)
                    if (expression_type <> 'int' or expr_dims > 0):
                        raise semanticError('Dimension must be integer type.')
                    ir_of_expression_list.append(ir_of_expression)
                symbol_entry = symbol_table.lookup(self.id)
                return iArray(symbol_entry.offset,
                              self.dtype,
                              ir_of_expression_list)
        if self.typ == 'arg' : return None  # dimensions....?

        #otherwise, functionwith new scope etc...
        symbol_table.newscope()
        for arg_declaration in self.funargdeclaration_list:
            symbol_table.insert(arg_declaration)

        ir_of_function_body=self.funprog.check(symbol_table)
        (unused,returntype,unuseddims) = self.funprog.statement_list[-1].exp.check(symbol_table)
        symbol_table.removescope()

        symbol_entry=symbol_table.lookup((self.id, self.funargtypes()))
        if returntype <> symbol_entry.function_type:
            raise semanticError(
                "Return expression not of type %s in function %s."%(
                symbol_entry.function_type,self.id))       
        
        return iFunctionBody(symbol_entry.code_label,
                             ir_of_function_body.storage,
                             len(self.funargdeclaration_list),
                             ir_of_function_body.function_body_list,
                             ir_of_function_body.array_info_list,
                             ir_of_function_body.statement_list)

    def id(self):
        return self.id

    def isvar(self):
        return self.typ == 'var'
    def isfun(self):
        return self.typ == 'fun'


    def funargtypes(self):
        ret=[]
        for  x in self.funargdeclaration_list:
            ret.append(x.dtype)
        return tuple(ret)

    def funargdims(self):
        ret=[]
        for  x in self.funargdeclaration_list:
            ret.append(x.arraydim)
        return tuple(ret)


    def __repr__(self):
        ret=""
        #print self.typ
        if self.typ =="var":
            item = ''
            #print self.arraydim
            for ad in self.arraydim:
                item = item + child(ad)
            ret = node('Var declaration' , leaf('string','id',self.id) + item + leaf('string','type',self.dtype))
            return ret
        elif self.typ =="arg":
            dimension=''
            if self.arraydim > 0:
                dimension = leaf('int','dimensions',self.arraydim)
            return node('Argument declaration', leaf('string', 'id',self.id) + dimension +
                        leaf('string','type',  self.dtype))
        else:
            argdeclaration_list = ''
            for declaration in self.funargdeclaration_list:
                argdeclaration_list = argdeclaration_list + child(declaration)

            return node('Function Declaration',leaf('string', 'id', self.id) +
                        leaf('string','type',self.dtype) + argdeclaration_list + child(self.funprog))

#data M_stmt = M_ass (string,M_expr)
#            | M_while (M_expr,M_stmt)
#            | M_cond (M_expr,M_stmt,M_stmt) 
#            | M_read string
#            | M_print M_expr
#            | M_return M_expr
#            | M_block ([M_decl],[M_stmt])


class ass_stmt:
    def __init__(self,id,arrayaddress,exp):
        self.id=id
        self.arrayaddress = arrayaddress
        self.exp = exp

    def check(self,st):
        (lvl,off,typ,iraa)=checkID(st,self.id,self.arrayaddress)
        (ire,etype, edims) = self.exp.check(st)
        if etype != typ:
            raise semanticError("Mixed types in assign to %s. Expecting %s, got %s."%(
                self.id,typ,etype ))
        if edims > 0:
            raise semanticError("Can not assign an array to %s."%(
                self.id))
        if (len(iraa) > 0):
            return iAssignArrayElement(lvl,
                                       off,
                                       iraa,
                                       ire)
        else:
            return  iAssignVariable(lvl,off,ire)

    def __repr__(self):

        addr = ''
        for aa in self.arrayaddress:
            addr = addr+child(aa,typ="array address")
        return node('assign stmt', leaf('string','id',self.id) + addr + child(self.exp,typ="assignment value"))

class while_stmt:
    def __init__(self,exp,stmt):
        self.exp = exp
        self.stmt = stmt

    def __repr__(self):
        return node('While stmt', child(self.exp)+child(self.stmt))



    def check(self,symbol_table):
        (ir_of_expression,expression_type,expression_dims)=self.exp.check(symbol_table)
        if expression_type != 'bool' :
            raise semanticError("While expression not of type bool.")
        if expression_dims > 0 :
            raise semanticError("While expression can not be an array.")
        irs=self.stmt.check(symbol_table)
        return iWhile(ir_of_expression,irs)


class cond_stmt:
    def __init__(self,exp,tstmt,fstmt):
        self.exp = exp
        self.tstmt=tstmt
        self.fstmt=fstmt

    def __repr__(self):
        return node('IF stmt', child(self.exp)+ child(self.tstmt,typ='True statement')+
                              child(self.fstmt,typ='False statement'))


    def check(self,symbol_table):
        (ir_of_expression,expression_type, expression_dims)=self.exp.check(symbol_table)
        if expression_type != 'bool' :
            raise semanticError("If expression not of type bool")
        if expression_dims > 0 :
            raise semanticError("If expression can not be an array.")
        irts=self.tstmt.check(symbol_table)
        irfs=self.fstmt.check(symbol_table)
        return iCond(ir_of_expression,irts,irfs)


class read_stmt:
    def __init__(self,id,arrayaddress):
        self.id=id
        self.arrayaddress = arrayaddress

    def __repr__(self):
        addr=''
        for aa in self.arrayaddress:
            addr = addr + child(aa,typ='array address')
        return node('Read stmt',leaf('string','id',self.id) + addr)


    def check(self,symbol_table):
        (lvl,off,typ,iraa)=checkID(symbol_table,self.id,self.arrayaddress)
        if (len(iraa) > 0):
            return iReadArrayElement(lvl,
                                     off,
                                     typ,
                                     iraa)
        else:
            return  iReadVariable(lvl,off,typ)


class print_stmt:
    def __init__(self,exp):
        self.exp=exp

    def __repr__(self):
        return node('Print statement',child(self.exp))


    def check(self,symbol_table):
        (ir_of_expression,expression_type,expression_dims)=self.exp.check(symbol_table)
        if expression_dims > 0 :
            raise semanticError("Print expression can not be an array.")

        return iPrint(ir_of_expression, expression_type)
    

class ret_stmt:
    def __init__(self,exp):
        self.exp=exp

    def __repr__(self):
        return node('Return statement',child(self.exp))

    def check(self,symbol_table):
        (ir_of_expression,expression_type, expression_dims)=self.exp.check(symbol_table)
        if expression_dims > 0 :
            raise semanticError("Return expression can not be an array.")

        return iReturn(ir_of_expression)


class block_stmt:
    def __init__(self,declaration_list,statement_list):
        self.declaration_list = declaration_list
        self.statement_list = statement_list

    def __repr__(self):
        children=[]
        for declaration in self.declaration_list:
            children.append(child(declaration))
        for statement in self.statement_list:
            children.append(child(statement))
        return node('Block statement',''.join(children))

    def check(self,symbol_table):
        symbol_table.newscope()
        for declaration in self.declaration_list:
            symbol_table.insert(declaration)

        ir_of_array_list=[]
        ir_of_function_body_list=[]
        for declaration in self.declaration_list:
            ir_of_declaration= declaration.check(symbol_table)
            if ir_of_declaration != None :
                if ir_of_declaration.iAmAFunction():
                    ir_of_function_body_list.append(ir_of_declaration)
                else:
                    ir_of_array_list.append(ir_of_declaration)
        ir_of_statement_list=[]
        for statement in self.statement_list:
            ir_of_statement_list.append(statement.check(symbol_table))
        varstore = symbol_table.storage()
        symbol_table.removescope()

        return iBlock(ir_of_function_body_list,
                      varstore,
                      ir_of_array_list,
                      ir_of_statement_list)


#data M_type = M_int | M_bool
# Not needed in python - use a string or enumerated value  if you want.

#data M_expr = M_num Int
#            | M_bl Bool
#            | M_id String
#            | M_app (M_operation,[M_expr])

class expr:
    def __init__(self, numv=None, bval=None, rval=None,id=None, arrayaddress=None,apply=None):
        self.numv=numv
        self.bval=bval
        self.rval=rval
        self.id=id
        self.arrayaddress=arrayaddress
        self.apply=apply

    def __repr__(self):
        if self.numv <>None :
            return node('expr', leaf('int', 'int value', self.numv),typ="ival")
        if self.bval <>None :
            return node('expr', leaf('bool', 'bool value', self.bval),typ="bval")
        if self.rval <>None :
            return node('expr', leaf('real', 'real value', self.rval),typ="rval")
        if self.id <>None :
            addr = ''
            #print "*****ID ****\n",self.id , "\n****End of ID name****"
            #print "*****arrayaddress****\n",self.arrayaddress , "\n****End of arrayaddress name****"

            for aa in self.arrayaddress:
                addr = addr + child(aa, typ='array address')
            return node('expr', leaf('string', 'id', self.id) + addr,typ="identifier")

        appfn=self.apply[0]
        appfn = appfn.replace('&&','&amp;&amp;');
        appfn = appfn.replace('<','&lt;')
        appfn = appfn.replace('>','&gt;')

        appfnparms=self.apply[1]

        parms = ''
        for p in appfnparms:
            parms = parms + child(p,typ='Function parameter')

        return node('expr',leaf('string','Function name',appfn) + parms,typ="apply")

    def check(self,symbol_table):
        if self.numv <> None :
            return (iIntegerExpression(self.numv),
                    'int',
                    0)
        if self.bval <> None :
            return (iBooleanExpression(self.bval),
                    'bool'
                    ,0)
        if self.rval <> None :
            return (iFloatExpression(self.rval),
                    'real'
                    ,0)
        if self.id <> None :
            symbol_entry=symbol_table.lookup(self.id)
            if len(self.arrayaddress) > 0:
                if len(self.arrayaddress) <>symbol_entry.num_of_dimensions():
                    raise semanticError("Improper number of dimensions.")
                else:
                    indexexps=[]
                    for indexexp in self.arrayaddress :
                        (iindex,indtype,inddims) = indexexp.check(symbol_table)
                        if indtype <> 'int':
                            raise semanticError("Array indexes must be of type integer.")
                        if inddims > 0:
                            raise semanticError("Array indexes cannot be arrays.")
                        indexexps.append(iindex)
                    return ((iArrayIdentifier(symbol_entry.level,
                                              symbol_entry.offset,
                                              indexexps)),
                            symbol_entry.entry_type(),
                            0)
            else:
                return ((iSimpleIdentifier(symbol_entry.level,
                                         symbol_entry.offset)),
                        symbol_entry.entry_type(),
                        symbol_entry.num_of_dimensions())

        #else we are in an apply operation


        appop=self.apply[0]   # a string
        appargs=self.apply[1] # an instance of args

        irargs=[]
        argtypes=[]
        argdims=[]
        for arg in appargs:
            (irarg,argtype,argdim) = arg.check(symbol_table)
            irargs.append(irarg)
            argtypes.append(argtype)
            argdims.append(argdim)

        fkey=(appop,tuple(argtypes))
        try:
            stentry=symbol_table.lookup(fkey)
        except semanticError, s:
            print s
            raise semanticError("function not found, or arg types wrong")

        if appop == 'SIZE':
            if argdims[0] == 0:
                raise semanticError("Size must be applied only to arrays, %s is not an array."%appargs[0].id)
            if appargs[1].numv >= argdims[0]:
                raise semanticError("Size was applied to the array %s, asking for the %d dimension, but it only has %d dimensions."%(appargs[0].id,appargs[1].numv, argdims[0]))
        else:
            for i in range(len(stentry.function_argdims)):
                if argdims[i] <> stentry.function_argdims[i]:
                    raise semanticError("Function %s called with %d'th arg improperly dimensioned."%(self.appop,i))

        if stentry.code_label == "builtin":
            op=iOperation(appop,argtypes)
        else:
            op=iFunction(stentry.code_label,stentry.level)

        return (iFunctionOrOperationApplication(op,irargs),
                stentry.entry_type(),
                0)


#data M_operation =  M_fn String | M_add | M_mul | M_sub | M_div | M_neg
#                | M_lt | M_le | M_gt | M_ge | M_eq | M_not | M_and | M_or;



