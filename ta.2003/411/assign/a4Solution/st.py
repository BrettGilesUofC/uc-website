
labelgen=0

from mpexcep import semanticError

VARIABLEOFFSETSTART=1
ARGUMENTOFFSETSTART=-5

class stVariableArrayEntry:
    def __init__(self,level,offset, variable_type, dimensions):
        self.level = level
        self.variable_type=variable_type
        self.offset=offset
        self.dimensions = dimensions

    def entry_type(self):
        return self.variable_type

    def i_am_an_array(self): return 1
    def i_am_a_function(self): return 0

    def num_of_dimensions(self):
        return len(self.dimensions)

class stVariableEntry:
    def __init__(self,level,offset,variable_type):
        self.level = level
        self.variable_type=variable_type
        self.offset=offset

    def entry_type(self):
        return self.variable_type

    def i_am_an_array(self): return 0

    def i_am_a_function(self): return 0      

    def num_of_dimensions(self):
        return 0

class stArgumentEntry(stVariableArrayEntry): # just to have a different type...
    def __init__(self,level,offset,argument_type,arraydimensions):
        stVariableArrayEntry.__init__(self,
                                      level,
                                      offset,
                                      argument_type,
                                      arraydimensions)
    def i_am_an_array(self): return self.dimensions


    def num_of_dimensions(self):
        return self.dimensions
    
class stFunctionEntry:
    def __init__(self,level,codelabel,
                 function_type,
                 function_argtypes,
                 function_argdims):
        self.level = level
        self.function_argtypes=function_argtypes
        self.function_type=function_type
        self.function_argdims = function_argdims
        self.code_label=codelabel

    def entry_type(self):
        return self.function_type

    def i_am_an_array(self): return 0

    def i_am_a_function(self): return 1

    def num_of_dimensions(self):
        return 0


#doesn't need to be a class really.... Only one instance
class SymbolTable:
    def __init__(self):
        self.__symbol_table={}   # dict of ids -> [stentries]
        self.level=-1
        self.identifiers_at_level_list=[[]]  # list of ids at this level
        self.argument_offset_list = []
        self.variable_offset_list = []
        self.array_sizes = []


    def __repr__(self):
        return self.__symbol_table.__repr__() + \
               self.identifiers_at_level_list.__repr__()

    def newscope(self):
        self.level += 1
        self.identifiers_at_level_list.append([])
        self.array_sizes.append([])
        self.argument_offset_list.append(ARGUMENTOFFSETSTART)
        self.variable_offset_list.append(VARIABLEOFFSETSTART)



    def removescope(self):
        if self.level == 0: return
        try:
            for id in self.identifiers_at_level_list[-1]:
                self.__symbol_table[id].pop()
                if 0 == len(self.__symbol_table[id]):
                    del self.__symbol_table[id]
            self.identifiers_at_level_list.pop()
            self.array_sizes.pop()
            self.argument_offset_list.pop()
            self.variable_offset_list.pop()
            self.level -= 1
        except:
            print "Internal symbol table error"
            raise


    def storage(self):
        return (self.variable_offset_list[-1] -1)

    def lookup(self, lookup_id):
      #  print "looking for ",lookup_id
      #  print "in ",self.__symbol_table.keys()
      #  print
      try:
          sentry = self.__symbol_table[lookup_id][-1]
      except KeyError:
          try:
              raise semanticError("Identifier not found: %s"%(lookup_id))
          except TypeError:
              raise semanticError("Function %s with arguments %s not found"%lookup_id)
      return sentry

    def insert(self,declaration):
        (id, entry) = self.make_entry(declaration)

        if id in self.identifiers_at_level_list[-1]:
            raise semanticError("Duplicate id." + id)

        try:
            self.__symbol_table[id].append(entry)
        except:
            self.__symbol_table[id] = [entry]

        self.identifiers_at_level_list[-1].append(id)


    def insertbuiltin(self,fname,type,argtypes):

        stid = (fname,tuple(argtypes)) # mangling....
        if stid in self.identifiers_at_level_list[-1]:
            raise semanticError("Duplicate builtin.")
        self.identifiers_at_level_list[-1].append(stid)

        argdims=[]
        if len(argtypes) == 1:
            argdims=[0]
        elif len(argtypes) == 2:
            argdims=[0,0]
        newentry=stFunctionEntry(0,'builtin',type,argtypes, argdims)
        try:
            self.__symbol_table[stid].append(newentry)
        except:
            self.__symbol_table[stid] = [newentry]

    def make_entry(self,declaration):
        if declaration.isvar() and len(declaration.arraydim) <> 0:
            newentry = stVariableArrayEntry(self.level,
                                            self.variable_offset_list[-1],
                                            declaration.dtype,
                                            declaration.arraydim)
            self.array_sizes[-1].append((self.variable_offset_list[-1],
                                         declaration.arraydim))
            self.variable_offset_list[-1] +=1
            return (declaration.id, newentry)
        elif declaration.isvar():
            newentry = stVariableEntry(self.level,
                                       self.variable_offset_list[-1],
                                       declaration.dtype)
            self.variable_offset_list[-1]+=1
            return (declaration.id, newentry)
        elif declaration.isfun():

            newentry = stFunctionEntry(self.level,
                                       getlabel( declaration.id),
                                       declaration.dtype,
                                       declaration.funargtypes(),
                                       declaration.funargdims())
            return (tuple((declaration.id,tuple(declaration.funargtypes()))),
                     newentry)
        else:
            newentry= stArgumentEntry(self.level,
                                      self.argument_offset_list[-1],
                                      declaration.dtype,
                                      declaration.arraydim)
            self.argument_offset_list[-1] -=1
            return (declaration.id, newentry)


def getlabel(s):
    global labelgen
    labelgen+=1
    return "func_%s_%d"%(s,labelgen)

