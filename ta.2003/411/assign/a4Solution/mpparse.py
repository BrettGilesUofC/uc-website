
import yacc, sys
from mplex import tokens
from ast import *
# Get the token map
#tokens = mplex.tokens
precedence = (
    ('right','UMINUS'),
    )

def p_prog(t):
    'prog : block'
    t[0]=t[1]


def p_block(t):
    'block : declarations programbody'
    t[0]=prog(t[1],t[2])


def p_programbody(t):
    'programbody : BEGIN progstmts END'
    t[0]=t[2]


def p_progstmts(t):
    'progstmts : progstmt SEMICOLON progstmts'
    t[0]=[t[1]]+t[3] # catenate the first element onto the front of the list


def p_progstmts_empty(t):
    'progstmts : empty'
    t[0]=[]


def p_progstmt(t):
    '''progstmt : IF expr THEN progstmt ELSE progstmt
                 | READ identifier
                 | WHILE expr DO progstmt
                 | identifier ASSIGN expr
                 | PRINT expr
                 | CLPAR block CRPAR'''
    if t[1]=='if': 
        t[0]=cond_stmt(t[2],t[4],t[6])
    elif t[1] == 'read':
        t[0]=read_stmt(t[2][0], t[2][1])
    elif t[1] == 'while':
        t[0]=while_stmt(t[2],t[4])
    elif t[1] == 'print':
        t[0]=print_stmt(t[2])
    elif t[1] == '{':
        t[0]=block_stmt(t[2].declaration_list, t[2].statement_list) #t2 is a block which returns a prog...
    else:  # 'ID':
        t[0]=ass_stmt(t[1][0], t[1][1],t[3]) # identifier now has potential array addresses

def p_identifier(t):
    '''identifier : ID arrayaddress'''
    t[0] = (t[1],t[2])

def p_arrayaddress_empty(t):
    'arrayaddress : empty'
    t[0]=[]
    #print 'aaempty', t[0]

def p_arrayaddress_list(t):
    '''arrayaddress : SQLPAR expr SQRPAR arrayaddress'''
    t[0] = [t[2]]+t[4]
    #print 'aalist', t[2], t[4]
    

def p_declarations_empty(t):
    'declarations : empty'
    t[0]=[]

def p_declarations_list(t):
    'declarations : declaration SEMICOLON declarations'
    t[0]=[t[1]]+t[3]

def p_declaration(t):
    '''declaration : var_declaration
                    | fundec'''

    t[0]=t[1]

def p_var_declaration(t):
    'var_declaration :  VAR ID arraydimensions COLON type'
    t[0]=decl('var',id=t[2], dtype=t[5],arraydim=t[3])


def p_type(t):
    '''type : INT
            | BOOL
            | REAL'''

    t[0]=t[1]
#def p_datadec(t):
#    'datadec : DATA ID EQUAL cons_declarations'
#    t[0] = (t[2],t[4])
#
#    
#def p_cons_declarations(t):
#    'cons_declarations : cons_decl more_cons_decl'
#    t[0] = [t[1]]+t[2]
#    
#def p_more_cons_decl_l(t):
#    'more_cons_decl : SLASH cons_decl more_cons_decl'
#    t[0] = [t[2]]+t[3]
#
#def p_more_cons_decl_e(t):
#    'more_cons_decl : empty'
#    t[0] = []
#
#def p_cons_decl(t):
#    'cons_decl : CID OF type_list'
#    t[0] = (t[1], t[3])
#
#def p_type_list(t):
#    'type_list : type more_type'
#    t[0] = [t[1]]+ t[2]
#
#def p_more_type_l(t):
#    'more_type : MULT type more_type'
#    t[0] = [t[2]]+ t[3]
#def p_more_type_e(t):
#    'more_type : empty'
#    t[0] = []
    
def p_arraydimensions(t):
    'arraydimensions : arrayaddress'
    t[0]=t[1]
    #print 'td', t[0]
    
def p_fundec(t):
    'fundec : FUN ID paramlist COLON type CLPAR funblock CRPAR'
    t[0]=decl('fun', id=t[2],funargdeclaration_list=t[3],dtype=t[5],funprog=t[7])

def p_paramlist(t):
    'paramlist : LPAR parameters RPAR'
    t[0]=t[2]

def p_parameters(t):
    'parameters : parameters1'
    t[0]=t[1]

def p_parameters_empty(t):
    'parameters : empty'

    t[0]=[]

def p_parameters1_1(t):
    'parameters1 : parameters1 COMMA basicdeclaration'

    t[0]=t[1]+[t[3]]

    
def p_parameters1_2(t):
    'parameters1 : basicdeclaration'

    t[0]=[t[1]]


def p_basicdeclaration(t):
    'basicdeclaration :  ID basicarraydimensions COLON type'
    t[0]=decl('arg',id=t[1], dtype=t[4],arraydim=t[2])



def p_basicarraydimensions_empty(t):
    'basicarraydimensions : empty'
    t[0]=0

def p_basicarraydimensions_list(t):
    '''basicarraydimensions : SQLPAR SQRPAR basicarraydimensions'''
    t[0] = 1+t[3]


def p_funblock(t):
    'funblock : declarations fun_body'
    t[0]=prog(t[1],t[2])

def p_fun_body(t):
    'fun_body :  BEGIN progstmts RETURN expr SEMICOLON END'
    rstmt=ret_stmt(t[4])
    t[0]=t[2] + [rstmt]

def p_expr_1(t):
    'expr  :   expr OR brint_term'
    t[0]=expr(apply=(t[2],[t[1],t[3]])) # tuple of (OP, explist)

def p_expr_2(t):
    'expr :  brint_term'
    t[0]=t[1]

def p_brint_term_1(t):
    'brint_term : brint_term AND brint_factor'
    t[0]=expr(apply=(t[2],[t[1],t[3]]))

def p_brint_term_2(t):
    'brint_term :  brint_factor'
    t[0]=t[1]

def p_brint_factor_1(t):
    'brint_factor : NOT brint_factor'
    t[0]=expr(apply=(t[1],[t[2]]))

def p_brint_factor_2(t):
    'brint_factor :  int_expr'
    t[0]=t[1]

              
def p_brint_factor_3(t):
    'brint_factor : int_expr compare_op int_expr'
    t[0]=expr(apply=(t[2],[t[1],t[3]]))
    
def p_compare_op(t):
    '''compare_op : EQUAL
                  | LT
                  | GT
                  | LE
                  | GE'''
    t[0]=t[1]

def p_int_expr_1(t):
    'int_expr : int_expr addop int_term'
    t[0]=expr(apply=(t[2],[t[1],t[3]]))

    
def p_int_expr_2(t):
    'int_expr : int_term'
    t[0]=t[1]
    
def p_addop(t):
    '''addop : ADD
             | SUB'''
    t[0]=t[1]

def p_int_term_1(t):
    'int_term : int_term mulop int_factor'
    t[0]=expr(apply=(t[2],[t[1],t[3]]))

def p_int_term_2(t):
    'int_term  : int_factor'
    t[0]=t[1]

def p_mulop(t):
    '''mulop : MULT 
           | DIV'''
    t[0]=t[1]

def p_int_factor_0(t):
    'int_factor : ID modifier_list'
    if t[2][0] == "array" :
        #print 'id(array)', t[1], t[2]
        t[0]=expr(id=t[1],arrayaddress=t[2][1])
    else:
        #print 'id(func)', t[1], t[2]
        t[0]=expr(apply=(t[1],t[2][1]))
          
def p_int_factor_1(t):
    'int_factor : LPAR expr RPAR'
    t[0]=t[2]

def p_int_factor_2(t):
     'int_factor : SIZE LPAR ID basicarraydimensions RPAR'
     t[0] = expr(apply=("SIZE", [expr(id=t[3],arrayaddress=[]), expr(numv=t[4])]))

def p_int_factor_3(t):
    '''int_factor : FLOAT LPAR expr RPAR
                            | FLOOR LPAR expr RPAR
                            | CIEL LPAR expr RPAR'''
    t[0]=expr(apply=(t[1],[t[3]]))
 
def p_int_factor_4(t):
    'int_factor : IVAL'
    t[0]=expr(numv=t[1])

def p_int_factor_5(t):
    'int_factor : BVAL'
    t[0]=expr(bval=t[1])

def p_int_factor_6(t):
    'int_factor : SUB int_factor %prec UMINUS'
    t[0]=expr(apply=("NEG",[t[2]]))

def p_int_factor_7(t):
    'int_factor : RVAL'
    t[0]=expr(rval=t[1])

def p_modifier_list_array(t):
    '''modifier_list : arrayaddress'''
    t[0]=("array",t[1])
    
def p_modifier_list_args(t):
    '''modifier_list : LPAR arguments1 RPAR'''
    t[0]=("apply",t[2])

def p_modifier_list_empty(t):
    '''modifier_list : LPAR  RPAR'''
    t[0]=("apply",[])
    
#def p_arguments(t):
#    'arguments : arguments1'
#    t[0]=t[1]
    
#def p_arguments_empty(t):
#    'arguments :  empty'
#    t[0]=[]

def p_arguments1_1(t):
    'arguments1 : arguments1 COMMA expr'
    t[0]=t[1] + [t[3]]
def p_arguments1_2(t):
    'arguments1 :  expr'
    t[0]=[t[1]]



def p_empty(t):
    'empty : '
    pass

def p_error(t):
    print 'Yacc error on ', t
    sys.exit(4)

yacc.yacc()

def mpparse(str):
    return yacc.parse(str)



