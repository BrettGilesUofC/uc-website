
def node(name,item, typ=None):
    if typ:
        return "<node name='%s'  type='%s'>\n%s</node>\n"%(name,typ,item)
    else:
        return "<node name='%s'>\n%s</node>\n"%(name,item)
    
def child(item,typ=None):
    if typ == None :
        return "<child>\n%s</child>\n"%item
    else:
        return "<child type='%s'>\n%s</child>\n"%(typ,item)
    
def childList(itemList,typ=None):
    children=[]
    for item in itemList:
        children.append(child(item,typ))
    return ''.join(children)

def leaf(typ,name,item):
      return child("<leaf type='%s' name='%s'>\n%s\n</leaf>\n"%(typ,name,item))
