#!/usr/bin/python

# cmntlex.py
#
# Remove comments from m+ 
# ----------------------------------------------------------------------
import sys
import lex

# Reserved words


tokens = (
    'SC', 'EC',
    'CHAR', 'NEWLINE')
    

# Newlines
def t_NEWLINE(t):
    r'\n+'
    t.lineno += t.value.count("\n")
    return t
    
# Operators
t_SC           = r'\/\*'
t_EC           = r'\*\/'
t_CHAR         = r'.'


def t_error(t):
    print "Illegal character %s" % repr(t.value[0])
    t.skip(1)
    
cmntlex = lex.lex()

def remove_comments(inp):
    ret=""
    cmntlex.input(inp)
    incomments=0
    while 1:
        tok=cmntlex.token()
        if not tok: break
        if tok.type=='SC' :
            incomments += 1
        if not incomments : ret=ret+tok.value
        if tok.type=='EC':
            incomments -= 1

        
    return ret
    
if __name__ == "__main__":
    t=sys.stdin
    inp = t.read()
    print remove_comments(inp)

        


   



