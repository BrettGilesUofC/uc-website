function remove_levels(lvel){
	var end_expr = /\w*\.html$/;
	var locs = location.pathname.split('/');
	
	var take_levels = (locs.length-2)-lvel;
	if (end_expr.test(locs[locs.length-1]) || locs[locs.length-1] == '') {
		take_levels++;
	};

	var ret = "/";
	for (var i = 1; i < take_levels; i++) {
		ret += locs[i] + "/";
	};

	return ret;
}  

function write_menu(level){
	rel_path = remove_levels(level);
	document.writeln('<!-- menu table -->');
  document.writeln('<div class="menubar">');
  document.writeln('<table class="menubar">');
  document.writeln('  <tr>');
  document.write('    <td class="menuitem"><a href="http://www.ucalgary.ca/">UCalgary</a></td>');
  document.write('    <td  class="menuitem"><a href="http://www.cpsc.ucalgary.ca/">CPSC</a></td>');
  document.write('    <td class="menuitem"><a href="'+rel_path+'">My Main</a></td>');
  document.write('    <td class="menuitem"><a href="'+rel_path+'research/index.html">My Research</a></td>');
  document.write('    <td class="menuitem"><a href="'+rel_path+'research/lqpl.html">LQPL</a></td>');
  // document.write('    <td class="menuitem"><a href="'+rel_path+'hobbies/layout.html">My Hobbies</a></td>');
  document.writeln('</tr>');
  document.writeln('</table> <!-- End Menu Table -->');
  document.writeln('</div>');
}
